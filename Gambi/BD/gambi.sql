toc.dat                                                                                             0000600 0004000 0002000 00000451271 13563757540 0014470 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP           %            
    w            gambi    10.10    10.10 �               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                    0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                    0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                    1262    221303    gambi    DATABASE     �   CREATE DATABASE gambi WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Colombia.1252' LC_CTYPE = 'Spanish_Colombia.1252';
    DROP DATABASE gambi;
             postgres    false                     0    0    DATABASE gambi    COMMENT     E   COMMENT ON DATABASE gambi IS 'Base de datos para el software Gambi';
                  postgres    false    3103                     2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false         !           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                     2615    254119    security    SCHEMA        CREATE SCHEMA security;
    DROP SCHEMA security;
             postgres    false         "           0    0    SCHEMA security    COMMENT     F   COMMENT ON SCHEMA security IS 'Esquema de la seguridad del proyecto';
                  postgres    false    5                     2615    221304    usuario    SCHEMA        CREATE SCHEMA usuario;
    DROP SCHEMA usuario;
             postgres    false         #           0    0    SCHEMA usuario    COMMENT     A   COMMENT ON SCHEMA usuario IS 'Esquema de los datos del usuario';
                  postgres    false    8                     3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false         $           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1                    1255    304339    f_cerrar_sesion(bigint)    FUNCTION     �   CREATE FUNCTION security.f_cerrar_sesion(_doc_identidad bigint) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE security.autenticacion
	SET 
		fecha_fin = current_timestamp
	WHERE 
		doc_identidad = _doc_identidad;
END
$$;
 ?   DROP FUNCTION security.f_cerrar_sesion(_doc_identidad bigint);
       security       postgres    false    5    1                    1255    304344    f_continuar(bigint)    FUNCTION     �  CREATE FUNCTION security.f_continuar(_doc bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE
		_cantidad integer;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM security.autenticacion WHERE doc_identidad = _doc);
		IF (_cantidad = 1)
			THEN
				UPDATE security.autenticacion
				SET 
					fecha_fin = 1
				WHERE 
					doc_identidad = _doc;
			RETURN QUERY SELECT true;
		ELSE 
			RETURN QUERY SELECT false;
		END IF;
	END
$$;
 1   DROP FUNCTION security.f_continuar(_doc bigint);
       security       postgres    false    1    5                    1255    304342 *   f_guardar_sesion(bigint, text, text, text)    FUNCTION     �  CREATE FUNCTION security.f_guardar_sesion(_doc_identidad bigint, _ip text, _mac text, _session text) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE
		_cantidad integer;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM security.autenticacion WHERE doc_identidad = _doc_identidad);
		IF (_cantidad = 0)
			THEN
				INSERT INTO security.autenticacion
				(
					doc_identidad,
					ip,
					mac,
					fecha_ini,
					sesion,
					fecha_fin
				)
				VALUES
				(
					_doc_identidad,
					_ip,
					_mac,
					current_timestamp,
					_session,
					1
				);
			RETURN QUERY SELECT true;
		ELSEIF (_cantidad = 1)
			THEN
				UPDATE security.autenticacion
				SET 
					doc_identidad = _doc_identidad,
					ip = _ip,
					mac = _mac,
					fecha_ini = current_timestamp,
					sesion = _session,
					fecha_fin = 1
				WHERE 
					doc_identidad = _doc_identidad;
			RETURN QUERY SELECT true;
		ELSE 
			RETURN QUERY SELECT false;
		END IF;
	END
$$;
 d   DROP FUNCTION security.f_guardar_sesion(_doc_identidad bigint, _ip text, _mac text, _session text);
       security       postgres    false    1    5         �            1255    254182    f_log_auditoria()    FUNCTION     �  CREATE FUNCTION security.f_log_auditoria() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	 DECLARE
		_pk TEXT :='';		-- Representa la llave primaria de la tabla que esta siedno modificada.
		_sql TEXT;		-- Variable para la creacion del procedured.
		_column_guia RECORD; 	-- Variable para el FOR guarda los nombre de las columnas.
		_column_key RECORD; 	-- Variable para el FOR guarda los PK de las columnas.
		_session TEXT;	-- Almacena el usuario que genera el cambio.
		_user_db TEXT;		-- Almacena el usuario de bd que genera la transaccion.
		_control INT;		-- Variabel de control par alas llaves primarias.
		_count_key INT = 0;	-- Cantidad de columnas pertenecientes al PK.
		_sql_insert TEXT;	-- Variable para la construcción del insert del json de forma dinamica.
		_sql_delete TEXT;	-- Variable para la construcción del delete del json de forma dinamica.
		_sql_update TEXT;	-- Variable para la construcción del update del json de forma dinamica.
		_new_data RECORD; 	-- Fila que representa los campos nuevos del registro.
		_old_data RECORD;	-- Fila que representa los campos viejos del registro.

	BEGIN

			-- Se genera la evaluacion para determianr el tipo de accion sobre la tabla
		 IF (TG_OP = 'INSERT') THEN
			_new_data := NEW;
			_old_data := NEW;
		ELSEIF (TG_OP = 'UPDATE') THEN
			_new_data := NEW;
			_old_data := OLD;
		ELSE
			_new_data := OLD;
			_old_data := OLD;
		END IF;

		-- Se genera la evaluacion para determianr el tipo de accion sobre la tabla
		IF ((SELECT COUNT(*) FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME AND column_name = 'id' ) > 0) THEN
			_pk := _new_data.id;
		ELSE
			_pk := '-1';
		END IF;

		-- Se valida que exista el campo modified_by
		IF ((SELECT COUNT(*) FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME AND column_name = 'session') > 0) THEN
			_session := _new_data.session;
		ELSE
			_session := '';
		END IF;

		-- Se guarda el susuario de bd que genera la transaccion
		_user_db := (SELECT CURRENT_USER);

		-- Se evalua que exista el procedimeinto adecuado
		IF (SELECT COUNT(*) FROM security.function_db_view acfdv WHERE acfdv.b_function = 'field_audit' AND acfdv.b_type_parameters = TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', character varying, character varying, character varying, text, character varying, text, text') > 0
			THEN
				-- Se realiza la invocación del procedured generado dinamivamente
				PERFORM security.field_audit(_new_data, _old_data, TG_OP, _session, _user_db , _pk, ''::text);
		ELSE
			-- Se empieza la construcción del Procedured generico
			_sql := 'CREATE OR REPLACE FUNCTION security.field_audit( _data_new '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', _data_old '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', _accion character varying, _session text, _user_db character varying, _table_pk text, _init text)'
			|| ' RETURNS TEXT AS ''
'
			|| '
'
	|| '	DECLARE
'
	|| '		_column_data TEXT;
	 	_datos jsonb;
	 	
'
	|| '	BEGIN
			_datos = ''''{}'''';
';
			-- Se evalua si hay que actualizar la pk del registro de auditoria.
			IF _pk = '-1'
				THEN
					_sql := _sql
					|| '
		_column_data := ';

					-- Se genera el update con la clave pk de la tabla
					SELECT
						COUNT(isk.column_name)
					INTO
						_control
					FROM
						information_schema.table_constraints istc JOIN information_schema.key_column_usage isk ON isk.constraint_name = istc.constraint_name
					WHERE
						istc.table_schema = TG_TABLE_SCHEMA
					 AND	istc.table_name = TG_TABLE_NAME
					 AND	istc.constraint_type ilike '%primary%';

					-- Se agregan las columnas que componen la pk de la tabla.
					FOR _column_key IN SELECT
							isk.column_name
						FROM
							information_schema.table_constraints istc JOIN information_schema.key_column_usage isk ON isk.constraint_name = istc.constraint_name
						WHERE
							istc.table_schema = TG_TABLE_SCHEMA
						 AND	istc.table_name = TG_TABLE_NAME
						 AND	istc.constraint_type ilike '%primary%'
						ORDER BY 
							isk.ordinal_position  LOOP

						_sql := _sql || ' _data_new.' || _column_key.column_name;
						
						_count_key := _count_key + 1 ;
						
						IF _count_key < _control THEN
							_sql :=	_sql || ' || ' || ''''',''''' || ' ||';
						END IF;
					END LOOP;
				_sql := _sql || ';';
			END IF;

			_sql_insert:='
		IF _accion = ''''INSERT''''
			THEN
				';
			_sql_delete:='
		ELSEIF _accion = ''''DELETE''''
			THEN
				';
			_sql_update:='
		ELSE
			';

			-- Se genera el ciclo de agregado de columnas para el nuevo procedured
			FOR _column_guia IN SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME
				LOOP
						
					_sql_insert:= _sql_insert || '_datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_nuevo'
					|| ''''', '
					|| '_data_new.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_insert:= _sql_insert
						||'::text';
					END IF;

					_sql_insert:= _sql_insert || ')::jsonb;
				';

					_sql_delete := _sql_delete || '_datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_anterior'
					|| ''''', '
					|| '_data_old.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_delete:= _sql_delete
						||'::text';
					END IF;

					_sql_delete:= _sql_delete || ')::jsonb;
				';

					_sql_update := _sql_update || 'IF _data_old.' || _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update || ' <> _data_new.' || _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update || '
				THEN _datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_anterior'
					|| ''''', '
					|| '_data_old.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update
					|| ', '''''
					|| _column_guia.column_name
					|| '_nuevo'
					|| ''''', _data_new.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update
					|| ')::jsonb;
			END IF;
			';
			END LOOP;

			-- Se le agrega la parte final del procedured generico
			
			_sql:= _sql || _sql_insert || _sql_delete || _sql_update
			|| ' 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			''''' || TG_TABLE_SCHEMA || ''''',
			''''' || TG_TABLE_NAME || ''''',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;'''
|| '
LANGUAGE plpgsql;';

			-- Se genera la ejecución de _sql, es decir se crea el nuevo procedured de forma generica.
			EXECUTE _sql;

		-- Se realiza la invocación del procedured generado dinamivamente
			PERFORM security.field_audit(_new_data, _old_data, TG_OP::character varying, _session, _user_db, _pk, ''::text);

		END IF;

		RETURN NULL;

END;
$$;
 *   DROP FUNCTION security.f_log_auditoria();
       security       postgres    false    1    5         �            1259    303823    autenticacion    TABLE     �   CREATE TABLE security.autenticacion (
    id integer NOT NULL,
    doc_identidad bigint NOT NULL,
    ip text NOT NULL,
    mac text NOT NULL,
    fecha_ini timestamp without time zone NOT NULL,
    sesion text NOT NULL,
    fecha_fin text NOT NULL
);
 #   DROP TABLE security.autenticacion;
       security         postgres    false    5         %           0    0    TABLE autenticacion    COMMENT     }   COMMENT ON TABLE security.autenticacion IS 'Tabla de datos para almacenar los datos de los usuarios cuando inician sesión';
            security       postgres    false    222                     1255    303836    f_obtener_sesion(bigint)    FUNCTION       CREATE FUNCTION security.f_obtener_sesion(_doc_identidad bigint) RETURNS SETOF security.autenticacion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			sc.*
		FROM 
			security.autenticacion sc
		WHERE 
			doc_identidad = _doc_identidad;
	END;
$$;
 @   DROP FUNCTION security.f_obtener_sesion(_doc_identidad bigint);
       security       postgres    false    5    222    1                    1255    304047    f_validar_fecha_fin(bigint)    FUNCTION     6  CREATE FUNCTION security.f_validar_fecha_fin(_doc_identidad bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF (SELECT fecha_fin FROM security.autenticacion WHERE doc_identidad = _doc_identidad) = null
		THEN
		RETURN QUERY SELECT true;
	ELSE 
		RETURN QUERY SELECT false;
	END IF;
END
$$;
 C   DROP FUNCTION security.f_validar_fecha_fin(_doc_identidad bigint);
       security       postgres    false    5    1         �            1259    262409 	   atraccion    TABLE     1  CREATE TABLE usuario.atraccion (
    id_atraccion integer NOT NULL,
    nombre text NOT NULL,
    precio_per integer NOT NULL,
    descripcion text NOT NULL,
    cant_per integer NOT NULL,
    disponibilidad boolean NOT NULL,
    hora_inicio time without time zone,
    hora_fin time without time zone
);
    DROP TABLE usuario.atraccion;
       usuario         postgres    false    8         &           0    0    TABLE atraccion    COMMENT     \   COMMENT ON TABLE usuario.atraccion IS 'tabla para almacenar los datos de las atracciones ';
            usuario       postgres    false    206                    1255    262810 i   field_audit(usuario.atraccion, usuario.atraccion, character varying, text, character varying, text, text)    FUNCTION     #  CREATE FUNCTION security.field_audit(_data_new usuario.atraccion, _data_old usuario.atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_atraccion;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_atraccion_nuevo', _data_new.id_atraccion)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('precio_per_nuevo', _data_new.precio_per)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				_datos := _datos || json_build_object('cant_per_nuevo', _data_new.cant_per)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('hora_inicio_nuevo', _data_new.hora_inicio)::jsonb;
				_datos := _datos || json_build_object('hora_fin_nuevo', _data_new.hora_fin)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_atraccion_anterior', _data_old.id_atraccion)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('precio_per_anterior', _data_old.precio_per)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				_datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('hora_inicio_anterior', _data_old.hora_inicio)::jsonb;
				_datos := _datos || json_build_object('hora_fin_anterior', _data_old.hora_fin)::jsonb;
				
		ELSE
			IF _data_old.id_atraccion <> _data_new.id_atraccion
				THEN _datos := _datos || json_build_object('id_atraccion_anterior', _data_old.id_atraccion, 'id_atraccion_nuevo', _data_new.id_atraccion)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.precio_per <> _data_new.precio_per
				THEN _datos := _datos || json_build_object('precio_per_anterior', _data_old.precio_per, 'precio_per_nuevo', _data_new.precio_per)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			IF _data_old.cant_per <> _data_new.cant_per
				THEN _datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per, 'cant_per_nuevo', _data_new.cant_per)::jsonb;
			END IF;
			IF _data_old.disponibilidad <> _data_new.disponibilidad
				THEN _datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad, 'disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
			END IF;
			IF _data_old.hora_inicio <> _data_new.hora_inicio
				THEN _datos := _datos || json_build_object('hora_inicio_anterior', _data_old.hora_inicio, 'hora_inicio_nuevo', _data_new.hora_inicio)::jsonb;
			END IF;
			IF _data_old.hora_fin <> _data_new.hora_fin
				THEN _datos := _datos || json_build_object('hora_fin_anterior', _data_old.hora_fin, 'hora_fin_nuevo', _data_new.hora_fin)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'atraccion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.atraccion, _data_old usuario.atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    1    5    206    206         �            1259    229889    estado    TABLE     _   CREATE TABLE usuario.estado (
    id_estado integer NOT NULL,
    descripcion text NOT NULL
);
    DROP TABLE usuario.estado;
       usuario         postgres    false    8         '           0    0    TABLE estado    COMMENT     b   COMMENT ON TABLE usuario.estado IS 'Tabla para almacenar los datos de los estados de la reserva';
            usuario       postgres    false    201                    1255    304350 c   field_audit(usuario.estado, usuario.estado, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new usuario.estado, _data_old usuario.estado, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_estado;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_estado_nuevo', _data_new.id_estado)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				
		ELSE
			IF _data_old.id_estado <> _data_new.id_estado
				THEN _datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado, 'id_estado_nuevo', _data_new.id_estado)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'estado',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.estado, _data_old usuario.estado, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    201    201    1    5         �            1259    304847 
   estado_fac    TABLE     \   CREATE TABLE usuario.estado_fac (
    id integer NOT NULL,
    descripcion text NOT NULL
);
    DROP TABLE usuario.estado_fac;
       usuario         postgres    false    8         6           1255    304887 k   field_audit(usuario.estado_fac, usuario.estado_fac, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new usuario.estado_fac, _data_old usuario.estado_fac, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'estado_fac',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.estado_fac, _data_old usuario.estado_fac, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    230    1    5    230         �            1259    304829    factura_atraccion    TABLE       CREATE TABLE usuario.factura_atraccion (
    id_factura integer NOT NULL,
    total_pag bigint NOT NULL,
    id_sedeatr integer NOT NULL,
    doc_identidad bigint NOT NULL,
    fecha_ent date NOT NULL,
    id_sede integer NOT NULL,
    estado integer NOT NULL
);
 &   DROP TABLE usuario.factura_atraccion;
       usuario         postgres    false    8         8           1255    304891 y   field_audit(usuario.factura_atraccion, usuario.factura_atraccion, character varying, text, character varying, text, text)    FUNCTION     v  CREATE FUNCTION security.field_audit(_data_new usuario.factura_atraccion, _data_old usuario.factura_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_factura;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_factura_nuevo', _data_new.id_factura)::jsonb;
				_datos := _datos || json_build_object('total_pag_nuevo', _data_new.total_pag)::jsonb;
				_datos := _datos || json_build_object('id_sedeatr_nuevo', _data_new.id_sedeatr)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('estado_nuevo', _data_new.estado)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_factura_anterior', _data_old.id_factura)::jsonb;
				_datos := _datos || json_build_object('total_pag_anterior', _data_old.total_pag)::jsonb;
				_datos := _datos || json_build_object('id_sedeatr_anterior', _data_old.id_sedeatr)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('estado_anterior', _data_old.estado)::jsonb;
				
		ELSE
			IF _data_old.id_factura <> _data_new.id_factura
				THEN _datos := _datos || json_build_object('id_factura_anterior', _data_old.id_factura, 'id_factura_nuevo', _data_new.id_factura)::jsonb;
			END IF;
			IF _data_old.total_pag <> _data_new.total_pag
				THEN _datos := _datos || json_build_object('total_pag_anterior', _data_old.total_pag, 'total_pag_nuevo', _data_new.total_pag)::jsonb;
			END IF;
			IF _data_old.id_sedeatr <> _data_new.id_sedeatr
				THEN _datos := _datos || json_build_object('id_sedeatr_anterior', _data_old.id_sedeatr, 'id_sedeatr_nuevo', _data_new.id_sedeatr)::jsonb;
			END IF;
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.fecha_ent <> _data_new.fecha_ent
				THEN _datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent, 'fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.estado <> _data_new.estado
				THEN _datos := _datos || json_build_object('estado_anterior', _data_old.estado, 'estado_nuevo', _data_new.estado)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'factura_atraccion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.factura_atraccion, _data_old usuario.factura_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    1    5    228    228         �            1259    262421    factura_habitacion    TABLE     �   CREATE TABLE usuario.factura_habitacion (
    id_factura integer NOT NULL,
    total_pag bigint NOT NULL,
    id_sedehab integer,
    doc_identidad bigint NOT NULL,
    fecha_ent date,
    id_sede integer,
    estado integer
);
 '   DROP TABLE usuario.factura_habitacion;
       usuario         postgres    false    8         (           0    0    TABLE factura_habitacion    COMMENT     b   COMMENT ON TABLE usuario.factura_habitacion IS 'Tabla para almacenar los datos de las facturas.';
            usuario       postgres    false    208         :           1255    304793 {   field_audit(usuario.factura_habitacion, usuario.factura_habitacion, character varying, text, character varying, text, text)    FUNCTION     y  CREATE FUNCTION security.field_audit(_data_new usuario.factura_habitacion, _data_old usuario.factura_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_factura;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_factura_nuevo', _data_new.id_factura)::jsonb;
				_datos := _datos || json_build_object('total_pag_nuevo', _data_new.total_pag)::jsonb;
				_datos := _datos || json_build_object('id_sedehab_nuevo', _data_new.id_sedehab)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('estado_nuevo', _data_new.estado)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_factura_anterior', _data_old.id_factura)::jsonb;
				_datos := _datos || json_build_object('total_pag_anterior', _data_old.total_pag)::jsonb;
				_datos := _datos || json_build_object('id_sedehab_anterior', _data_old.id_sedehab)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('estado_anterior', _data_old.estado)::jsonb;
				
		ELSE
			IF _data_old.id_factura <> _data_new.id_factura
				THEN _datos := _datos || json_build_object('id_factura_anterior', _data_old.id_factura, 'id_factura_nuevo', _data_new.id_factura)::jsonb;
			END IF;
			IF _data_old.total_pag <> _data_new.total_pag
				THEN _datos := _datos || json_build_object('total_pag_anterior', _data_old.total_pag, 'total_pag_nuevo', _data_new.total_pag)::jsonb;
			END IF;
			IF _data_old.id_sedehab <> _data_new.id_sedehab
				THEN _datos := _datos || json_build_object('id_sedehab_anterior', _data_old.id_sedehab, 'id_sedehab_nuevo', _data_new.id_sedehab)::jsonb;
			END IF;
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.fecha_ent <> _data_new.fecha_ent
				THEN _datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent, 'fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.estado <> _data_new.estado
				THEN _datos := _datos || json_build_object('estado_anterior', _data_old.estado, 'estado_nuevo', _data_new.estado)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'factura_habitacion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.factura_habitacion, _data_old usuario.factura_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    5    208    208    1         �            1259    262430 
   habitacion    TABLE     �   CREATE TABLE usuario.habitacion (
    id_habitacion integer NOT NULL,
    tipo_habitacion text NOT NULL,
    cant_per integer NOT NULL,
    precio_hab integer NOT NULL,
    disponibilidad boolean NOT NULL
);
    DROP TABLE usuario.habitacion;
       usuario         postgres    false    8         )           0    0    TABLE habitacion    COMMENT     ]   COMMENT ON TABLE usuario.habitacion IS 'Tabla para almacenar los datos de las habitaciones';
            usuario       postgres    false    210                    1255    295644 k   field_audit(usuario.habitacion, usuario.habitacion, character varying, text, character varying, text, text)    FUNCTION     �
  CREATE FUNCTION security.field_audit(_data_new usuario.habitacion, _data_old usuario.habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_habitacion;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_habitacion_nuevo', _data_new.id_habitacion)::jsonb;
				_datos := _datos || json_build_object('tipo_habitacion_nuevo', _data_new.tipo_habitacion)::jsonb;
				_datos := _datos || json_build_object('cant_per_nuevo', _data_new.cant_per)::jsonb;
				_datos := _datos || json_build_object('precio_hab_nuevo', _data_new.precio_hab)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_habitacion_anterior', _data_old.id_habitacion)::jsonb;
				_datos := _datos || json_build_object('tipo_habitacion_anterior', _data_old.tipo_habitacion)::jsonb;
				_datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per)::jsonb;
				_datos := _datos || json_build_object('precio_hab_anterior', _data_old.precio_hab)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad)::jsonb;
				
		ELSE
			IF _data_old.id_habitacion <> _data_new.id_habitacion
				THEN _datos := _datos || json_build_object('id_habitacion_anterior', _data_old.id_habitacion, 'id_habitacion_nuevo', _data_new.id_habitacion)::jsonb;
			END IF;
			IF _data_old.tipo_habitacion <> _data_new.tipo_habitacion
				THEN _datos := _datos || json_build_object('tipo_habitacion_anterior', _data_old.tipo_habitacion, 'tipo_habitacion_nuevo', _data_new.tipo_habitacion)::jsonb;
			END IF;
			IF _data_old.cant_per <> _data_new.cant_per
				THEN _datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per, 'cant_per_nuevo', _data_new.cant_per)::jsonb;
			END IF;
			IF _data_old.precio_hab <> _data_new.precio_hab
				THEN _datos := _datos || json_build_object('precio_hab_anterior', _data_old.precio_hab, 'precio_hab_nuevo', _data_new.precio_hab)::jsonb;
			END IF;
			IF _data_old.disponibilidad <> _data_new.disponibilidad
				THEN _datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad, 'disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'habitacion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.habitacion, _data_old usuario.habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    1    210    210    5         �            1259    304821    reserva_atraccion    TABLE     
  CREATE TABLE usuario.reserva_atraccion (
    id_reserva integer NOT NULL,
    fecha_ent date NOT NULL,
    fecha_sal date NOT NULL,
    id_estado integer NOT NULL,
    id_sede_atra integer NOT NULL,
    doc_identidad bigint NOT NULL,
    id_sede integer NOT NULL
);
 &   DROP TABLE usuario.reserva_atraccion;
       usuario         postgres    false    8         9           1255    304890 y   field_audit(usuario.reserva_atraccion, usuario.reserva_atraccion, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new usuario.reserva_atraccion, _data_old usuario.reserva_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_reserva;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_reserva_nuevo', _data_new.id_reserva)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('fecha_sal_nuevo', _data_new.fecha_sal)::jsonb;
				_datos := _datos || json_build_object('id_estado_nuevo', _data_new.id_estado)::jsonb;
				_datos := _datos || json_build_object('id_sede_atra_nuevo', _data_new.id_sede_atra)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_reserva_anterior', _data_old.id_reserva)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('fecha_sal_anterior', _data_old.fecha_sal)::jsonb;
				_datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado)::jsonb;
				_datos := _datos || json_build_object('id_sede_atra_anterior', _data_old.id_sede_atra)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				
		ELSE
			IF _data_old.id_reserva <> _data_new.id_reserva
				THEN _datos := _datos || json_build_object('id_reserva_anterior', _data_old.id_reserva, 'id_reserva_nuevo', _data_new.id_reserva)::jsonb;
			END IF;
			IF _data_old.fecha_ent <> _data_new.fecha_ent
				THEN _datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent, 'fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
			END IF;
			IF _data_old.fecha_sal <> _data_new.fecha_sal
				THEN _datos := _datos || json_build_object('fecha_sal_anterior', _data_old.fecha_sal, 'fecha_sal_nuevo', _data_new.fecha_sal)::jsonb;
			END IF;
			IF _data_old.id_estado <> _data_new.id_estado
				THEN _datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado, 'id_estado_nuevo', _data_new.id_estado)::jsonb;
			END IF;
			IF _data_old.id_sede_atra <> _data_new.id_sede_atra
				THEN _datos := _datos || json_build_object('id_sede_atra_anterior', _data_old.id_sede_atra, 'id_sede_atra_nuevo', _data_new.id_sede_atra)::jsonb;
			END IF;
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'reserva_atraccion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.reserva_atraccion, _data_old usuario.reserva_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    226    1    5    226         �            1259    262637    reserva_habitacion    TABLE     �   CREATE TABLE usuario.reserva_habitacion (
    id_reserva integer NOT NULL,
    fecha_ent date NOT NULL,
    fecha_sal date NOT NULL,
    id_estado integer NOT NULL,
    id_sede_hab integer,
    doc_identidad bigint,
    id_sede integer
);
 '   DROP TABLE usuario.reserva_habitacion;
       usuario         postgres    false    8         *           0    0    TABLE reserva_habitacion    COMMENT     a   COMMENT ON TABLE usuario.reserva_habitacion IS 'Tabla para almacenar los datos de las reservas';
            usuario       postgres    false    212         ;           1255    304771 {   field_audit(usuario.reserva_habitacion, usuario.reserva_habitacion, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new usuario.reserva_habitacion, _data_old usuario.reserva_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_reserva;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_reserva_nuevo', _data_new.id_reserva)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('fecha_sal_nuevo', _data_new.fecha_sal)::jsonb;
				_datos := _datos || json_build_object('id_estado_nuevo', _data_new.id_estado)::jsonb;
				_datos := _datos || json_build_object('id_sede_hab_nuevo', _data_new.id_sede_hab)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_reserva_anterior', _data_old.id_reserva)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('fecha_sal_anterior', _data_old.fecha_sal)::jsonb;
				_datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado)::jsonb;
				_datos := _datos || json_build_object('id_sede_hab_anterior', _data_old.id_sede_hab)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				
		ELSE
			IF _data_old.id_reserva <> _data_new.id_reserva
				THEN _datos := _datos || json_build_object('id_reserva_anterior', _data_old.id_reserva, 'id_reserva_nuevo', _data_new.id_reserva)::jsonb;
			END IF;
			IF _data_old.fecha_ent <> _data_new.fecha_ent
				THEN _datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent, 'fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
			END IF;
			IF _data_old.fecha_sal <> _data_new.fecha_sal
				THEN _datos := _datos || json_build_object('fecha_sal_anterior', _data_old.fecha_sal, 'fecha_sal_nuevo', _data_new.fecha_sal)::jsonb;
			END IF;
			IF _data_old.id_estado <> _data_new.id_estado
				THEN _datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado, 'id_estado_nuevo', _data_new.id_estado)::jsonb;
			END IF;
			IF _data_old.id_sede_hab <> _data_new.id_sede_hab
				THEN _datos := _datos || json_build_object('id_sede_hab_anterior', _data_old.id_sede_hab, 'id_sede_hab_nuevo', _data_new.id_sede_hab)::jsonb;
			END IF;
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'reserva_habitacion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.reserva_habitacion, _data_old usuario.reserva_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    212    212    5    1         �            1259    221313    rol    TABLE     T   CREATE TABLE usuario.rol (
    id_rol integer NOT NULL,
    nombre text NOT NULL
);
    DROP TABLE usuario.rol;
       usuario         postgres    false    8         +           0    0 	   TABLE rol    COMMENT     O   COMMENT ON TABLE usuario.rol IS 'Tabla para almacenar los datos de los roles';
            usuario       postgres    false    199                    1255    254203 ]   field_audit(usuario.rol, usuario.rol, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new usuario.rol, _data_old usuario.rol, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_rol;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_rol_nuevo', _data_new.id_rol)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				
		ELSE
			IF _data_old.id_rol <> _data_new.id_rol
				THEN _datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol, 'id_rol_nuevo', _data_new.id_rol)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'rol',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.rol, _data_old usuario.rol, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    199    5    1    199         �            1259    262649    sede    TABLE     �   CREATE TABLE usuario.sede (
    id_sede integer NOT NULL,
    nombre text NOT NULL,
    direccion text NOT NULL,
    telefono bigint NOT NULL,
    id_admi bigint NOT NULL
);
    DROP TABLE usuario.sede;
       usuario         postgres    false    8         ,           0    0 
   TABLE sede    COMMENT     P   COMMENT ON TABLE usuario.sede IS 'Tabla para almacenar los datos de las sedes';
            usuario       postgres    false    214                    1255    262826 _   field_audit(usuario.sede, usuario.sede, character varying, text, character varying, text, text)    FUNCTION     �	  CREATE FUNCTION security.field_audit(_data_new usuario.sede, _data_old usuario.sede, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_sede;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('direccion_nuevo', _data_new.direccion)::jsonb;
				_datos := _datos || json_build_object('telefono_nuevo', _data_new.telefono)::jsonb;
				_datos := _datos || json_build_object('id_admi_nuevo', _data_new.id_admi)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('direccion_anterior', _data_old.direccion)::jsonb;
				_datos := _datos || json_build_object('telefono_anterior', _data_old.telefono)::jsonb;
				_datos := _datos || json_build_object('id_admi_anterior', _data_old.id_admi)::jsonb;
				
		ELSE
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.direccion <> _data_new.direccion
				THEN _datos := _datos || json_build_object('direccion_anterior', _data_old.direccion, 'direccion_nuevo', _data_new.direccion)::jsonb;
			END IF;
			IF _data_old.telefono <> _data_new.telefono
				THEN _datos := _datos || json_build_object('telefono_anterior', _data_old.telefono, 'telefono_nuevo', _data_new.telefono)::jsonb;
			END IF;
			IF _data_old.id_admi <> _data_new.id_admi
				THEN _datos := _datos || json_build_object('id_admi_anterior', _data_old.id_admi, 'id_admi_nuevo', _data_new.id_admi)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'sede',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.sede, _data_old usuario.sede, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    5    214    214    1         �            1259    262661    sedeatra    TABLE     �  CREATE TABLE usuario.sedeatra (
    id_sede_atra integer NOT NULL,
    descripcion text NOT NULL,
    cant_per integer NOT NULL,
    hora_ini time(6) without time zone NOT NULL,
    hora_fin time(6) without time zone NOT NULL,
    disponibilidad boolean NOT NULL,
    id_atraccion integer NOT NULL,
    id_sede integer NOT NULL,
    nombre text NOT NULL,
    precio integer NOT NULL
);
    DROP TABLE usuario.sedeatra;
       usuario         postgres    false    8         -           0    0    TABLE sedeatra    COMMENT     e   COMMENT ON TABLE usuario.sedeatra IS 'Tabla para almacenar los datos de las atracciones de la sede';
            usuario       postgres    false    216         &           1255    304434 g   field_audit(usuario.sedeatra, usuario.sedeatra, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new usuario.sedeatra, _data_old usuario.sedeatra, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_sede_atra;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_sede_atra_nuevo', _data_new.id_sede_atra)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				_datos := _datos || json_build_object('cant_per_nuevo', _data_new.cant_per)::jsonb;
				_datos := _datos || json_build_object('hora_ini_nuevo', _data_new.hora_ini)::jsonb;
				_datos := _datos || json_build_object('hora_fin_nuevo', _data_new.hora_fin)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('id_atraccion_nuevo', _data_new.id_atraccion)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('precio_nuevo', _data_new.precio)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_sede_atra_anterior', _data_old.id_sede_atra)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				_datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per)::jsonb;
				_datos := _datos || json_build_object('hora_ini_anterior', _data_old.hora_ini)::jsonb;
				_datos := _datos || json_build_object('hora_fin_anterior', _data_old.hora_fin)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('id_atraccion_anterior', _data_old.id_atraccion)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('precio_anterior', _data_old.precio)::jsonb;
				
		ELSE
			IF _data_old.id_sede_atra <> _data_new.id_sede_atra
				THEN _datos := _datos || json_build_object('id_sede_atra_anterior', _data_old.id_sede_atra, 'id_sede_atra_nuevo', _data_new.id_sede_atra)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			IF _data_old.cant_per <> _data_new.cant_per
				THEN _datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per, 'cant_per_nuevo', _data_new.cant_per)::jsonb;
			END IF;
			IF _data_old.hora_ini <> _data_new.hora_ini
				THEN _datos := _datos || json_build_object('hora_ini_anterior', _data_old.hora_ini, 'hora_ini_nuevo', _data_new.hora_ini)::jsonb;
			END IF;
			IF _data_old.hora_fin <> _data_new.hora_fin
				THEN _datos := _datos || json_build_object('hora_fin_anterior', _data_old.hora_fin, 'hora_fin_nuevo', _data_new.hora_fin)::jsonb;
			END IF;
			IF _data_old.disponibilidad <> _data_new.disponibilidad
				THEN _datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad, 'disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
			END IF;
			IF _data_old.id_atraccion <> _data_new.id_atraccion
				THEN _datos := _datos || json_build_object('id_atraccion_anterior', _data_old.id_atraccion, 'id_atraccion_nuevo', _data_new.id_atraccion)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.precio <> _data_new.precio
				THEN _datos := _datos || json_build_object('precio_anterior', _data_old.precio, 'precio_nuevo', _data_new.precio)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'sedeatra',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.sedeatra, _data_old usuario.sedeatra, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    216    1    5    216         �            1259    262673    sedehab    TABLE       CREATE TABLE usuario.sedehab (
    id_sede_hab integer NOT NULL,
    tipo_hab text NOT NULL,
    cant_per integer NOT NULL,
    disponibilidad boolean NOT NULL,
    id_habitacion integer NOT NULL,
    id_sede integer NOT NULL,
    precio integer NOT NULL
);
    DROP TABLE usuario.sedehab;
       usuario         postgres    false    8         .           0    0    TABLE sedehab    COMMENT     e   COMMENT ON TABLE usuario.sedehab IS 'Tabla para almacenar los datos de las habitaciones de la sede';
            usuario       postgres    false    218         #           1255    304455 e   field_audit(usuario.sedehab, usuario.sedehab, character varying, text, character varying, text, text)    FUNCTION     w  CREATE FUNCTION security.field_audit(_data_new usuario.sedehab, _data_old usuario.sedehab, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_sede_hab;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_sede_hab_nuevo', _data_new.id_sede_hab)::jsonb;
				_datos := _datos || json_build_object('tipo_hab_nuevo', _data_new.tipo_hab)::jsonb;
				_datos := _datos || json_build_object('cant_per_nuevo', _data_new.cant_per)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('id_habitacion_nuevo', _data_new.id_habitacion)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('precio_nuevo', _data_new.precio)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_sede_hab_anterior', _data_old.id_sede_hab)::jsonb;
				_datos := _datos || json_build_object('tipo_hab_anterior', _data_old.tipo_hab)::jsonb;
				_datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('id_habitacion_anterior', _data_old.id_habitacion)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('precio_anterior', _data_old.precio)::jsonb;
				
		ELSE
			IF _data_old.id_sede_hab <> _data_new.id_sede_hab
				THEN _datos := _datos || json_build_object('id_sede_hab_anterior', _data_old.id_sede_hab, 'id_sede_hab_nuevo', _data_new.id_sede_hab)::jsonb;
			END IF;
			IF _data_old.tipo_hab <> _data_new.tipo_hab
				THEN _datos := _datos || json_build_object('tipo_hab_anterior', _data_old.tipo_hab, 'tipo_hab_nuevo', _data_new.tipo_hab)::jsonb;
			END IF;
			IF _data_old.cant_per <> _data_new.cant_per
				THEN _datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per, 'cant_per_nuevo', _data_new.cant_per)::jsonb;
			END IF;
			IF _data_old.disponibilidad <> _data_new.disponibilidad
				THEN _datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad, 'disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
			END IF;
			IF _data_old.id_habitacion <> _data_new.id_habitacion
				THEN _datos := _datos || json_build_object('id_habitacion_anterior', _data_old.id_habitacion, 'id_habitacion_nuevo', _data_new.id_habitacion)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.precio <> _data_new.precio
				THEN _datos := _datos || json_build_object('precio_anterior', _data_old.precio, 'precio_nuevo', _data_new.precio)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'sedehab',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.sedehab, _data_old usuario.sedehab, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    5    218    218    1         �            1259    221305    usuario    TABLE     3  CREATE TABLE usuario.usuario (
    doc_identidad bigint NOT NULL,
    nombre text NOT NULL,
    apellido text NOT NULL,
    edad integer NOT NULL,
    num_cel bigint NOT NULL,
    correo_elec text NOT NULL,
    contrasena text NOT NULL,
    id_rol integer NOT NULL,
    session text,
    id_sede integer
);
    DROP TABLE usuario.usuario;
       usuario         postgres    false    8         /           0    0    TABLE usuario    COMMENT     H   COMMENT ON TABLE usuario.usuario IS 'Tabla para los datos del usuario';
            usuario       postgres    false    198                     1255    254188 e   field_audit(usuario.usuario, usuario.usuario, character varying, text, character varying, text, text)    FUNCTION     Z  CREATE FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.doc_identidad;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('apellido_nuevo', _data_new.apellido)::jsonb;
				_datos := _datos || json_build_object('edad_nuevo', _data_new.edad)::jsonb;
				_datos := _datos || json_build_object('num_cel_nuevo', _data_new.num_cel)::jsonb;
				_datos := _datos || json_build_object('correo_elec_nuevo', _data_new.correo_elec)::jsonb;
				_datos := _datos || json_build_object('contrasena_nuevo', _data_new.contrasena)::jsonb;
				_datos := _datos || json_build_object('id_rol_nuevo', _data_new.id_rol)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('apellido_anterior', _data_old.apellido)::jsonb;
				_datos := _datos || json_build_object('edad_anterior', _data_old.edad)::jsonb;
				_datos := _datos || json_build_object('num_cel_anterior', _data_old.num_cel)::jsonb;
				_datos := _datos || json_build_object('correo_elec_anterior', _data_old.correo_elec)::jsonb;
				_datos := _datos || json_build_object('contrasena_anterior', _data_old.contrasena)::jsonb;
				_datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				
		ELSE
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.apellido <> _data_new.apellido
				THEN _datos := _datos || json_build_object('apellido_anterior', _data_old.apellido, 'apellido_nuevo', _data_new.apellido)::jsonb;
			END IF;
			IF _data_old.edad <> _data_new.edad
				THEN _datos := _datos || json_build_object('edad_anterior', _data_old.edad, 'edad_nuevo', _data_new.edad)::jsonb;
			END IF;
			IF _data_old.num_cel <> _data_new.num_cel
				THEN _datos := _datos || json_build_object('num_cel_anterior', _data_old.num_cel, 'num_cel_nuevo', _data_new.num_cel)::jsonb;
			END IF;
			IF _data_old.correo_elec <> _data_new.correo_elec
				THEN _datos := _datos || json_build_object('correo_elec_anterior', _data_old.correo_elec, 'correo_elec_nuevo', _data_new.correo_elec)::jsonb;
			END IF;
			IF _data_old.contrasena <> _data_new.contrasena
				THEN _datos := _datos || json_build_object('contrasena_anterior', _data_old.contrasena, 'contrasena_nuevo', _data_new.contrasena)::jsonb;
			END IF;
			IF _data_old.id_rol <> _data_new.id_rol
				THEN _datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol, 'id_rol_nuevo', _data_new.id_rol)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'usuario',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    1    198    198    5                    1255    304347    f_consulta_clien(bigint)    FUNCTION     \  CREATE FUNCTION usuario.f_consulta_clien(_doc_identidad bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	filtro integer;
		BEGIN
			filtro := (SELECT id_rol FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
			IF filtro = 4 
			THEN 
				RETURN QUERY
				SELECT 
					uu.*
				FROM 
					usuario.usuario uu 
				WHERE doc_identidad = _doc_identidad;
			ELSE 
				RETURN QUERY SELECT 
					1::BIGINT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					1::BIGINT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					''::TEXT,
					1::INTEGER;
			END IF;
		END
$$;
 ?   DROP FUNCTION usuario.f_consulta_clien(_doc_identidad bigint);
       usuario       postgres    false    1    198    8         �            1255    304346    f_consulta_emple_clien(bigint)    FUNCTION     r  CREATE FUNCTION usuario.f_consulta_emple_clien(_doc_identidad bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	filtro integer;
		BEGIN
			filtro := (SELECT id_rol FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
			IF (filtro = 3) OR filtro = 4 
			THEN 
				RETURN QUERY
				SELECT 
					uu.*
				FROM 
					usuario.usuario uu 
				WHERE doc_identidad = _doc_identidad;
			ELSE 
				RETURN QUERY SELECT 
					1::BIGINT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					1::BIGINT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					''::TEXT,
					1::INTEGER;
			END IF;
		END
$$;
 E   DROP FUNCTION usuario.f_consulta_emple_clien(_doc_identidad bigint);
       usuario       postgres    false    8    198    1                    1255    304371    f_consulta_reserva(integer)    FUNCTION     ;  CREATE FUNCTION usuario.f_consulta_reserva(_id_reserva integer) RETURNS SETOF usuario.reserva_habitacion
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	filtro integer;
		BEGIN
			filtro := (SELECT id_estado FROM usuario.reserva WHERE id_reserva = _id_reserva);
			IF filtro = 1
			THEN 
				RETURN QUERY
				SELECT 
					ur.*
				FROM 
					usuario.reserva ur 
				WHERE id_reserva = _id_reserva;
			ELSE 
				RETURN QUERY SELECT 
					-1::INTEGER,
					''::TIMESTAMP,
					''::TIMESTAMP,
					''::JSON,
					1::INTEGER,
					1::INTEGER,
					1::INTEGER;
			END IF;
		END
$$;
 ?   DROP FUNCTION usuario.f_consulta_reserva(_id_reserva integer);
       usuario       postgres    false    8    1    212         �            1255    271078    f_eliminar_atraccion(integer)    FUNCTION     �   CREATE FUNCTION usuario.f_eliminar_atraccion(_id_atraccion integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		DELETE FROM usuario.atraccion
		WHERE id_atraccion = _id_atraccion;
	END;
$$;
 C   DROP FUNCTION usuario.f_eliminar_atraccion(_id_atraccion integer);
       usuario       postgres    false    8    1                    1255    304444 "   f_eliminar_atraccion_sede(integer)    FUNCTION     �   CREATE FUNCTION usuario.f_eliminar_atraccion_sede(_id_atraccion integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		DELETE FROM usuario.sedeatra
		WHERE id_sede_atra = _id_atraccion;
	END;
$$;
 H   DROP FUNCTION usuario.f_eliminar_atraccion_sede(_id_atraccion integer);
       usuario       postgres    false    8    1         �            1255    295642    f_eliminar_habitacion(integer)    FUNCTION     �   CREATE FUNCTION usuario.f_eliminar_habitacion(_id_habitacion integer) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		DELETE FROM usuario.habitacion WHERE id_habitacion = _id_habitacion;
	END;
$$;
 E   DROP FUNCTION usuario.f_eliminar_habitacion(_id_habitacion integer);
       usuario       postgres    false    8    198    1                    1255    304449 #   f_eliminar_habitacion_sede(integer)    FUNCTION     �   CREATE FUNCTION usuario.f_eliminar_habitacion_sede(_id_hab integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		DELETE FROM usuario.sedehab
		WHERE id_sede_hab = _id_hab;
	END;
$$;
 C   DROP FUNCTION usuario.f_eliminar_habitacion_sede(_id_hab integer);
       usuario       postgres    false    1    8         
           1255    295632    f_eliminar_sede(integer)    FUNCTION     �  CREATE FUNCTION usuario.f_eliminar_sede(_id_sede integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad int;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE id_sede = _id_sede);
		IF _cantidad = 0
		THEN 
			DELETE FROM usuario.sede WHERE id_sede = _id_sede;
			RETURN QUERY SELECT true;
		ELSE 
			RETURN QUERY SELECT false;
		END IF;
	END
$$;
 9   DROP FUNCTION usuario.f_eliminar_sede(_id_sede integer);
       usuario       postgres    false    1    8         $           1255    287441    f_eliminar_usuario(bigint)    FUNCTION     �  CREATE FUNCTION usuario.f_eliminar_usuario(_doc_identidad bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad int;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.sede WHERE id_admi = _doc_identidad);
		IF _cantidad = 0
			THEN 
				DELETE FROM usuario.usuario
				WHERE doc_identidad = _doc_identidad;
			RETURN QUERY SELECT true;
		ELSE
			RETURN QUERY SELECT false;
		END IF;
	END;
$$;
 A   DROP FUNCTION usuario.f_eliminar_usuario(_doc_identidad bigint);
       usuario       postgres    false    8    1         3           1255    304838 (   f_entregar_reserva_atr(integer, integer)    FUNCTION     i  CREATE FUNCTION usuario.f_entregar_reserva_atr(_id_sede_atr integer, _id_reserva integer) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		UPDATE usuario.sedeatra
			SET 
				disponibilidad = true
			WHERE 
				id_sede_atra = _id_sede_atr;
		UPDATE usuario.reserva_atraccion
			SET 
				id_estado = 2
			WHERE 
				id_reserva = _id_reserva;
	END
$$;
 Y   DROP FUNCTION usuario.f_entregar_reserva_atr(_id_sede_atr integer, _id_reserva integer);
       usuario       postgres    false    8    1                    1255    304808 %   f_entregar_reservar(integer, integer)    FUNCTION     e  CREATE FUNCTION usuario.f_entregar_reservar(_id_sede_hab integer, _id_reserva integer) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		UPDATE usuario.sedehab
			SET 
				disponibilidad = true
			WHERE 
				id_sede_hab = _id_sede_hab;
		UPDATE usuario.reserva_habitacion
			SET 
				id_estado = 2
			WHERE 
				id_reserva = _id_reserva;
	END
$$;
 V   DROP FUNCTION usuario.f_entregar_reservar(_id_sede_hab integer, _id_reserva integer);
       usuario       postgres    false    1    8         !           1255    279250 k   f_insertar_atraccion(text, integer, text, integer, boolean, time without time zone, time without time zone)    FUNCTION     �	  CREATE FUNCTION usuario.f_insertar_atraccion(_nombre text, _precio_per integer, _descripcion text, _cant_per integer, _disponibilidad boolean, _hora_ini time without time zone, _hora_fin time without time zone) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		IF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) = (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) <= 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					INSERT INTO usuario.atraccion
						(		
						nombre,
						precio_per,
						descripcion,
						cant_per,
						disponibilidad,
						hora_inicio,
						hora_fin
						)
					VALUES 
						(	
						_nombre,
						_precio_per,
						_descripcion,
						_cant_per,
						_disponibilidad,
						_hora_ini,
						_hora_fin
						);
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		ELSEIF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) > (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) <= 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					INSERT INTO usuario.atraccion
						(		
						nombre,
						precio_per,
						descripcion,
						cant_per,
						disponibilidad,
						hora_inicio,
						hora_fin
						)
					VALUES 
						(	
						_nombre,
						_precio_per,
						_descripcion,
						_cant_per,
						_disponibilidad,
						_hora_ini,
						_hora_fin
						);
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		ELSEIF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) < (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) < 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					INSERT INTO usuario.atraccion
						(		
						nombre,
						precio_per,
						descripcion,
						cant_per,
						disponibilidad,
						hora_inicio,
						hora_fin
						)
					VALUES 
						(	
						_nombre,
						_precio_per,
						_descripcion,
						_cant_per,
						_disponibilidad,
						_hora_ini,
						_hora_fin
						);
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		END IF;
	END
$$;
 �   DROP FUNCTION usuario.f_insertar_atraccion(_nombre text, _precio_per integer, _descripcion text, _cant_per integer, _disponibilidad boolean, _hora_ini time without time zone, _hora_fin time without time zone);
       usuario       postgres    false    8    1         (           1255    304396 4   f_insertar_atraccion_sede(integer, integer, integer)    FUNCTION     �  CREATE FUNCTION usuario.f_insertar_atraccion_sede(_id_atraccion integer, _id_sede integer, _precio integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
		_nombre text;
		_descripcion text;
		_cant integer;
		_hora_ini time without time zone;
		_hora_fin time without time zone;
		_disponibilidad boolean;
	BEGIN
		_nombre := (SELECT nombre FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_descripcion := (SELECT descripcion FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_cant := (SELECT cant_per FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_hora_ini := (SELECT hora_inicio FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_hora_fin := (SELECT hora_fin FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_disponibilidad := (SELECT disponibilidad FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		
		INSERT INTO usuario.sedeatra
		(
			nombre,
			descripcion,
			cant_per,
			hora_ini,
			hora_fin,
			disponibilidad,
			id_atraccion,
			id_sede,
			precio
		)
		VALUES 
		(
			_nombre,
			_descripcion,
			_cant,
			_hora_ini,
			_hora_fin,
			_disponibilidad,
			_id_atraccion,
			_id_sede,
			_precio
		);
	END
$$;
 k   DROP FUNCTION usuario.f_insertar_atraccion_sede(_id_atraccion integer, _id_sede integer, _precio integer);
       usuario       postgres    false    1    8                    1255    304813 :   f_insertar_factura(bigint, integer, bigint, date, integer)    FUNCTION     �  CREATE FUNCTION usuario.f_insertar_factura(_total bigint, _id_sede_hab integer, _doc_identidad bigint, _fecha_sal date, _estado integer) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	precioo integer;
	_id_sede integer;
	BEGIN
		precioo := (SELECT precio FROM usuario.sedehab WHERE id_sede_hab = _id_sede_hab);
		_id_sede := (SELECT id_sede FROM usuario.sedehab WHERE id_sede_hab = _id_sede_hab);
		INSERT INTO usuario.factura_habitacion
		(
			total_pag,
			id_sedehab,
			doc_identidad,
			fecha_ent,
			id_sede,
			estado
		)
		VALUES 
		(
			_total * precioo,
			_id_sede_hab,
			_doc_identidad,
			_fecha_sal,
			_id_sede,
			_estado
		);
	END
$$;
 �   DROP FUNCTION usuario.f_insertar_factura(_total bigint, _id_sede_hab integer, _doc_identidad bigint, _fecha_sal date, _estado integer);
       usuario       postgres    false    1    8                    1255    304836 ?   f_insertar_factura_atra(bigint, integer, bigint, date, integer)    FUNCTION     �  CREATE FUNCTION usuario.f_insertar_factura_atra(_total bigint, _id_sede_atr integer, _doc_identidad bigint, _fecha_sal date, _estado integer) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	precioo integer;
	_id_sede integer;
	BEGIN
		precioo := (SELECT precio FROM usuario.sedeatra WHERE id_sede_atra = _id_sede_atr);
		_id_sede := (SELECT id_sede FROM usuario.sedeatra WHERE id_sede_atra = _id_sede_atr);
		INSERT INTO usuario.factura_atraccion
		(
			total_pag,
			id_sedeatr,
			doc_identidad,
			fecha_ent,
			id_sede,
			estado
		)
		VALUES 
		(
			_total * precioo,
			_id_sede_atr,
			_doc_identidad,
			_fecha_sal,
			_id_sede,
			_estado
		);
	END
$$;
 �   DROP FUNCTION usuario.f_insertar_factura_atra(_total bigint, _id_sede_atr integer, _doc_identidad bigint, _fecha_sal date, _estado integer);
       usuario       postgres    false    8    1                    1255    295639 6   f_insertar_habitacion(text, integer, integer, boolean)    FUNCTION     �  CREATE FUNCTION usuario.f_insertar_habitacion(_tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	BEGIN
		INSERT INTO usuario.habitacion
		(
			tipo_habitacion,
			cant_per,
			precio_hab,
			disponibilidad
		)
		VALUES 
		(
			_tipo_hab,
			_cant_per,
			_precio_hab,
			_disponibilidad
		);
	END
$$;
 ~   DROP FUNCTION usuario.f_insertar_habitacion(_tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean);
       usuario       postgres    false    8    1         ,           1255    304446 5   f_insertar_habitacion_sede(integer, integer, integer)    FUNCTION     �  CREATE FUNCTION usuario.f_insertar_habitacion_sede(_id_habi integer, _id_sede integer, _precio integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
		_tipo_hab text;
		_cant integer;
		_disponibilidad boolean;
	BEGIN
		_tipo_hab := (SELECT tipo_habitacion FROM usuario.habitacion WHERE id_habitacion = _id_habi);
		_cant := (SELECT cant_per FROM usuario.habitacion WHERE id_habitacion = _id_habi);
		_disponibilidad := (SELECT disponibilidad FROM usuario.habitacion WHERE id_habitacion = _id_habi);
		
		INSERT INTO usuario.sedehab
		(
			tipo_hab,
			cant_per,
			disponibilidad,
			id_habitacion,
			id_sede,
			precio
		)
		VALUES 
		(
			_tipo_hab,
			_cant,
			_disponibilidad,
			_id_habi,
			_id_sede,
			_precio
		);
	END
$$;
 g   DROP FUNCTION usuario.f_insertar_habitacion_sede(_id_habi integer, _id_sede integer, _precio integer);
       usuario       postgres    false    1    8                    1255    295629 +   f_insertar_sede(text, text, bigint, bigint)    FUNCTION     U  CREATE FUNCTION usuario.f_insertar_sede(_nombre text, _direccion text, _telefono bigint, _id_admi bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	BEGIN
		INSERT INTO usuario.sede
		(
			nombre,
			direccion,
			telefono,
			id_admi
		)
		VALUES 
		(
			_nombre,
			_direccion,
			_telefono,
			_id_admi
		);
	END
$$;
 i   DROP FUNCTION usuario.f_insertar_sede(_nombre text, _direccion text, _telefono bigint, _id_admi bigint);
       usuario       postgres    false    8    1                    1255    262824 T   f_insertaru(bigint, text, text, integer, bigint, text, text, integer, text, integer)    FUNCTION     p  CREATE FUNCTION usuario.f_insertaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _id_rol integer, _session text, _id_sede integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad integer;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		IF _cantidad = 0 
			THEN 
				INSERT INTO usuario.usuario
				(
					doc_identidad,
					nombre,
					apellido,
					edad,
					num_Cel,
					correo_Elec,
					contrasena,
					id_rol,
					session,
					id_sede
				)
				VALUES 
				(
					_doc_identidad,
					_nombre,
					_apellido,
					_edad,
					_num_cel,
					_correo_elec,
					_contrasena,
					_id_rol,
					_session,
					_id_sede
				);
			RETURN QUERY SELECT true;
		ELSE
			RETURN QUERY SELECT false;
		END IF;
	END
$$;
 �   DROP FUNCTION usuario.f_insertaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _id_rol integer, _session text, _id_sede integer);
       usuario       postgres    false    1    8                    1255    271077    f_leer_atraccion()    FUNCTION     �   CREATE FUNCTION usuario.f_leer_atraccion() RETURNS SETOF usuario.atraccion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			ua.*
		FROM 
			usuario.atraccion ua;
	END;
$$;
 *   DROP FUNCTION usuario.f_leer_atraccion();
       usuario       postgres    false    206    8    1         �            1259    304435    v_atraccion_sede    VIEW     B  CREATE VIEW usuario.v_atraccion_sede AS
 SELECT 0 AS id_sede_atraccion,
    ''::text AS nombre,
    ''::text AS descripcion,
    0 AS cant_per,
    0 AS precio,
    '00:00:00'::time without time zone AS hora_ini,
    '00:00:00'::time without time zone AS hora_fin,
    true AS disponibilidad,
    ''::text AS nombre_sede;
 $   DROP VIEW usuario.v_atraccion_sede;
       usuario       postgres    false    8         '           1255    304440    f_leer_atraccion_sede()    FUNCTION     �  CREATE FUNCTION usuario.f_leer_atraccion_sede() RETURNS SETOF usuario.v_atraccion_sede
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
		
			us.id_sede_atra,
			us.nombre,
			us.descripcion,
			us.cant_per,
			us.precio,
			us.hora_ini,
			us.hora_fin,
			us.disponibilidad,
			uu.nombre
		FROM usuario.sedeatra us
			JOIN usuario.sede uu ON uu.id_sede = us.id_sede;
	END;
$$;
 /   DROP FUNCTION usuario.f_leer_atraccion_sede();
       usuario       postgres    false    223    8    1         )           1255    304443    f_leer_atraccion_sede(bigint)    FUNCTION     $  CREATE FUNCTION usuario.f_leer_atraccion_sede(_doc_identidad bigint) RETURNS SETOF usuario.v_atraccion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	_sede int;
	BEGIN 
		_sede := (SELECT id_sede FROM usuario.usuario WHERE doc_identidad =_doc_identidad);
		RETURN QUERY 
		SELECT 
			us.id_sede_atra,
			us.nombre,
			us.descripcion,
			us.cant_per,
			us.precio,
			us.hora_ini,
			us.hora_fin,
			us.disponibilidad,
			uu.nombre
		FROM usuario.sedeatra us
			JOIN usuario.sede uu ON uu.id_sede = us.id_sede
		WHERE us.id_sede = _sede;
	END;
$$;
 D   DROP FUNCTION usuario.f_leer_atraccion_sede(_doc_identidad bigint);
       usuario       postgres    false    1    223    8         -           1255    304464 &   f_leer_atraccion_sede_consulta(bigint)    FUNCTION     �  CREATE FUNCTION usuario.f_leer_atraccion_sede_consulta(_doc_identidad bigint) RETURNS SETOF usuario.v_atraccion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	BEGIN 
		RETURN QUERY 
		SELECT 
			us.id_sede_atra,
			us.nombre,
			us.descripcion,
			us.cant_per,
			us.precio,
			us.hora_ini,
			us.hora_fin,
			us.disponibilidad,
			uu.nombre
		FROM usuario.sedeatra us
			JOIN usuario.sede uu ON uu.id_sede = us.id_sede
		WHERE us.id_atraccion = _doc_identidad;
	END;
$$;
 M   DROP FUNCTION usuario.f_leer_atraccion_sede_consulta(_doc_identidad bigint);
       usuario       postgres    false    223    1    8         �            1259    304867 	   v_factura    VIEW     �   CREATE VIEW usuario.v_factura AS
 SELECT 0 AS id_reserva,
    (0)::bigint AS total,
    ''::text AS sede_atra,
    (0)::bigint AS doc_identidad,
    '2019-01-01'::date AS fecha_sal,
    ''::text AS sede,
    ''::text AS estado;
    DROP VIEW usuario.v_factura;
       usuario       postgres    false    8         5           1255    304871    f_leer_factura_atraccion()    FUNCTION     "  CREATE FUNCTION usuario.f_leer_factura_atraccion() RETURNS SETOF usuario.v_factura
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			ur.id_factura,
			ur.total_pag,
			us.nombre,
			uu.doc_identidad,
			ur.fecha_ent,
			uq.nombre,
			ue.descripcion
		FROM usuario.factura_atraccion ur
			JOIN usuario.estado_fac ue ON ur.estado = ue.id
			JOIN usuario.sedeatra us ON ur.id_sedeatr = us.id_sede_atra
			JOIN usuario.usuario uu ON ur.doc_identidad = uu.doc_identidad
			JOIN usuario.sede uq ON ur.id_sede = uq.id_sede;
	END;
$$;
 2   DROP FUNCTION usuario.f_leer_factura_atraccion();
       usuario       postgres    false    8    1    231         2           1255    304882 $   f_leer_factura_atraccion_re(integer)    FUNCTION     n  CREATE FUNCTION usuario.f_leer_factura_atraccion_re(_id_reserva integer) RETURNS SETOF usuario.v_factura
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			ur.id_factura,
			ur.total_pag,
			us.nombre,
			uu.doc_identidad,
			ur.fecha_ent,
			uq.nombre,
			ue.descripcion
		FROM usuario.factura_atraccion ur
			JOIN usuario.estado_fac ue ON ur.estado = ue.id
			JOIN usuario.sedeatra us ON ur.id_sedeatr = us.id_sede_atra
			JOIN usuario.usuario uu ON ur.doc_identidad = uu.doc_identidad
			JOIN usuario.sede uq ON ur.id_sede = uq.id_sede
		WHERE ur.estado = 2 AND ur.id_factura = _id_reserva;
	END;
$$;
 H   DROP FUNCTION usuario.f_leer_factura_atraccion_re(_id_reserva integer);
       usuario       postgres    false    1    8    231         �            1255    295643    f_leer_habitacion()    FUNCTION     �   CREATE FUNCTION usuario.f_leer_habitacion() RETURNS SETOF usuario.habitacion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uh.*
		FROM 
			usuario.habitacion uh;
	END;
$$;
 +   DROP FUNCTION usuario.f_leer_habitacion();
       usuario       postgres    false    1    210    8         �            1259    304450    v_habitacion_sede    VIEW     �   CREATE VIEW usuario.v_habitacion_sede AS
 SELECT 0 AS id_sede_habitacion,
    ''::text AS tipo_hab,
    0 AS cant_per,
    true AS disponibilidad,
    0 AS precio,
    ''::text AS nombre_sede;
 %   DROP VIEW usuario.v_habitacion_sede;
       usuario       postgres    false    8         +           1255    304454    f_leer_habitacion_sede(bigint)    FUNCTION     �  CREATE FUNCTION usuario.f_leer_habitacion_sede(_doc_identidad bigint) RETURNS SETOF usuario.v_habitacion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	_sede int;
	BEGIN 
		_sede := (SELECT id_sede FROM usuario.usuario WHERE doc_identidad =_doc_identidad);
		RETURN QUERY 
		SELECT 
			uh.id_sede_hab,
			uh.tipo_hab,
			uh.cant_per,
			uh.disponibilidad,
			uh.precio,
			uu.nombre
		FROM usuario.sedehab uh
			JOIN usuario.sede uu ON uu.id_sede = uh.id_sede
		WHERE uh.id_sede = _sede;
	END;
$$;
 E   DROP FUNCTION usuario.f_leer_habitacion_sede(_doc_identidad bigint);
       usuario       postgres    false    8    224    1         .           1255    304465 '   f_leer_habitacion_sede_consulta(bigint)    FUNCTION     �  CREATE FUNCTION usuario.f_leer_habitacion_sede_consulta(_doc_identidad bigint) RETURNS SETOF usuario.v_habitacion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	BEGIN 
		RETURN QUERY 
		SELECT 
			uh.id_sede_hab,
			uh.tipo_hab,
			uh.cant_per,
			uh.disponibilidad,
			uh.precio,
			uu.nombre
		FROM usuario.sedehab uh
			JOIN usuario.sede uu ON uu.id_sede = uh.id_sede
		WHERE uh.id_habitacion = _doc_identidad;
	END;
$$;
 N   DROP FUNCTION usuario.f_leer_habitacion_sede_consulta(_doc_identidad bigint);
       usuario       postgres    false    1    8    224         /           1255    304770 *   f_leer_habitacion_sede_disponibles(bigint)    FUNCTION       CREATE FUNCTION usuario.f_leer_habitacion_sede_disponibles(_doc_identidad bigint) RETURNS SETOF usuario.v_habitacion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	_sede int;
	BEGIN 
		_sede := (SELECT id_sede FROM usuario.usuario WHERE doc_identidad =_doc_identidad);
		RETURN QUERY 
		SELECT 
			uh.id_sede_hab,
			uh.tipo_hab,
			uh.cant_per,
			uh.disponibilidad,
			uh.precio,
			uu.nombre
		FROM usuario.sedehab uh
			JOIN usuario.sede uu ON uu.id_sede = uh.id_sede
		WHERE uh.id_sede = _sede AND uh.disponibilidad = true;
	END;
$$;
 Q   DROP FUNCTION usuario.f_leer_habitacion_sede_disponibles(_doc_identidad bigint);
       usuario       postgres    false    1    224    8                    1255    304781    f_leer_reserva()    FUNCTION     �   CREATE FUNCTION usuario.f_leer_reserva() RETURNS SETOF usuario.reserva_habitacion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			ur.*
		FROM 
			usuario.reserva_habitacion ur;
	END;
$$;
 (   DROP FUNCTION usuario.f_leer_reserva();
       usuario       postgres    false    212    8    1         0           1255    304837    f_leer_reserva_atra()    FUNCTION     �   CREATE FUNCTION usuario.f_leer_reserva_atra() RETURNS SETOF usuario.reserva_atraccion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			ur.*
		FROM 
			usuario.reserva_atraccion ur;
	END;
$$;
 -   DROP FUNCTION usuario.f_leer_reserva_atra();
       usuario       postgres    false    8    226    1         �            1259    304840    v_reserva_atraccion    VIEW     �   CREATE VIEW usuario.v_reserva_atraccion AS
 SELECT 0 AS id_reserva,
    '2019-01-01'::date AS fecha_ent,
    '2019-01-01'::date AS fecha_sal,
    ''::text AS estado,
    ''::text AS sede_atra,
    (0)::bigint AS doc_identidad,
    ''::text AS sede;
 '   DROP VIEW usuario.v_reserva_atraccion;
       usuario       postgres    false    8         1           1255    304845    f_leer_reserva_atraccion()    FUNCTION     4  CREATE FUNCTION usuario.f_leer_reserva_atraccion() RETURNS SETOF usuario.v_reserva_atraccion
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			ur.id_reserva,
			ur.fecha_ent,
			ur.fecha_sal,
			ue.descripcion,
			us.nombre,
			uu.doc_identidad,
			uq.nombre
		FROM usuario.reserva_atraccion ur
			JOIN usuario.estado ue ON ur.id_estado = ue.id_estado
			JOIN usuario.sedeatra us ON ur.id_sede_atra = us.id_sede_atra
			JOIN usuario.usuario uu ON ur.doc_identidad = uu.doc_identidad
			JOIN usuario.sede uq ON ur.id_sede = uq.id_sede;
	END;
$$;
 2   DROP FUNCTION usuario.f_leer_reserva_atraccion();
       usuario       postgres    false    229    1    8         4           1255    304846    f_leer_reserva_habitacion()    FUNCTION     5  CREATE FUNCTION usuario.f_leer_reserva_habitacion() RETURNS SETOF usuario.v_reserva_atraccion
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			ur.id_reserva,
			ur.fecha_ent,
			ur.fecha_sal,
			ue.descripcion,
			us.tipo_hab,
			uu.doc_identidad,
			uq.nombre
		FROM usuario.reserva_habitacion ur
			JOIN usuario.estado ue ON ur.id_estado = ue.id_estado
			JOIN usuario.sedehab us ON ur.id_sede_hab = us.id_sede_hab
			JOIN usuario.usuario uu ON ur.doc_identidad = uu.doc_identidad
			JOIN usuario.sede uq ON ur.id_sede = uq.id_sede;
	END;
$$;
 3   DROP FUNCTION usuario.f_leer_reserva_habitacion();
       usuario       postgres    false    1    8    229         �            1259    295633    v_sede    VIEW     �   CREATE VIEW usuario.v_sede AS
 SELECT 0 AS id_sede,
    ''::text AS nombre,
    ''::text AS direccion,
    (0)::bigint AS telefono,
    (0)::bigint AS id_admi,
    ''::text AS nombre_admi;
    DROP VIEW usuario.v_sede;
       usuario       postgres    false    8         �            1255    295638    f_leer_se()    FUNCTION     Y  CREATE FUNCTION usuario.f_leer_se() RETURNS SETOF usuario.v_sede
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			us.id_sede,
			us.nombre,
			us.direccion,
			us.telefono,
			uu.doc_identidad,
			uu.nombre
		FROM usuario.sede us		
			JOIN usuario.usuario uu ON uu.doc_identidad = us.id_admi
		WHERE uu.id_rol = 2;
	END;
$$;
 #   DROP FUNCTION usuario.f_leer_se();
       usuario       postgres    false    8    1    220         �            1255    271079    f_leer_sede(bigint)    FUNCTION     �   CREATE FUNCTION usuario.f_leer_sede(_doc_identidad bigint) RETURNS SETOF usuario.sede
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			us.*
		FROM 
			usuario.sede us;
	END;
$$;
 :   DROP FUNCTION usuario.f_leer_sede(_doc_identidad bigint);
       usuario       postgres    false    8    214    1         �            1259    262849 	   v_usuario    VIEW       CREATE VIEW usuario.v_usuario AS
 SELECT (0)::bigint AS doc_identidad,
    ''::text AS nombre,
    ''::text AS apellido,
    (0)::bigint AS num_celular,
    ''::text AS cor_electronico,
    0 AS id_rol,
    ''::text AS rol_nombre,
    0 AS id_sede,
    ''::text AS sede_nombre;
    DROP VIEW usuario.v_usuario;
       usuario       postgres    false    8                    1255    262853    f_leer_usuario_admi()    FUNCTION     �  CREATE FUNCTION usuario.f_leer_usuario_admi() RETURNS SETOF usuario.v_usuario
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			uu.doc_identidad,
			uu.nombre,
			uu.apellido,
			uu.num_cel,
			uu.correo_elec,
			uu.id_rol,
			ur.nombre,
			uu.id_sede,
			us.nombre
		FROM usuario.usuario uu 
			JOIN usuario.rol ur ON ur.id_rol = uu.id_rol
			JOIN usuario.sede us ON us.id_sede = uu.id_sede
		WHERE uu.id_rol = 2;
	END;
$$;
 -   DROP FUNCTION usuario.f_leer_usuario_admi();
       usuario       postgres    false    8    219    1                    1255    304345    f_leer_usuario_emple()    FUNCTION     �  CREATE FUNCTION usuario.f_leer_usuario_emple() RETURNS SETOF usuario.v_usuario
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			uu.doc_identidad,
			uu.nombre,
			uu.apellido,
			uu.num_cel,
			uu.correo_elec,
			uu.id_rol,
			ur.nombre,
			uu.id_sede,
			us.nombre
		FROM usuario.usuario uu 
			JOIN usuario.rol ur ON ur.id_rol = uu.id_rol
			JOIN usuario.sede us ON us.id_sede = uu.id_sede
		WHERE uu.id_rol = 3;
	END;
$$;
 .   DROP FUNCTION usuario.f_leer_usuario_emple();
       usuario       postgres    false    219    1    8         �            1255    262832 	   f_leerr()    FUNCTION     �   CREATE FUNCTION usuario.f_leerr() RETURNS SETOF usuario.rol
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			*
		FROM usuario.rol;
	END;
$$;
 !   DROP FUNCTION usuario.f_leerr();
       usuario       postgres    false    1    199    8         �            1255    254295 
   f_leertu()    FUNCTION     �   CREATE FUNCTION usuario.f_leertu() RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu;
	END;
$$;
 "   DROP FUNCTION usuario.f_leertu();
       usuario       postgres    false    8    198    1                    1255    262857    f_leeru(bigint)    FUNCTION     �   CREATE FUNCTION usuario.f_leeru(_doc_identidad bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
	RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu 
		WHERE doc_identidad = _doc_identidad;
	END
$$;
 6   DROP FUNCTION usuario.f_leeru(_doc_identidad bigint);
       usuario       postgres    false    198    1    8         �            1255    262829 
   f_leerua()    FUNCTION     �   CREATE FUNCTION usuario.f_leerua() RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu 
		WHERE id_rol = 2;
	END;
$$;
 "   DROP FUNCTION usuario.f_leerua();
       usuario       postgres    false    1    198    8         �            1255    262834 
   f_leerue()    FUNCTION     �   CREATE FUNCTION usuario.f_leerue() RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu 
		WHERE id_rol = 3;
	END;
$$;
 "   DROP FUNCTION usuario.f_leerue();
       usuario       postgres    false    198    8    1         �            1255    221404    f_login(bigint, text)    FUNCTION     ;  CREATE FUNCTION usuario.f_login(_doc bigint, _contra text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu JOIN usuario.rol ur ON ur.id_rol = uu.id_rol
		WHERE 
			uu.doc_identidad = _doc
		AND
			uu.contrasena = _contra;
	END;
$$;
 :   DROP FUNCTION usuario.f_login(_doc bigint, _contra text);
       usuario       postgres    false    1    8    198         "           1255    287437 u   f_modificar_atraccion(integer, text, integer, text, integer, time without time zone, time without time zone, boolean)    FUNCTION     Q	  CREATE FUNCTION usuario.f_modificar_atraccion(_id_atraccion integer, _nombre text, _precio_per integer, _descripcion text, _cant_per integer, _hora_ini time without time zone, _hora_fin time without time zone, _disponibilidad boolean) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		IF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) = (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) <= 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					UPDATE usuario.atraccion
					SET 
						nombre = _nombre,
						precio_per = _precio_per,
						descripcion = _descripcion,
						cant_per = _cant_per,
						hora_inicio = _hora_ini,
						hora_fin = _hora_fin,
						disponibilidad = _disponibilidad
					WHERE 
						id_atraccion = _id_atraccion;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		ELSEIF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) > (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) <= 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					UPDATE usuario.atraccion
					SET 
						nombre = _nombre,
						precio_per = _precio_per,
						descripcion = _descripcion,
						cant_per = _cant_per,
						hora_inicio = _hora_ini,
						hora_fin = _hora_fin,
						disponibilidad = _disponibilidad
					WHERE 
						id_atraccion = _id_atraccion;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		ELSEIF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) < (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) < 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					UPDATE usuario.atraccion
					SET 
						nombre = _nombre,
						precio_per = _precio_per,
						descripcion = _descripcion,
						cant_per = _cant_per,
						hora_inicio = _hora_ini,
						hora_fin = _hora_fin,
						disponibilidad = _disponibilidad
					WHERE 
						id_atraccion = _id_atraccion;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		END IF;
	END
$$;
 �   DROP FUNCTION usuario.f_modificar_atraccion(_id_atraccion integer, _nombre text, _precio_per integer, _descripcion text, _cant_per integer, _hora_ini time without time zone, _hora_fin time without time zone, _disponibilidad boolean);
       usuario       postgres    false    8    1         *           1255    304447 ,   f_modificar_atraccion_sede(integer, integer)    FUNCTION     �   CREATE FUNCTION usuario.f_modificar_atraccion_sede(_id_atra integer, _precio integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		UPDATE usuario.sedeatra
			SET 
				precio = _precio
			WHERE 
				id_sede_atra = _id_atra;
	END
$$;
 U   DROP FUNCTION usuario.f_modificar_atraccion_sede(_id_atra integer, _precio integer);
       usuario       postgres    false    1    8                    1255    304878 !   f_modificar_factura_atra(integer)    FUNCTION       CREATE FUNCTION usuario.f_modificar_factura_atra(_id_factura integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		UPDATE usuario.factura_atraccion
			SET				
				fecha_ent = (select now()::date),
				estado = 2
			WHERE 
				id_factura = _id_factura;
	END
$$;
 E   DROP FUNCTION usuario.f_modificar_factura_atra(_id_factura integer);
       usuario       postgres    false    8    1                    1255    295640 @   f_modificar_habitacion(integer, text, integer, integer, boolean)    FUNCTION     �  CREATE FUNCTION usuario.f_modificar_habitacion(_id_habitacion integer, _tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		UPDATE usuario.habitacion
			SET 
				tipo_habitacion = _tipo_hab,
				cant_per = _cant_per,
				precio_hab = _precio_hab,
				disponibilidad = _disponibilidad
			WHERE 
				id_habitacion = _id_habitacion;
	END
$$;
 �   DROP FUNCTION usuario.f_modificar_habitacion(_id_habitacion integer, _tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean);
       usuario       postgres    false    8    1                    1255    304448 -   f_modificar_habitacion_sede(integer, integer)    FUNCTION     �   CREATE FUNCTION usuario.f_modificar_habitacion_sede(_id_hab integer, _precio integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		UPDATE usuario.sedehab
			SET 
				precio = _precio
			WHERE 
				id_sede_hab = _id_hab;
	END
$$;
 U   DROP FUNCTION usuario.f_modificar_habitacion_sede(_id_hab integer, _precio integer);
       usuario       postgres    false    8    1         	           1255    295630 5   f_modificar_sede(integer, text, text, bigint, bigint)    FUNCTION     r  CREATE FUNCTION usuario.f_modificar_sede(_id_sede integer, _nombre text, _direccion text, _telefono bigint, _id_admi bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	BEGIN
		UPDATE usuario.sede
			SET				
				nombre = _nombre,
				direccion = _direccion,
				telefono = _telefono,
				id_admi = _id_admi
			WHERE 
				id_sede = _id_sede;
	END
$$;
 |   DROP FUNCTION usuario.f_modificar_sede(_id_sede integer, _nombre text, _direccion text, _telefono bigint, _id_admi bigint);
       usuario       postgres    false    1    8                    1255    262848 L   f_modificar_usuario_admi(bigint, text, text, bigint, text, integer, integer)    FUNCTION     t  CREATE FUNCTION usuario.f_modificar_usuario_admi(_doc_identidad bigint, _nombre text, _apellido text, _num_cel bigint, _correo_elec text, _id_rol integer, _id_sede integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad integer;
	_doc bigint;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		_doc := (SELECT doc_identidad FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		IF _cantidad = 0 OR NOT _doc <> _doc_identidad
			THEN 
				UPDATE usuario.usuario
				SET 
					doc_identidad = _doc_identidad, 
					nombre = _nombre, 
					apellido = _apellido, 
					num_cel = _num_cel, 
					correo_elec = _correo_elec, 
					id_rol = _id_rol, 
					id_sede = _id_sede
				WHERE doc_identidad = _doc_identidad;
				RETURN QUERY SELECT true;
			ELSE
				RETURN QUERY SELECT false;
		END IF;
	END
$$;
 �   DROP FUNCTION usuario.f_modificar_usuario_admi(_doc_identidad bigint, _nombre text, _apellido text, _num_cel bigint, _correo_elec text, _id_rol integer, _id_sede integer);
       usuario       postgres    false    1    8                    1255    287440 C   f_modificaru(bigint, text, text, integer, bigint, text, text, text)    FUNCTION     e  CREATE FUNCTION usuario.f_modificaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _session text) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad integer;
	_doc bigint;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		_doc := (SELECT doc_identidad FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		IF _cantidad = 0 OR NOT _doc <> _doc_identidad
			THEN 
				UPDATE usuario.usuario
				SET doc_identidad = _doc_identidad, nombre = _nombre, apellido = _apellido, edad = _edad, num_Cel = _num_cel, correo_Elec = _correo_elec,
				contrasena = _contrasena, session = _session
				WHERE doc_identidad = _doc_identidad;
				RETURN QUERY SELECT true;
			ELSE
				RETURN QUERY SELECT false;
		END IF;
	END
$$;
 �   DROP FUNCTION usuario.f_modificaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _session text);
       usuario       postgres    false    1    8         %           1255    304795 0   f_reservar(date, date, integer, integer, bigint)    FUNCTION       CREATE FUNCTION usuario.f_reservar(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_hab integer, _doc_identidad bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_sede integer;
	_cantidad integer;
	_concu bool;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		_sede := (SELECT id_sede FROM usuario.sedehab WHERE id_sede_hab = _id_sede_hab);
		_concu := (SELECT disponibilidad FROM usuario.sedehab WHERE id_sede_hab = _id_sede_hab);
		IF (_cantidad = 1) AND (_concu = true)
			THEN 
				INSERT INTO usuario.reserva_habitacion
				(
					fecha_ent,
					fecha_sal,
					id_estado,
					id_sede_hab,
					doc_identidad,
					id_sede
				)
				VALUES 
				(
					_fecha_ent,
					_fecha_sal,
					_id_estado,
					_id_sede_hab,
					_doc_identidad,
					_sede
				);

				UPDATE usuario.sedehab
					SET 
						disponibilidad = false
					WHERE 
						id_sede_hab = _id_sede_hab;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
		END IF;
	END
$$;
 �   DROP FUNCTION usuario.f_reservar(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_hab integer, _doc_identidad bigint);
       usuario       postgres    false    8    1         7           1255    304839 5   f_reservar_atra(date, date, integer, integer, bigint)    FUNCTION     .  CREATE FUNCTION usuario.f_reservar_atra(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_atra integer, _doc_identidad bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_sede integer;
	_cantidad integer;
	_concu bool;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		_sede := (SELECT id_sede FROM usuario.sedeatra WHERE id_sede_atra = _id_sede_atra);
		_concu := (SELECT disponibilidad FROM usuario.sedeatra WHERE id_sede_atra = _id_sede_atra);
		IF (_cantidad = 1) AND (_concu = true)
			THEN 
				INSERT INTO usuario.reserva_atraccion
				(
					fecha_ent,
					fecha_sal,
					id_estado,
					id_sede_atra,
					doc_identidad,
					id_sede
				)
				VALUES 
				(
					_fecha_ent,
					_fecha_sal,
					_id_estado,
					_id_sede_atra,
					_doc_identidad,
					_sede
				);

				UPDATE usuario.sedeatra
					SET 
						disponibilidad = false
					WHERE 
						id_sede_atra = _id_sede_atra;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
		END IF;
	END
$$;
 �   DROP FUNCTION usuario.f_reservar_atra(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_atra integer, _doc_identidad bigint);
       usuario       postgres    false    8    1         �            1259    254122 	   auditoria    TABLE     !  CREATE TABLE security.auditoria (
    id bigint NOT NULL,
    fecha timestamp(4) without time zone,
    accion character varying(100),
    schema character varying(200),
    tabla character varying(200),
    session text,
    user_bd character varying(100),
    data jsonb,
    pk text
);
    DROP TABLE security.auditoria;
       security         postgres    false    5         0           0    0    TABLE auditoria    COMMENT     f   COMMENT ON TABLE security.auditoria IS 'Tabla para almacenar los datos de la auditoria del proyecto';
            security       postgres    false    203         �            1259    254120    auditoria_id_seq    SEQUENCE     �   CREATE SEQUENCE security.auditoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE security.auditoria_id_seq;
       security       postgres    false    203    5         1           0    0    auditoria_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE security.auditoria_id_seq OWNED BY security.auditoria.id;
            security       postgres    false    202         �            1259    303821    autenticacion_id_seq    SEQUENCE     �   CREATE SEQUENCE security.autenticacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE security.autenticacion_id_seq;
       security       postgres    false    5    222         2           0    0    autenticacion_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE security.autenticacion_id_seq OWNED BY security.autenticacion.id;
            security       postgres    false    221         �            1259    254177    function_db_view    VIEW     �  CREATE VIEW security.function_db_view AS
 SELECT pp.proname AS b_function,
    oidvectortypes(pp.proargtypes) AS b_type_parameters
   FROM (pg_proc pp
     JOIN pg_namespace pn ON ((pn.oid = pp.pronamespace)))
  WHERE ((pn.nspname)::text <> ALL (ARRAY[('pg_catalog'::character varying)::text, ('information_schema'::character varying)::text, ('admin_control'::character varying)::text, ('vial'::character varying)::text]));
 %   DROP VIEW security.function_db_view;
       security       postgres    false    5         �            1259    262407    atraccion_id_atraccion_seq    SEQUENCE     �   CREATE SEQUENCE usuario.atraccion_id_atraccion_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE usuario.atraccion_id_atraccion_seq;
       usuario       postgres    false    8    206         3           0    0    atraccion_id_atraccion_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE usuario.atraccion_id_atraccion_seq OWNED BY usuario.atraccion.id_atraccion;
            usuario       postgres    false    205         �            1259    229887    estado_id_Estado_seq    SEQUENCE     �   CREATE SEQUENCE usuario."estado_id_Estado_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE usuario."estado_id_Estado_seq";
       usuario       postgres    false    201    8         4           0    0    estado_id_Estado_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE usuario."estado_id_Estado_seq" OWNED BY usuario.estado.id_estado;
            usuario       postgres    false    200         �            1259    304827     factura_atraccion_id_factura_seq    SEQUENCE     �   CREATE SEQUENCE usuario.factura_atraccion_id_factura_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE usuario.factura_atraccion_id_factura_seq;
       usuario       postgres    false    8    228         5           0    0     factura_atraccion_id_factura_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE usuario.factura_atraccion_id_factura_seq OWNED BY usuario.factura_atraccion.id_factura;
            usuario       postgres    false    227         �            1259    262419    factura_id_factura_seq    SEQUENCE     �   CREATE SEQUENCE usuario.factura_id_factura_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE usuario.factura_id_factura_seq;
       usuario       postgres    false    208    8         6           0    0    factura_id_factura_seq    SEQUENCE OWNED BY     ^   ALTER SEQUENCE usuario.factura_id_factura_seq OWNED BY usuario.factura_habitacion.id_factura;
            usuario       postgres    false    207         �            1259    262428    habitacion_id_habitacion_seq    SEQUENCE     �   CREATE SEQUENCE usuario.habitacion_id_habitacion_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE usuario.habitacion_id_habitacion_seq;
       usuario       postgres    false    8    210         7           0    0    habitacion_id_habitacion_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE usuario.habitacion_id_habitacion_seq OWNED BY usuario.habitacion.id_habitacion;
            usuario       postgres    false    209         �            1259    304819     reserva_atraccion_id_reserva_seq    SEQUENCE     �   CREATE SEQUENCE usuario.reserva_atraccion_id_reserva_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE usuario.reserva_atraccion_id_reserva_seq;
       usuario       postgres    false    8    226         8           0    0     reserva_atraccion_id_reserva_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE usuario.reserva_atraccion_id_reserva_seq OWNED BY usuario.reserva_atraccion.id_reserva;
            usuario       postgres    false    225         �            1259    262635    reserva_id_reserva_seq    SEQUENCE     �   CREATE SEQUENCE usuario.reserva_id_reserva_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE usuario.reserva_id_reserva_seq;
       usuario       postgres    false    212    8         9           0    0    reserva_id_reserva_seq    SEQUENCE OWNED BY     ^   ALTER SEQUENCE usuario.reserva_id_reserva_seq OWNED BY usuario.reserva_habitacion.id_reserva;
            usuario       postgres    false    211         �            1259    262647    sede_id_sede_seq    SEQUENCE     �   CREATE SEQUENCE usuario.sede_id_sede_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE usuario.sede_id_sede_seq;
       usuario       postgres    false    8    214         :           0    0    sede_id_sede_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE usuario.sede_id_sede_seq OWNED BY usuario.sede.id_sede;
            usuario       postgres    false    213         �            1259    262659    sedeatra_id_sede_atra_seq    SEQUENCE     �   CREATE SEQUENCE usuario.sedeatra_id_sede_atra_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE usuario.sedeatra_id_sede_atra_seq;
       usuario       postgres    false    216    8         ;           0    0    sedeatra_id_sede_atra_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE usuario.sedeatra_id_sede_atra_seq OWNED BY usuario.sedeatra.id_sede_atra;
            usuario       postgres    false    215         �            1259    262671    sedehab_id_sede_hab_seq    SEQUENCE     �   CREATE SEQUENCE usuario.sedehab_id_sede_hab_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE usuario.sedehab_id_sede_hab_seq;
       usuario       postgres    false    8    218         <           0    0    sedehab_id_sede_hab_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE usuario.sedehab_id_sede_hab_seq OWNED BY usuario.sedehab.id_sede_hab;
            usuario       postgres    false    217         .           2604    254131    auditoria id    DEFAULT     p   ALTER TABLE ONLY security.auditoria ALTER COLUMN id SET DEFAULT nextval('security.auditoria_id_seq'::regclass);
 =   ALTER TABLE security.auditoria ALTER COLUMN id DROP DEFAULT;
       security       postgres    false    203    202    203         6           2604    303826    autenticacion id    DEFAULT     x   ALTER TABLE ONLY security.autenticacion ALTER COLUMN id SET DEFAULT nextval('security.autenticacion_id_seq'::regclass);
 A   ALTER TABLE security.autenticacion ALTER COLUMN id DROP DEFAULT;
       security       postgres    false    222    221    222         /           2604    262412    atraccion id_atraccion    DEFAULT     �   ALTER TABLE ONLY usuario.atraccion ALTER COLUMN id_atraccion SET DEFAULT nextval('usuario.atraccion_id_atraccion_seq'::regclass);
 F   ALTER TABLE usuario.atraccion ALTER COLUMN id_atraccion DROP DEFAULT;
       usuario       postgres    false    206    205    206         -           2604    229892    estado id_estado    DEFAULT     x   ALTER TABLE ONLY usuario.estado ALTER COLUMN id_estado SET DEFAULT nextval('usuario."estado_id_Estado_seq"'::regclass);
 @   ALTER TABLE usuario.estado ALTER COLUMN id_estado DROP DEFAULT;
       usuario       postgres    false    201    200    201         8           2604    304832    factura_atraccion id_factura    DEFAULT     �   ALTER TABLE ONLY usuario.factura_atraccion ALTER COLUMN id_factura SET DEFAULT nextval('usuario.factura_atraccion_id_factura_seq'::regclass);
 L   ALTER TABLE usuario.factura_atraccion ALTER COLUMN id_factura DROP DEFAULT;
       usuario       postgres    false    228    227    228         0           2604    262424    factura_habitacion id_factura    DEFAULT     �   ALTER TABLE ONLY usuario.factura_habitacion ALTER COLUMN id_factura SET DEFAULT nextval('usuario.factura_id_factura_seq'::regclass);
 M   ALTER TABLE usuario.factura_habitacion ALTER COLUMN id_factura DROP DEFAULT;
       usuario       postgres    false    208    207    208         1           2604    262433    habitacion id_habitacion    DEFAULT     �   ALTER TABLE ONLY usuario.habitacion ALTER COLUMN id_habitacion SET DEFAULT nextval('usuario.habitacion_id_habitacion_seq'::regclass);
 H   ALTER TABLE usuario.habitacion ALTER COLUMN id_habitacion DROP DEFAULT;
       usuario       postgres    false    210    209    210         7           2604    304824    reserva_atraccion id_reserva    DEFAULT     �   ALTER TABLE ONLY usuario.reserva_atraccion ALTER COLUMN id_reserva SET DEFAULT nextval('usuario.reserva_atraccion_id_reserva_seq'::regclass);
 L   ALTER TABLE usuario.reserva_atraccion ALTER COLUMN id_reserva DROP DEFAULT;
       usuario       postgres    false    225    226    226         2           2604    262640    reserva_habitacion id_reserva    DEFAULT     �   ALTER TABLE ONLY usuario.reserva_habitacion ALTER COLUMN id_reserva SET DEFAULT nextval('usuario.reserva_id_reserva_seq'::regclass);
 M   ALTER TABLE usuario.reserva_habitacion ALTER COLUMN id_reserva DROP DEFAULT;
       usuario       postgres    false    212    211    212         3           2604    262652    sede id_sede    DEFAULT     n   ALTER TABLE ONLY usuario.sede ALTER COLUMN id_sede SET DEFAULT nextval('usuario.sede_id_sede_seq'::regclass);
 <   ALTER TABLE usuario.sede ALTER COLUMN id_sede DROP DEFAULT;
       usuario       postgres    false    214    213    214         4           2604    262664    sedeatra id_sede_atra    DEFAULT     �   ALTER TABLE ONLY usuario.sedeatra ALTER COLUMN id_sede_atra SET DEFAULT nextval('usuario.sedeatra_id_sede_atra_seq'::regclass);
 E   ALTER TABLE usuario.sedeatra ALTER COLUMN id_sede_atra DROP DEFAULT;
       usuario       postgres    false    216    215    216         5           2604    262676    sedehab id_sede_hab    DEFAULT     |   ALTER TABLE ONLY usuario.sedehab ALTER COLUMN id_sede_hab SET DEFAULT nextval('usuario.sedehab_id_sede_hab_seq'::regclass);
 C   ALTER TABLE usuario.sedehab ALTER COLUMN id_sede_hab DROP DEFAULT;
       usuario       postgres    false    217    218    218                   0    254122 	   auditoria 
   TABLE DATA               c   COPY security.auditoria (id, fecha, accion, schema, tabla, session, user_bd, data, pk) FROM stdin;
    security       postgres    false    203       3076.dat           0    303823    autenticacion 
   TABLE DATA               c   COPY security.autenticacion (id, doc_identidad, ip, mac, fecha_ini, sesion, fecha_fin) FROM stdin;
    security       postgres    false    222       3092.dat           0    262409 	   atraccion 
   TABLE DATA               �   COPY usuario.atraccion (id_atraccion, nombre, precio_per, descripcion, cant_per, disponibilidad, hora_inicio, hora_fin) FROM stdin;
    usuario       postgres    false    206       3078.dat           0    229889    estado 
   TABLE DATA               9   COPY usuario.estado (id_estado, descripcion) FROM stdin;
    usuario       postgres    false    201       3074.dat           0    304847 
   estado_fac 
   TABLE DATA               6   COPY usuario.estado_fac (id, descripcion) FROM stdin;
    usuario       postgres    false    230       3097.dat           0    304829    factura_atraccion 
   TABLE DATA               z   COPY usuario.factura_atraccion (id_factura, total_pag, id_sedeatr, doc_identidad, fecha_ent, id_sede, estado) FROM stdin;
    usuario       postgres    false    228       3096.dat           0    262421    factura_habitacion 
   TABLE DATA               {   COPY usuario.factura_habitacion (id_factura, total_pag, id_sedehab, doc_identidad, fecha_ent, id_sede, estado) FROM stdin;
    usuario       postgres    false    208       3080.dat 
          0    262430 
   habitacion 
   TABLE DATA               k   COPY usuario.habitacion (id_habitacion, tipo_habitacion, cant_per, precio_hab, disponibilidad) FROM stdin;
    usuario       postgres    false    210       3082.dat           0    304821    reserva_atraccion 
   TABLE DATA                  COPY usuario.reserva_atraccion (id_reserva, fecha_ent, fecha_sal, id_estado, id_sede_atra, doc_identidad, id_sede) FROM stdin;
    usuario       postgres    false    226       3094.dat           0    262637    reserva_habitacion 
   TABLE DATA                  COPY usuario.reserva_habitacion (id_reserva, fecha_ent, fecha_sal, id_estado, id_sede_hab, doc_identidad, id_sede) FROM stdin;
    usuario       postgres    false    212       3084.dat            0    221313    rol 
   TABLE DATA               .   COPY usuario.rol (id_rol, nombre) FROM stdin;
    usuario       postgres    false    199       3072.dat           0    262649    sede 
   TABLE DATA               N   COPY usuario.sede (id_sede, nombre, direccion, telefono, id_admi) FROM stdin;
    usuario       postgres    false    214       3086.dat           0    262661    sedeatra 
   TABLE DATA               �   COPY usuario.sedeatra (id_sede_atra, descripcion, cant_per, hora_ini, hora_fin, disponibilidad, id_atraccion, id_sede, nombre, precio) FROM stdin;
    usuario       postgres    false    216       3088.dat           0    262673    sedehab 
   TABLE DATA               s   COPY usuario.sedehab (id_sede_hab, tipo_hab, cant_per, disponibilidad, id_habitacion, id_sede, precio) FROM stdin;
    usuario       postgres    false    218       3090.dat �          0    221305    usuario 
   TABLE DATA               �   COPY usuario.usuario (doc_identidad, nombre, apellido, edad, num_cel, correo_elec, contrasena, id_rol, session, id_sede) FROM stdin;
    usuario       postgres    false    198       3071.dat =           0    0    auditoria_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('security.auditoria_id_seq', 575, true);
            security       postgres    false    202         >           0    0    autenticacion_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('security.autenticacion_id_seq', 13, true);
            security       postgres    false    221         ?           0    0    atraccion_id_atraccion_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('usuario.atraccion_id_atraccion_seq', 53, true);
            usuario       postgres    false    205         @           0    0    estado_id_Estado_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('usuario."estado_id_Estado_seq"', 1, false);
            usuario       postgres    false    200         A           0    0     factura_atraccion_id_factura_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('usuario.factura_atraccion_id_factura_seq', 4, true);
            usuario       postgres    false    227         B           0    0    factura_id_factura_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('usuario.factura_id_factura_seq', 20, true);
            usuario       postgres    false    207         C           0    0    habitacion_id_habitacion_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('usuario.habitacion_id_habitacion_seq', 2, true);
            usuario       postgres    false    209         D           0    0     reserva_atraccion_id_reserva_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('usuario.reserva_atraccion_id_reserva_seq', 2, true);
            usuario       postgres    false    225         E           0    0    reserva_id_reserva_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('usuario.reserva_id_reserva_seq', 17, true);
            usuario       postgres    false    211         F           0    0    sede_id_sede_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('usuario.sede_id_sede_seq', 5, true);
            usuario       postgres    false    213         G           0    0    sedeatra_id_sede_atra_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('usuario.sedeatra_id_sede_atra_seq', 8, true);
            usuario       postgres    false    215         H           0    0    sedehab_id_sede_hab_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('usuario.sedehab_id_sede_hab_seq', 3, true);
            usuario       postgres    false    217         B           2606    254133    auditoria auditoria_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY security.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY security.auditoria DROP CONSTRAINT auditoria_pkey;
       security         postgres    false    203         \           2606    303831     autenticacion autenticacion_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY security.autenticacion
    ADD CONSTRAINT autenticacion_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY security.autenticacion DROP CONSTRAINT autenticacion_pkey;
       security         postgres    false    222         D           2606    262417    atraccion atraccion_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY usuario.atraccion
    ADD CONSTRAINT atraccion_pkey PRIMARY KEY (id_atraccion);
 C   ALTER TABLE ONLY usuario.atraccion DROP CONSTRAINT atraccion_pkey;
       usuario         postgres    false    206         c           2606    304854    estado_fac estado_fac_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY usuario.estado_fac
    ADD CONSTRAINT estado_fac_pkey PRIMARY KEY (id);
 E   ALTER TABLE ONLY usuario.estado_fac DROP CONSTRAINT estado_fac_pkey;
       usuario         postgres    false    230         @           2606    229897    estado estado_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY usuario.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id_estado);
 =   ALTER TABLE ONLY usuario.estado DROP CONSTRAINT estado_pkey;
       usuario         postgres    false    201         `           2606    304834 (   factura_atraccion factura_atraccion_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY usuario.factura_atraccion
    ADD CONSTRAINT factura_atraccion_pkey PRIMARY KEY (id_factura);
 S   ALTER TABLE ONLY usuario.factura_atraccion DROP CONSTRAINT factura_atraccion_pkey;
       usuario         postgres    false    228         F           2606    262426    factura_habitacion factura_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY usuario.factura_habitacion
    ADD CONSTRAINT factura_pkey PRIMARY KEY (id_factura);
 J   ALTER TABLE ONLY usuario.factura_habitacion DROP CONSTRAINT factura_pkey;
       usuario         postgres    false    208         K           2606    262438    habitacion habitacion_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY usuario.habitacion
    ADD CONSTRAINT habitacion_pkey PRIMARY KEY (id_habitacion);
 E   ALTER TABLE ONLY usuario.habitacion DROP CONSTRAINT habitacion_pkey;
       usuario         postgres    false    210         ^           2606    304826 (   reserva_atraccion reserva_atraccion_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY usuario.reserva_atraccion
    ADD CONSTRAINT reserva_atraccion_pkey PRIMARY KEY (id_reserva);
 S   ALTER TABLE ONLY usuario.reserva_atraccion DROP CONSTRAINT reserva_atraccion_pkey;
       usuario         postgres    false    226         O           2606    262645    reserva_habitacion reserva_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY usuario.reserva_habitacion
    ADD CONSTRAINT reserva_pkey PRIMARY KEY (id_reserva);
 J   ALTER TABLE ONLY usuario.reserva_habitacion DROP CONSTRAINT reserva_pkey;
       usuario         postgres    false    212         >           2606    221320    rol rol_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY usuario.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id_rol);
 7   ALTER TABLE ONLY usuario.rol DROP CONSTRAINT rol_pkey;
       usuario         postgres    false    199         R           2606    262657    sede sede_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY usuario.sede
    ADD CONSTRAINT sede_pkey PRIMARY KEY (id_sede);
 9   ALTER TABLE ONLY usuario.sede DROP CONSTRAINT sede_pkey;
       usuario         postgres    false    214         V           2606    262669    sedeatra sedeatra_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY usuario.sedeatra
    ADD CONSTRAINT sedeatra_pkey PRIMARY KEY (id_sede_atra);
 A   ALTER TABLE ONLY usuario.sedeatra DROP CONSTRAINT sedeatra_pkey;
       usuario         postgres    false    216         Z           2606    262681    sedehab sedehab_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY usuario.sedehab
    ADD CONSTRAINT sedehab_pkey PRIMARY KEY (id_sede_hab);
 ?   ALTER TABLE ONLY usuario.sedehab DROP CONSTRAINT sedehab_pkey;
       usuario         postgres    false    218         <           2606    221312    usuario usuario_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (doc_identidad);
 ?   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT usuario_pkey;
       usuario         postgres    false    198         a           1259    304860    fki_fk_factura_atra_estado_fac    INDEX     _   CREATE INDEX fki_fk_factura_atra_estado_fac ON usuario.factura_atraccion USING btree (estado);
 3   DROP INDEX usuario.fki_fk_factura_atra_estado_fac;
       usuario         postgres    false    228         G           1259    304866    fki_fk_factura_estado_fa    INDEX     Z   CREATE INDEX fki_fk_factura_estado_fa ON usuario.factura_habitacion USING btree (estado);
 -   DROP INDEX usuario.fki_fk_factura_estado_fa;
       usuario         postgres    false    208         H           1259    262700    fki_fk_factura_sedehab    INDEX     \   CREATE INDEX fki_fk_factura_sedehab ON usuario.factura_habitacion USING btree (id_sedehab);
 +   DROP INDEX usuario.fki_fk_factura_sedehab;
       usuario         postgres    false    208         I           1259    262694    fki_fk_factura_usuario    INDEX     _   CREATE INDEX fki_fk_factura_usuario ON usuario.factura_habitacion USING btree (doc_identidad);
 +   DROP INDEX usuario.fki_fk_factura_usuario;
       usuario         postgres    false    208         L           1259    262712    fki_fk_reserva_estado    INDEX     Z   CREATE INDEX fki_fk_reserva_estado ON usuario.reserva_habitacion USING btree (id_estado);
 *   DROP INDEX usuario.fki_fk_reserva_estado;
       usuario         postgres    false    212         M           1259    262718    fki_fk_reserva_sedehab    INDEX     ]   CREATE INDEX fki_fk_reserva_sedehab ON usuario.reserva_habitacion USING btree (id_sede_hab);
 +   DROP INDEX usuario.fki_fk_reserva_sedehab;
       usuario         postgres    false    212         P           1259    262730    fki_fk_sede_usuario    INDEX     H   CREATE INDEX fki_fk_sede_usuario ON usuario.sede USING btree (id_admi);
 (   DROP INDEX usuario.fki_fk_sede_usuario;
       usuario         postgres    false    214         S           1259    262736    fki_fk_sedeatra_atra    INDEX     R   CREATE INDEX fki_fk_sedeatra_atra ON usuario.sedeatra USING btree (id_atraccion);
 )   DROP INDEX usuario.fki_fk_sedeatra_atra;
       usuario         postgres    false    216         T           1259    262742    fki_fk_sedeatra_sede    INDEX     M   CREATE INDEX fki_fk_sedeatra_sede ON usuario.sedeatra USING btree (id_sede);
 )   DROP INDEX usuario.fki_fk_sedeatra_sede;
       usuario         postgres    false    216         W           1259    262748    fki_fk_sedehab_hab    INDEX     P   CREATE INDEX fki_fk_sedehab_hab ON usuario.sedehab USING btree (id_habitacion);
 '   DROP INDEX usuario.fki_fk_sedehab_hab;
       usuario         postgres    false    218         X           1259    262754    fki_fk_sedehab_sed    INDEX     J   CREATE INDEX fki_fk_sedehab_sed ON usuario.sedehab USING btree (id_sede);
 '   DROP INDEX usuario.fki_fk_sedehab_sed;
       usuario         postgres    false    218         9           1259    221326    fki_fk_usuario_rol    INDEX     I   CREATE INDEX fki_fk_usuario_rol ON usuario.usuario USING btree (id_rol);
 '   DROP INDEX usuario.fki_fk_usuario_rol;
       usuario         postgres    false    198         :           1259    262821    fki_fk_usuario_sede    INDEX     K   CREATE INDEX fki_fk_usuario_sede ON usuario.usuario USING btree (id_sede);
 (   DROP INDEX usuario.fki_fk_usuario_sede;
       usuario         postgres    false    198         t           2620    262418    atraccion tg_usuario_atraccion    TRIGGER     �   CREATE TRIGGER tg_usuario_atraccion AFTER INSERT OR DELETE OR UPDATE ON usuario.atraccion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 8   DROP TRIGGER tg_usuario_atraccion ON usuario.atraccion;
       usuario       postgres    false    251    206         s           2620    254205    estado tg_usuario_estado    TRIGGER     �   CREATE TRIGGER tg_usuario_estado AFTER INSERT OR DELETE OR UPDATE ON usuario.estado FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 2   DROP TRIGGER tg_usuario_estado ON usuario.estado;
       usuario       postgres    false    201    251         u           2620    262427 %   factura_habitacion tg_usuario_factura    TRIGGER     �   CREATE TRIGGER tg_usuario_factura AFTER INSERT OR DELETE OR UPDATE ON usuario.factura_habitacion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 ?   DROP TRIGGER tg_usuario_factura ON usuario.factura_habitacion;
       usuario       postgres    false    208    251         w           2620    262439     habitacion tg_usuario_habitacion    TRIGGER     �   CREATE TRIGGER tg_usuario_habitacion AFTER INSERT OR DELETE OR UPDATE ON usuario.habitacion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 :   DROP TRIGGER tg_usuario_habitacion ON usuario.habitacion;
       usuario       postgres    false    251    210         x           2620    262646 %   reserva_habitacion tg_usuario_reserva    TRIGGER     �   CREATE TRIGGER tg_usuario_reserva AFTER INSERT OR DELETE OR UPDATE ON usuario.reserva_habitacion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 ?   DROP TRIGGER tg_usuario_reserva ON usuario.reserva_habitacion;
       usuario       postgres    false    212    251         r           2620    254202    rol tg_usuario_rol    TRIGGER     �   CREATE TRIGGER tg_usuario_rol AFTER INSERT OR DELETE OR UPDATE ON usuario.rol FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 ,   DROP TRIGGER tg_usuario_rol ON usuario.rol;
       usuario       postgres    false    199    251         y           2620    262658    sede tg_usuario_sede    TRIGGER     �   CREATE TRIGGER tg_usuario_sede AFTER INSERT OR DELETE OR UPDATE ON usuario.sede FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 .   DROP TRIGGER tg_usuario_sede ON usuario.sede;
       usuario       postgres    false    214    251         z           2620    262670    sedeatra tg_usuario_sedeatra    TRIGGER     �   CREATE TRIGGER tg_usuario_sedeatra AFTER INSERT OR DELETE OR UPDATE ON usuario.sedeatra FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 6   DROP TRIGGER tg_usuario_sedeatra ON usuario.sedeatra;
       usuario       postgres    false    216    251         {           2620    262682    sedehab tg_usuario_sedehab    TRIGGER     �   CREATE TRIGGER tg_usuario_sedehab AFTER INSERT OR DELETE OR UPDATE ON usuario.sedehab FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 4   DROP TRIGGER tg_usuario_sedehab ON usuario.sedehab;
       usuario       postgres    false    251    218         q           2620    254210    usuario tg_usuario_usuario    TRIGGER     �   CREATE TRIGGER tg_usuario_usuario AFTER INSERT OR DELETE OR UPDATE ON usuario.usuario FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 4   DROP TRIGGER tg_usuario_usuario ON usuario.usuario;
       usuario       postgres    false    251    198         ~           2620    304884 %   estado_fac trigger_usuario_estado_fac    TRIGGER     �   CREATE TRIGGER trigger_usuario_estado_fac AFTER INSERT OR DELETE OR UPDATE ON usuario.estado_fac FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 ?   DROP TRIGGER trigger_usuario_estado_fac ON usuario.estado_fac;
       usuario       postgres    false    251    230         }           2620    304885 .   factura_atraccion trigger_usuario_factura_atra    TRIGGER     �   CREATE TRIGGER trigger_usuario_factura_atra AFTER INSERT OR DELETE OR UPDATE ON usuario.factura_atraccion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 H   DROP TRIGGER trigger_usuario_factura_atra ON usuario.factura_atraccion;
       usuario       postgres    false    251    228         v           2620    304886 .   factura_habitacion trigger_usuario_factura_hab    TRIGGER     �   CREATE TRIGGER trigger_usuario_factura_hab AFTER INSERT OR DELETE OR UPDATE ON usuario.factura_habitacion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 H   DROP TRIGGER trigger_usuario_factura_hab ON usuario.factura_habitacion;
       usuario       postgres    false    251    208         |           2620    304888 -   reserva_atraccion trigger_usuario_reserva_hab    TRIGGER     �   CREATE TRIGGER trigger_usuario_reserva_hab AFTER INSERT OR DELETE OR UPDATE ON usuario.reserva_atraccion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 G   DROP TRIGGER trigger_usuario_reserva_hab ON usuario.reserva_atraccion;
       usuario       postgres    false    226    251         p           2606    304855 ,   factura_atraccion fk_factura_atra_estado_fac    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.factura_atraccion
    ADD CONSTRAINT fk_factura_atra_estado_fac FOREIGN KEY (estado) REFERENCES usuario.estado_fac(id);
 W   ALTER TABLE ONLY usuario.factura_atraccion DROP CONSTRAINT fk_factura_atra_estado_fac;
       usuario       postgres    false    230    2915    228         h           2606    304861 '   factura_habitacion fk_factura_estado_fa    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.factura_habitacion
    ADD CONSTRAINT fk_factura_estado_fa FOREIGN KEY (estado) REFERENCES usuario.estado_fac(id);
 R   ALTER TABLE ONLY usuario.factura_habitacion DROP CONSTRAINT fk_factura_estado_fa;
       usuario       postgres    false    2915    208    230         g           2606    262695 %   factura_habitacion fk_factura_sedehab    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.factura_habitacion
    ADD CONSTRAINT fk_factura_sedehab FOREIGN KEY (id_sedehab) REFERENCES usuario.sedehab(id_sede_hab);
 P   ALTER TABLE ONLY usuario.factura_habitacion DROP CONSTRAINT fk_factura_sedehab;
       usuario       postgres    false    208    218    2906         I           0    0 3   CONSTRAINT fk_factura_sedehab ON factura_habitacion    COMMENT     �   COMMENT ON CONSTRAINT fk_factura_sedehab ON usuario.factura_habitacion IS 'Llave foránea para establecer la relación con las habitaciones de la sede';
            usuario       postgres    false    2919         f           2606    262689 %   factura_habitacion fk_factura_usuario    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.factura_habitacion
    ADD CONSTRAINT fk_factura_usuario FOREIGN KEY (doc_identidad) REFERENCES usuario.usuario(doc_identidad);
 P   ALTER TABLE ONLY usuario.factura_habitacion DROP CONSTRAINT fk_factura_usuario;
       usuario       postgres    false    2876    198    208         J           0    0 3   CONSTRAINT fk_factura_usuario ON factura_habitacion    COMMENT     �   COMMENT ON CONSTRAINT fk_factura_usuario ON usuario.factura_habitacion IS 'Llave foránea para establecer la relación con el usuario';
            usuario       postgres    false    2918         i           2606    262707 $   reserva_habitacion fk_reserva_estado    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.reserva_habitacion
    ADD CONSTRAINT fk_reserva_estado FOREIGN KEY (id_estado) REFERENCES usuario.estado(id_estado);
 O   ALTER TABLE ONLY usuario.reserva_habitacion DROP CONSTRAINT fk_reserva_estado;
       usuario       postgres    false    212    201    2880         K           0    0 2   CONSTRAINT fk_reserva_estado ON reserva_habitacion    COMMENT     �   COMMENT ON CONSTRAINT fk_reserva_estado ON usuario.reserva_habitacion IS 'Llave foránea para establecer la relación entre la reserva y el estado';
            usuario       postgres    false    2921         j           2606    262713 %   reserva_habitacion fk_reserva_sedehab    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.reserva_habitacion
    ADD CONSTRAINT fk_reserva_sedehab FOREIGN KEY (id_sede_hab) REFERENCES usuario.sedehab(id_sede_hab);
 P   ALTER TABLE ONLY usuario.reserva_habitacion DROP CONSTRAINT fk_reserva_sedehab;
       usuario       postgres    false    218    2906    212         L           0    0 3   CONSTRAINT fk_reserva_sedehab ON reserva_habitacion    COMMENT     �   COMMENT ON CONSTRAINT fk_reserva_sedehab ON usuario.reserva_habitacion IS 'Llave foránea para establecer la relación entre la reserva y las habitaciones de la sede';
            usuario       postgres    false    2922         k           2606    262725    sede fk_sede_usuario    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.sede
    ADD CONSTRAINT fk_sede_usuario FOREIGN KEY (id_admi) REFERENCES usuario.usuario(doc_identidad);
 ?   ALTER TABLE ONLY usuario.sede DROP CONSTRAINT fk_sede_usuario;
       usuario       postgres    false    214    198    2876         M           0    0 "   CONSTRAINT fk_sede_usuario ON sede    COMMENT     �   COMMENT ON CONSTRAINT fk_sede_usuario ON usuario.sede IS 'Llave foránea para establecer la relación entre la sede y el administrador';
            usuario       postgres    false    2923         l           2606    262731    sedeatra fk_sedeatra_atra    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.sedeatra
    ADD CONSTRAINT fk_sedeatra_atra FOREIGN KEY (id_atraccion) REFERENCES usuario.atraccion(id_atraccion);
 D   ALTER TABLE ONLY usuario.sedeatra DROP CONSTRAINT fk_sedeatra_atra;
       usuario       postgres    false    216    2884    206         N           0    0 '   CONSTRAINT fk_sedeatra_atra ON sedeatra    COMMENT     �   COMMENT ON CONSTRAINT fk_sedeatra_atra ON usuario.sedeatra IS 'Llave foránea para establecer la relación entre las atracciones de la sede y las atracciones';
            usuario       postgres    false    2924         m           2606    262737    sedeatra fk_sedeatra_sede    FK CONSTRAINT     ~   ALTER TABLE ONLY usuario.sedeatra
    ADD CONSTRAINT fk_sedeatra_sede FOREIGN KEY (id_sede) REFERENCES usuario.sede(id_sede);
 D   ALTER TABLE ONLY usuario.sedeatra DROP CONSTRAINT fk_sedeatra_sede;
       usuario       postgres    false    216    2898    214         O           0    0 '   CONSTRAINT fk_sedeatra_sede ON sedeatra    COMMENT     �   COMMENT ON CONSTRAINT fk_sedeatra_sede ON usuario.sedeatra IS 'Llave foránea para establecer la relación entre las atracciones de la sede y la sede';
            usuario       postgres    false    2925         n           2606    262743    sedehab fk_sedehab_hab    FK CONSTRAINT     �   ALTER TABLE ONLY usuario.sedehab
    ADD CONSTRAINT fk_sedehab_hab FOREIGN KEY (id_habitacion) REFERENCES usuario.habitacion(id_habitacion);
 A   ALTER TABLE ONLY usuario.sedehab DROP CONSTRAINT fk_sedehab_hab;
       usuario       postgres    false    218    2891    210         P           0    0 $   CONSTRAINT fk_sedehab_hab ON sedehab    COMMENT     �   COMMENT ON CONSTRAINT fk_sedehab_hab ON usuario.sedehab IS 'Llave foránea para establecer la relación entre las habitaciones de la sede y las habitaciones';
            usuario       postgres    false    2926         o           2606    262749    sedehab fk_sedehab_sed    FK CONSTRAINT     {   ALTER TABLE ONLY usuario.sedehab
    ADD CONSTRAINT fk_sedehab_sed FOREIGN KEY (id_sede) REFERENCES usuario.sede(id_sede);
 A   ALTER TABLE ONLY usuario.sedehab DROP CONSTRAINT fk_sedehab_sed;
       usuario       postgres    false    2898    218    214         Q           0    0 $   CONSTRAINT fk_sedehab_sed ON sedehab    COMMENT     �   COMMENT ON CONSTRAINT fk_sedehab_sed ON usuario.sedehab IS 'Llave foránea para establecer la relación entre las habitaciones de la sede y las sedes';
            usuario       postgres    false    2927         d           2606    221321    usuario fk_usuario_rol    FK CONSTRAINT     x   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT fk_usuario_rol FOREIGN KEY (id_rol) REFERENCES usuario.rol(id_rol);
 A   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT fk_usuario_rol;
       usuario       postgres    false    2878    199    198         R           0    0 $   CONSTRAINT fk_usuario_rol ON usuario    COMMENT     l   COMMENT ON CONSTRAINT fk_usuario_rol ON usuario.usuario IS 'Llave foránea entre las tablas usuario y rol';
            usuario       postgres    false    2916         e           2606    262816    usuario fk_usuario_sede    FK CONSTRAINT     |   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT fk_usuario_sede FOREIGN KEY (id_sede) REFERENCES usuario.sede(id_sede);
 B   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT fk_usuario_sede;
       usuario       postgres    false    198    2898    214         S           0    0 %   CONSTRAINT fk_usuario_sede ON usuario    COMMENT     �   COMMENT ON CONSTRAINT fk_usuario_sede ON usuario.usuario IS 'Llave foránea para establecer la relacione entre el usuario y la sede';
            usuario       postgres    false    2917                                                                                                                                                                                                                                                                                                                                               3076.dat                                                                                            0000600 0004000 0002000 00000336361 13563757540 0014304 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	2019-10-03 17:35:54.6044	UPDATE	usuario	usuario	\N	postgres	{"contrasena_nuevo": "321", "contrasena_anterior": "123"}	-1
4	2019-10-03 21:21:21.6637	UPDATE	usuario	usuario	1	postgres	{"doc_identidad_nuevo": 12356, "doc_identidad_anterior": 123}	-1
7	2019-10-03 21:24:51.5088	UPDATE	usuario	rol		postgres	{"nombre_nuevo": "admiSede", "nombre_anterior": "asd"}	-1
8	2019-10-03 21:37:24.707	UPDATE	usuario	usuario	1	postgres	{"nombre_nuevo": "asd", "nombre_anterior": "123"}	-1
9	2019-10-03 21:38:14.7835	UPDATE	usuario	usuario	1	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
10	2019-10-03 21:44:37.1696	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
11	2019-10-04 22:17:27.0664	UPDATE	usuario	usuario	2	postgres	{"edad_nuevo": 20, "id_rol_nuevo": 1, "nombre_nuevo": "Jairo", "edad_anterior": 12, "num_cel_nuevo": 3122861314, "session_nuevo": "2", "apellido_nuevo": "Champutis", "id_rol_anterior": 2, "nombre_anterior": "asd", "contrasena_nuevo": "123", "num_cel_anterior": 123, "session_anterior": "1", "apellido_anterior": "123", "correo_elec_nuevo": "jjchamputis@gmail.com", "contrasena_anterior": "1234", "correo_elec_anterior": "123"}	-1
44	2019-10-11 21:41:50.751	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 20, "hora_fin_nuevo": "2019-10-11", "hora_ini_nuevo": "2019-10-11", "precio_per_nuevo": 12000, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 1, "disponibilidad_nuevo": true}	-1
45	2019-10-11 21:53:38.1703	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 20, "hora_fin_anterior": "2019-10-11", "hora_ini_anterior": "2019-10-11", "precio_per_anterior": 12000, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 1, "disponibilidad_anterior": true}	-1
46	2019-10-11 21:54:53.4888	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 12, "hora_fin_nuevo": "2019-10-11", "hora_ini_nuevo": "2019-10-11", "precio_per_nuevo": 12000, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 2, "disponibilidad_nuevo": true}	-1
47	2019-10-11 22:48:35.8674	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 30, "hora_fin_nuevo": "2019-10-11", "hora_ini_nuevo": "2019-10-11", "precio_per_nuevo": 5000, "descripcion_nuevo": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_nuevo": 3, "disponibilidad_nuevo": true}	-1
48	2019-10-11 22:48:50.8212	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 30, "hora_fin_nuevo": "2019-10-11", "hora_ini_nuevo": "2019-10-11", "precio_per_nuevo": 5000, "descripcion_nuevo": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_nuevo": 4, "disponibilidad_nuevo": true}	-1
49	2019-10-11 23:27:58.7608	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 30, "hora_fin_anterior": "2019-10-11", "hora_ini_anterior": "2019-10-11", "precio_per_anterior": 5000, "descripcion_anterior": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_anterior": 4, "disponibilidad_anterior": true}	-1
50	2019-10-12 11:12:55.8502	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 30, "hora_fin_anterior": "2019-10-11", "hora_ini_anterior": "2019-10-11", "precio_per_anterior": 5000, "descripcion_anterior": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_anterior": 3, "disponibilidad_anterior": true}	-1
51	2019-10-12 11:50:47.0404	UPDATE	usuario	atraccion		postgres	{}	-1
52	2019-10-12 11:50:51.8718	UPDATE	usuario	atraccion		postgres	{}	-1
53	2019-10-12 11:50:57.1256	UPDATE	usuario	atraccion		postgres	{}	-1
54	2019-10-12 12:09:42.2777	UPDATE	usuario	atraccion		postgres	{}	-1
55	2019-10-13 18:26:07.5873	UPDATE	usuario	atraccion		postgres	{}	-1
56	2019-10-13 18:36:01.7039	UPDATE	usuario	atraccion		postgres	{}	-1
57	2019-10-13 18:44:59.5105	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 13, "cant_per_anterior": 12}	-1
58	2019-10-13 18:45:12.5829	UPDATE	usuario	atraccion		postgres	{}	-1
59	2019-10-13 18:45:18.2557	UPDATE	usuario	atraccion		postgres	{}	-1
60	2019-10-13 18:45:22.9847	UPDATE	usuario	atraccion		postgres	{}	-1
61	2019-10-13 18:45:25.3608	UPDATE	usuario	atraccion		postgres	{}	-1
62	2019-10-13 18:45:35.6749	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 15, "cant_per_anterior": 13}	-1
63	2019-10-13 18:45:39.1412	UPDATE	usuario	atraccion		postgres	{}	-1
64	2019-10-13 18:45:44.0134	UPDATE	usuario	atraccion		postgres	{}	-1
65	2019-10-13 23:39:08.1499	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 123, "hora_fin_nuevo": "2013-12-12", "hora_ini_nuevo": "2012-12-12", "precio_per_nuevo": 1234, "descripcion_nuevo": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_nuevo": 5, "disponibilidad_nuevo": true}	-1
66	2019-10-13 23:39:21.55	UPDATE	usuario	atraccion		postgres	{}	-1
67	2019-10-13 23:44:06.5493	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 123, "hora_fin_anterior": "2013-12-12", "hora_ini_anterior": "2012-12-12", "precio_per_anterior": 1234, "descripcion_anterior": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_anterior": 5, "disponibilidad_anterior": true}	-1
129	2019-10-14 17:10:30.1117	UPDATE	usuario	usuario	2	postgres	{}	-1
68	2019-10-13 23:44:36.624	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12", "hora_ini_nuevo": "2000-12-12", "precio_per_nuevo": 32, "descripcion_nuevo": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_nuevo": 6, "disponibilidad_nuevo": true}	-1
69	2019-10-13 23:45:39.2681	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12", "hora_ini_nuevo": "2000-12-12", "precio_per_nuevo": 32, "descripcion_nuevo": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_nuevo": 7, "disponibilidad_nuevo": true}	-1
70	2019-10-13 23:46:38.8544	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 12, "hora_fin_anterior": "2012-12-12", "hora_ini_anterior": "2000-12-12", "precio_per_anterior": 32, "descripcion_anterior": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_anterior": 6, "disponibilidad_anterior": true}	-1
71	2019-10-13 23:46:41.3872	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 12, "hora_fin_anterior": "2012-12-12", "hora_ini_anterior": "2000-12-12", "precio_per_anterior": 32, "descripcion_anterior": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_anterior": 7, "disponibilidad_anterior": true}	-1
72	2019-10-13 23:48:25.0429	UPDATE	usuario	atraccion		postgres	{}	-1
73	2019-10-13 23:48:30.3052	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 16, "cant_per_anterior": 15}	-1
74	2019-10-13 23:48:34.4905	UPDATE	usuario	atraccion		postgres	{}	-1
75	2019-10-13 23:50:45.5513	UPDATE	usuario	atraccion		postgres	{}	-1
76	2019-10-13 23:59:36.5368	UPDATE	usuario	atraccion		postgres	{}	-1
77	2019-10-13 23:59:52.1579	UPDATE	usuario	atraccion		postgres	{}	-1
78	2019-10-14 00:02:49.5018	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 17, "cant_per_anterior": 16}	-1
79	2019-10-14 00:02:52.6864	UPDATE	usuario	atraccion		postgres	{}	-1
80	2019-10-14 00:02:58.2895	UPDATE	usuario	atraccion		postgres	{}	-1
81	2019-10-14 00:06:31.6021	UPDATE	usuario	atraccion		postgres	{}	-1
82	2019-10-14 00:06:40.5616	UPDATE	usuario	atraccion		postgres	{}	-1
83	2019-10-14 00:07:17.1643	UPDATE	usuario	atraccion		postgres	{}	-1
84	2019-10-14 00:07:20.3138	UPDATE	usuario	atraccion		postgres	{}	-1
85	2019-10-14 00:15:54.6298	UPDATE	usuario	atraccion		postgres	{}	-1
86	2019-10-14 00:16:02.3822	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 17, "hora_fin_anterior": "2019-10-11", "hora_ini_anterior": "2019-10-11", "precio_per_anterior": 12000, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 2, "disponibilidad_anterior": true}	-1
87	2019-10-14 00:18:56.5544	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 1, "hora_fin_nuevo": "2121-12-12", "hora_ini_nuevo": "2121-12-12", "precio_per_nuevo": 122, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 8, "disponibilidad_nuevo": true}	-1
88	2019-10-14 00:19:02.6528	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 1, "hora_fin_nuevo": "2121-12-12", "hora_ini_nuevo": "2121-12-12", "precio_per_nuevo": 122, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 9, "disponibilidad_nuevo": true}	-1
89	2019-10-14 00:19:09.8642	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 1, "hora_fin_nuevo": "2121-12-12", "hora_ini_nuevo": "2121-12-12", "precio_per_nuevo": 122, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 10, "disponibilidad_nuevo": true}	-1
90	2019-10-14 00:19:33.3304	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 1, "hora_fin_nuevo": "2121-12-12", "hora_ini_nuevo": "2121-12-12", "precio_per_nuevo": 122, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 11, "disponibilidad_nuevo": true}	-1
91	2019-10-14 00:20:07.3335	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 1, "hora_fin_anterior": "2121-12-12", "hora_ini_anterior": "2121-12-12", "precio_per_anterior": 122, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 11, "disponibilidad_anterior": true}	-1
92	2019-10-14 00:20:08.5447	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 1, "hora_fin_anterior": "2121-12-12", "hora_ini_anterior": "2121-12-12", "precio_per_anterior": 122, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 8, "disponibilidad_anterior": true}	-1
93	2019-10-14 00:20:09.2259	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 1, "hora_fin_anterior": "2121-12-12", "hora_ini_anterior": "2121-12-12", "precio_per_anterior": 122, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 9, "disponibilidad_anterior": true}	-1
94	2019-10-14 00:20:12.5516	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 1, "hora_fin_anterior": "2121-12-12", "hora_ini_anterior": "2121-12-12", "precio_per_anterior": 122, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 10, "disponibilidad_anterior": true}	-1
95	2019-10-14 12:14:24.2929	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-12-12", "hora_ini_nuevo": "2012-12-12", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 12, "disponibilidad_nuevo": true}	-1
130	2019-10-14 17:10:34.8636	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{}	-1
131	2019-10-14 17:10:35.3939	UPDATE	usuario	usuario	2	postgres	{}	-1
96	2019-10-14 12:14:27.7962	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-12-12", "hora_ini_nuevo": "2012-12-12", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 13, "disponibilidad_nuevo": true}	-1
97	2019-10-14 12:14:54.5648	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-12-12", "hora_ini_nuevo": "2012-12-12", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 14, "disponibilidad_nuevo": true}	-1
98	2019-10-14 12:14:59.4448	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-12-12", "hora_ini_nuevo": "2012-12-12", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 15, "disponibilidad_nuevo": true}	-1
99	2019-10-14 12:17:36.3632	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 123, "hora_fin_anterior": "2012-12-12", "hora_ini_anterior": "2012-12-12", "precio_per_anterior": 123, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 15, "disponibilidad_anterior": true}	-1
100	2019-10-14 12:17:38.0812	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 123, "hora_fin_anterior": "2012-12-12", "hora_ini_anterior": "2012-12-12", "precio_per_anterior": 123, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 14, "disponibilidad_anterior": true}	-1
101	2019-10-14 12:17:39.2109	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 123, "hora_fin_anterior": "2012-12-12", "hora_ini_anterior": "2012-12-12", "precio_per_anterior": 123, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 13, "disponibilidad_anterior": true}	-1
102	2019-10-14 12:17:43.313	UPDATE	usuario	atraccion		postgres	{}	-1
103	2019-10-14 12:17:49.5356	UPDATE	usuario	atraccion		postgres	{}	-1
104	2019-10-14 12:21:25.1872	UPDATE	usuario	atraccion		postgres	{"precio_per_nuevo": 123333, "precio_per_anterior": 123}	-1
105	2019-10-14 12:21:30.1722	UPDATE	usuario	atraccion		postgres	{}	-1
106	2019-10-14 12:24:29.2144	UPDATE	usuario	atraccion		postgres	{}	-1
107	2019-10-14 12:24:41.9124	UPDATE	usuario	atraccion		postgres	{}	-1
108	2019-10-14 12:25:07.2113	UPDATE	usuario	atraccion		postgres	{}	-1
109	2019-10-14 12:25:14.6849	UPDATE	usuario	atraccion		postgres	{}	-1
110	2019-10-14 12:25:40.6173	UPDATE	usuario	atraccion		postgres	{}	-1
111	2019-10-14 12:27:07.2073	UPDATE	usuario	atraccion		postgres	{"precio_per_nuevo": 12, "precio_per_anterior": 123333}	-1
112	2019-10-14 12:28:33.2916	UPDATE	usuario	atraccion		postgres	{"precio_per_nuevo": 5000, "precio_per_anterior": 12}	-1
113	2019-10-14 12:28:36.5704	UPDATE	usuario	atraccion		postgres	{}	-1
114	2019-10-14 12:28:55.9466	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 123, "hora_fin_nuevo": "2010-12-12", "hora_ini_nuevo": "2019-12-12", "precio_per_nuevo": 123, "descripcion_nuevo": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_nuevo": 16, "disponibilidad_nuevo": true}	-1
115	2019-10-14 12:29:05.8478	UPDATE	usuario	atraccion		postgres	{"precio_per_nuevo": 4000, "precio_per_anterior": 123}	-1
116	2019-10-14 12:32:19.9349	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 123, "hora_fin_anterior": "2010-12-12", "hora_ini_anterior": "2019-12-12", "precio_per_anterior": 4000, "descripcion_anterior": "Consiste en varios automóviles eléctricos pequeños que obtienen la energía de la superficie y el techo del escenario.", "id_atraccion_anterior": 16, "disponibilidad_anterior": true}	-1
117	2019-10-14 12:32:57.0631	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 20, "cant_per_anterior": 123}	-1
118	2019-10-14 13:29:26.7481	INSERT	usuario	rol		postgres	{"id_rol_nuevo": 3, "nombre_nuevo": "Empleado"}	-1
119	2019-10-14 13:29:26.7481	INSERT	usuario	rol		postgres	{"id_rol_nuevo": 4, "nombre_nuevo": "Cliente"}	-1
120	2019-10-14 14:00:18.0453	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 20, "hora_fin_anterior": "2012-12-12", "hora_ini_anterior": "2012-12-12", "precio_per_anterior": 5000, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 12, "disponibilidad_anterior": true}	-1
121	2019-10-14 15:17:37.2155	INSERT	usuario	sede		postgres	{"nombre_nuevo": "Villeta", "id_admi_nuevo": 1234, "id_sede_nuevo": 1, "telefono_nuevo": 3222221213, "direccion_nuevo": "Carrera 2DA #10-10"}	-1
122	2019-10-14 15:18:05.9819	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
123	2019-10-14 15:18:42.8742	INSERT	usuario	sede		postgres	{"nombre_nuevo": "Tobia", "id_admi_nuevo": 123, "id_sede_nuevo": 2, "telefono_nuevo": 3211234354, "direccion_nuevo": "Diagonal 3RA #1-12"}	-1
124	2019-10-14 15:50:19.4068	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{}	-1
125	2019-10-14 15:50:19.4068	UPDATE	usuario	usuario	2	postgres	{}	-1
126	2019-10-14 16:20:28.7071	INSERT	usuario	usuario	ngnam5smwwqletut2kogxrrk	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 2, "num_cel_nuevo": 3211234354, "session_nuevo": "ngnam5smwwqletut2kogxrrk", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 987}	-1
127	2019-10-14 17:01:09.1137	DELETE	usuario	usuario	ngnam5smwwqletut2kogxrrk	postgres	{"edad_anterior": 19, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 3211234354, "session_anterior": "ngnam5smwwqletut2kogxrrk", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 987}	-1
128	2019-10-14 17:09:48.2732	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
132	2019-10-14 17:10:37.7669	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{}	-1
133	2019-10-14 17:10:41.8789	UPDATE	usuario	usuario	2	postgres	{}	-1
134	2019-10-14 17:10:46.228	UPDATE	usuario	usuario	2	postgres	{}	-1
135	2019-10-14 17:11:02.6475	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{}	-1
136	2019-10-14 17:11:14.7551	UPDATE	usuario	usuario	2	postgres	{}	-1
137	2019-10-14 17:11:33.6194	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
138	2019-10-14 17:11:39.0298	UPDATE	usuario	usuario	2	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
139	2019-10-14 17:11:42.4257	UPDATE	usuario	usuario	2	postgres	{}	-1
140	2019-10-14 17:27:00.1393	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
141	2019-10-14 17:27:12.3624	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
142	2019-10-14 17:27:39.3852	UPDATE	usuario	usuario	2	postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
143	2019-10-14 17:27:44.8317	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
144	2019-10-14 17:27:50.6239	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
145	2019-10-14 17:27:53.6855	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{}	-1
146	2019-10-14 17:28:22.1736	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
147	2019-10-14 17:28:22.1736	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
148	2019-10-14 17:33:49.74	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_sede_nuevo": 2, "id_rol_anterior": 2, "id_sede_anterior": 1}	-1
149	2019-10-14 17:33:59.869	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_sede_nuevo": 1, "id_rol_anterior": 2, "id_sede_anterior": 2}	-1
150	2019-10-14 17:34:09.1439	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
151	2019-10-14 17:34:12.2594	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
152	2019-10-14 17:34:17.0815	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
153	2019-10-14 17:34:21.0256	UPDATE	usuario	usuario	2	postgres	{}	-1
154	2019-10-14 17:34:27.7341	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
155	2019-10-14 17:34:32.748	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
156	2019-10-14 17:34:36.6177	UPDATE	usuario	usuario	2	postgres	{}	-1
157	2019-10-14 17:35:21.972	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
158	2019-10-14 17:35:42.3922	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_sede_nuevo": 2, "id_rol_anterior": 2, "id_sede_anterior": 1}	-1
159	2019-10-14 17:35:53.8399	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 3, "id_rol_anterior": 2}	-1
160	2019-10-14 17:36:59.3668	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{}	-1
161	2019-10-14 17:37:07.8104	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
162	2019-10-14 17:37:07.8104	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 3}	-1
163	2019-10-14 17:38:55.4302	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 3, "id_rol_anterior": 2}	-1
164	2019-10-14 17:39:02.9022	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
165	2019-10-14 17:39:30.242	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 3}	-1
166	2019-10-14 17:39:33.6758	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
167	2019-10-14 17:39:41.0537	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
168	2019-10-14 17:39:55.0248	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
169	2019-10-14 17:40:40.0388	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 4, "id_rol_anterior": 2}	-1
170	2019-10-14 17:44:04.1966	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_sede_nuevo": 1, "id_rol_anterior": 2, "id_sede_anterior": 2}	-1
171	2019-10-14 17:51:13.0574	UPDATE	usuario	usuario	2	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
172	2019-10-14 17:51:19.5343	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 4}	-1
173	2019-10-14 17:51:19.5343	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
174	2019-10-14 17:53:23.2717	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
175	2019-10-14 17:53:34.508	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
176	2019-10-14 18:06:29.8102	INSERT	usuario	usuario	azk4tnbs2nbozdxqplr41yrf	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 1, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 123, "session_nuevo": "azk4tnbs2nbozdxqplr41yrf", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 5678}	-1
177	2019-10-14 18:08:24.4445	INSERT	usuario	usuario	azk4tnbs2nbozdxqplr41yrf	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 3211234354, "session_nuevo": "azk4tnbs2nbozdxqplr41yrf", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 987213}	-1
178	2019-10-14 18:08:34.806	UPDATE	usuario	usuario	azk4tnbs2nbozdxqplr41yrf	postgres	{}	-1
179	2019-10-14 18:08:37.4374	DELETE	usuario	usuario	azk4tnbs2nbozdxqplr41yrf	postgres	{"edad_anterior": 19, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 1, "num_cel_anterior": 3211234354, "session_anterior": "azk4tnbs2nbozdxqplr41yrf", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 987213}	-1
180	2019-10-14 18:12:50.2562	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-02", "hora_ini_nuevo": "2012-12-12", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 17, "disponibilidad_nuevo": true}	-1
181	2019-10-14 18:12:57.1756	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 14, "cant_per_anterior": 12}	-1
182	2019-10-14 18:12:59.3286	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 14, "hora_fin_anterior": "2012-12-02", "hora_ini_anterior": "2012-12-12", "precio_per_anterior": 123, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 17, "disponibilidad_anterior": true}	-1
183	2019-10-14 19:41:32.4437	UPDATE	usuario	usuario	azk4tnbs2nbozdxqplr41yrf	postgres	{"id_rol_nuevo": 3, "id_rol_anterior": 1}	-1
184	2019-10-14 19:47:21.1256	INSERT	usuario	usuario	py1eeunownxhcp4rsxo1loud	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 3, "nombre_nuevo": "Sherik", "id_sede_nuevo": 2, "num_cel_nuevo": 3211234354, "session_nuevo": "py1eeunownxhcp4rsxo1loud", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 6543333}	-1
185	2019-10-14 20:43:26.7429	UPDATE	usuario	usuario	py1eeunownxhcp4rsxo1loud	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
186	2019-10-14 20:43:33.9868	UPDATE	usuario	usuario	py1eeunownxhcp4rsxo1loud	postgres	{"edad_nuevo": 23, "edad_anterior": 19, "id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
187	2019-10-14 20:43:36.729	DELETE	usuario	usuario	py1eeunownxhcp4rsxo1loud	postgres	{"edad_anterior": 23, "id_rol_anterior": 3, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 3211234354, "session_anterior": "py1eeunownxhcp4rsxo1loud", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 6543333}	-1
188	2019-10-17 09:59:59.0824	INSERT	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 1, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 123, "session_nuevo": "iaug23xp5n2a03bqgkbjl4t0", "apellido_nuevo": "Luna", "contrasena_nuevo": "12332", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 234234}	-1
189	2019-10-18 19:28:01.6119	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-12-12", "hora_ini_nuevo": "2012-12-12", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 18, "disponibilidad_nuevo": true}	-1
190	2019-10-18 21:08:03.7688	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 1, "num_cel_nuevo": 3121234565, "id_sede_anterior": 2, "num_cel_anterior": 3121231212, "correo_elec_nuevo": "asd@.com", "correo_elec_anterior": "asdfasd.com"}	-1
191	2019-10-18 21:14:34.6352	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_sede_nuevo": 2, "id_rol_anterior": 2, "id_sede_anterior": 1}	-1
192	2019-10-18 21:15:06.9752	UPDATE	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
193	2019-10-18 21:15:06.9752	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
194	2019-10-18 21:15:16.3347	UPDATE	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"id_rol_nuevo": 1, "id_sede_nuevo": 2, "id_rol_anterior": 2, "id_sede_anterior": 1}	-1
195	2019-10-18 21:15:23.752	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
196	2019-10-18 21:15:36.9933	UPDATE	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
197	2019-10-18 21:15:36.9933	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
198	2019-10-18 21:16:11.7182	UPDATE	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
199	2019-10-18 21:16:26.6262	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
200	2019-10-18 21:17:54.5106	UPDATE	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
201	2019-10-18 21:17:54.5106	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
202	2019-10-18 21:22:31.2265	UPDATE	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
203	2019-10-18 21:23:55.8625	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
204	2019-10-18 21:26:10.6518	UPDATE	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
205	2019-10-18 21:26:10.6518	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
206	2019-10-18 21:30:31.4912	DELETE	usuario	usuario	azk4tnbs2nbozdxqplr41yrf	postgres	{"edad_anterior": 19, "id_rol_anterior": 3, "nombre_anterior": "Sherik", "id_sede_anterior": 1, "num_cel_anterior": 123, "session_anterior": "azk4tnbs2nbozdxqplr41yrf", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 5678}	-1
207	2019-10-18 21:35:23.5429	DELETE	usuario	usuario	iaug23xp5n2a03bqgkbjl4t0	postgres	{"edad_anterior": 19, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 123, "session_anterior": "iaug23xp5n2a03bqgkbjl4t0", "apellido_anterior": "Luna", "contrasena_anterior": "12332", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 234234}	-1
208	2019-10-18 21:36:07.1492	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
209	2019-10-18 21:37:30.7904	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
210	2019-10-18 21:37:34.1694	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
211	2019-10-18 21:39:14.6209	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_sede_nuevo": 2, "id_rol_anterior": 2, "id_sede_anterior": 1}	-1
212	2019-10-18 21:47:40.4597	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
213	2019-10-18 21:47:59.202	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
214	2019-10-18 21:47:59.202	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
215	2019-10-18 21:59:21.8751	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
216	2019-10-18 22:06:02.059	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
217	2019-10-18 22:06:17.0953	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
218	2019-10-18 22:06:17.0953	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
219	2019-10-18 22:07:39.1303	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
220	2019-10-18 22:11:35.6009	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
221	2019-10-18 22:11:45.4195	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
222	2019-10-18 22:11:45.4195	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
223	2019-10-18 22:33:00.392	INSERT	usuario	usuario	2dfqtzfyl2s5dfl01drtoyvd	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 2, "num_cel_nuevo": 3211234354, "session_nuevo": "2dfqtzfyl2s5dfl01drtoyvd", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 654333312}	-1
224	2019-10-18 22:33:08.4566	INSERT	usuario	usuario	2dfqtzfyl2s5dfl01drtoyvd	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 3211234354, "session_nuevo": "2dfqtzfyl2s5dfl01drtoyvd", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 654333312345}	-1
225	2019-10-18 22:34:00.0958	UPDATE	usuario	usuario	2dfqtzfyl2s5dfl01drtoyvd	postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
226	2019-10-18 22:34:29.561	UPDATE	usuario	usuario	2	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
227	2019-10-18 22:39:35.3457	DELETE	usuario	usuario	2dfqtzfyl2s5dfl01drtoyvd	postgres	{"edad_anterior": 19, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 3211234354, "session_anterior": "2dfqtzfyl2s5dfl01drtoyvd", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 654333312}	-1
228	2019-10-18 22:43:54.0654	DELETE	usuario	usuario	2dfqtzfyl2s5dfl01drtoyvd	postgres	{"edad_anterior": 19, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 3211234354, "session_anterior": "2dfqtzfyl2s5dfl01drtoyvd", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 654333312345}	-1
229	2019-10-18 22:44:16.8496	INSERT	usuario	usuario	s3wqw44cyontclh3vaf54e4w	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 3211234354, "session_nuevo": "s3wqw44cyontclh3vaf54e4w", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 654333312345}	-1
230	2019-10-18 22:44:29.7215	UPDATE	usuario	usuario	s3wqw44cyontclh3vaf54e4w	postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
231	2019-10-18 22:44:33.5515	DELETE	usuario	usuario	s3wqw44cyontclh3vaf54e4w	postgres	{"edad_anterior": 19, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 3211234354, "session_anterior": "s3wqw44cyontclh3vaf54e4w", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 654333312345}	-1
232	2019-10-18 22:46:30.9384	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12", "hora_ini_nuevo": "2012-12-12", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 19, "disponibilidad_nuevo": true}	-1
233	2019-10-18 22:46:39.8334	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 145, "cant_per_anterior": 12}	-1
234	2019-10-18 22:46:41.4014	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 145, "hora_fin_anterior": "2012-12-12", "hora_ini_anterior": "2012-12-12", "precio_per_anterior": 123, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 19, "disponibilidad_anterior": true}	-1
235	2019-10-18 23:18:07.1156	UPDATE	usuario	atraccion		postgres	{"precio_per_nuevo": 123456, "precio_per_anterior": 123}	-1
236	2019-10-18 23:51:37.1309	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 3, "id_rol_anterior": 2}	-1
237	2019-10-18 23:58:06.1787	INSERT	usuario	usuario	4ku3vegnjik3lcumdlww1kq1	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 4, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 123, "session_nuevo": "4ku3vegnjik3lcumdlww1kq1", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 98765}	-1
238	2019-10-24 21:52:24.6987	INSERT	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_nuevo": 12, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 2, "num_cel_nuevo": 123, "session_nuevo": "m20nmqs2vkogc1ntusfh4c1h", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "asd", "doc_identidad_nuevo": 1234123}	-1
239	2019-10-24 21:52:31.0005	INSERT	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_nuevo": 12, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 2, "num_cel_nuevo": 123, "session_nuevo": "m20nmqs2vkogc1ntusfh4c1h", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "asd", "doc_identidad_nuevo": 12341231}	-1
240	2019-10-24 21:52:34.3578	INSERT	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_nuevo": 12, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 2, "num_cel_nuevo": 123, "session_nuevo": "m20nmqs2vkogc1ntusfh4c1h", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "asd", "doc_identidad_nuevo": 123412312}	-1
241	2019-10-24 21:52:38.4891	INSERT	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_nuevo": 12, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 2, "num_cel_nuevo": 123, "session_nuevo": "m20nmqs2vkogc1ntusfh4c1h", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "asd", "doc_identidad_nuevo": 1234123123}	-1
242	2019-10-24 21:52:43.0995	INSERT	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_nuevo": 12, "id_rol_nuevo": 2, "nombre_nuevo": "Sherik", "id_sede_nuevo": 2, "num_cel_nuevo": 123, "session_nuevo": "m20nmqs2vkogc1ntusfh4c1h", "apellido_nuevo": "Luna", "contrasena_nuevo": "123", "correo_elec_nuevo": "asd", "doc_identidad_nuevo": 12341231234}	-1
243	2019-10-24 21:58:46.4676	DELETE	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_anterior": 12, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 123, "session_anterior": "m20nmqs2vkogc1ntusfh4c1h", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "asd", "doc_identidad_anterior": 1234123123}	-1
244	2019-10-25 13:23:06.5616	DELETE	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_anterior": 12, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 123, "session_anterior": "m20nmqs2vkogc1ntusfh4c1h", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "asd", "doc_identidad_anterior": 12341231234}	-1
245	2019-10-25 13:23:09.3228	DELETE	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_anterior": 12, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 123, "session_anterior": "m20nmqs2vkogc1ntusfh4c1h", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "asd", "doc_identidad_anterior": 123412312}	-1
246	2019-10-25 13:23:19.7643	UPDATE	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
247	2019-10-25 13:23:25.5104	DELETE	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_anterior": 12, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 1, "num_cel_anterior": 123, "session_anterior": "m20nmqs2vkogc1ntusfh4c1h", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "asd", "doc_identidad_anterior": 12341231}	-1
248	2019-10-25 13:23:26.2694	DELETE	usuario	usuario	m20nmqs2vkogc1ntusfh4c1h	postgres	{"edad_anterior": 12, "id_rol_anterior": 2, "nombre_anterior": "Sherik", "id_sede_anterior": 2, "num_cel_anterior": 123, "session_anterior": "m20nmqs2vkogc1ntusfh4c1h", "apellido_anterior": "Luna", "contrasena_anterior": "123", "correo_elec_anterior": "asd", "doc_identidad_anterior": 1234123}	-1
249	2019-10-25 14:20:07.1989	UPDATE	usuario	atraccion		postgres	{}	-1
250	2019-10-25 15:12:33.0357	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asd", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12T00:00:00", "hora_ini_nuevo": "2012-12-12T12:00:00", "precio_per_nuevo": 123, "descripcion_nuevo": "asd", "id_atraccion_nuevo": 20, "disponibilidad_nuevo": true}	-1
251	2019-10-25 15:46:46.5324	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asd", "cant_per_anterior": 12, "hora_fin_anterior": "2012-12-12T00:00:00", "hora_ini_anterior": "2012-12-12T12:00:00", "precio_per_anterior": 123, "descripcion_anterior": "asd", "id_atraccion_anterior": 20, "disponibilidad_anterior": true}	-1
252	2019-10-25 15:47:13.3991	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-12-12T22:13:00", "hora_ini_nuevo": "2012-12-12T10:12:00", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 21, "disponibilidad_nuevo": true}	-1
253	2019-10-25 15:47:28.8482	UPDATE	usuario	atraccion		postgres	{"hora_ini_nuevo": "2012-12-12T11:12:00", "hora_ini_anterior": "2012-12-12T10:12:00"}	-1
254	2019-10-25 15:55:24.9939	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-12-12T13:30:00", "hora_ini_nuevo": "2012-12-12T00:12:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "id_atraccion_nuevo": 22, "disponibilidad_nuevo": true}	-1
255	2019-10-25 15:56:54.0277	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12T00:12:00", "hora_ini_nuevo": "2012-12-12T00:12:00", "precio_per_nuevo": 12, "descripcion_nuevo": "123", "id_atraccion_nuevo": 23, "disponibilidad_nuevo": true}	-1
256	2019-10-25 15:57:12.3724	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Coches chocones", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12T00:12:00", "hora_ini_nuevo": "2012-12-12T00:12:00", "precio_per_nuevo": 12, "descripcion_nuevo": "123", "id_atraccion_nuevo": 24, "disponibilidad_nuevo": true}	-1
257	2019-10-25 16:03:14.843	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 12, "hora_fin_anterior": "2012-12-12T00:12:00", "hora_ini_anterior": "2012-12-12T00:12:00", "precio_per_anterior": 12, "descripcion_anterior": "123", "id_atraccion_anterior": 24, "disponibilidad_anterior": true}	-1
258	2019-10-25 16:03:16.0943	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 123, "hora_fin_anterior": "2012-12-12T22:13:00", "hora_ini_anterior": "2012-12-12T11:12:00", "precio_per_anterior": 123, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 21, "disponibilidad_anterior": true}	-1
259	2019-10-25 16:03:16.9402	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 123, "hora_fin_anterior": "2012-12-12T13:30:00", "hora_ini_anterior": "2012-12-12T00:12:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "id_atraccion_anterior": 22, "disponibilidad_anterior": true}	-1
260	2019-10-25 16:03:17.7455	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Coches chocones", "cant_per_anterior": 12, "hora_fin_anterior": "2012-12-12T00:12:00", "hora_ini_anterior": "2012-12-12T00:12:00", "precio_per_anterior": 12, "descripcion_anterior": "123", "id_atraccion_anterior": 23, "disponibilidad_anterior": true}	-1
261	2019-10-25 16:03:47.9868	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-12-12T00:12:00", "hora_ini_nuevo": "2012-12-12T00:12:00", "precio_per_nuevo": 123, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 25, "disponibilidad_nuevo": true}	-1
262	2019-10-25 16:07:43.6289	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 12, "hora_fin_nuevo": "2111-12-12T00:12:00", "hora_ini_nuevo": "2111-12-12T00:12:00", "precio_per_nuevo": 12, "descripcion_nuevo": "12", "id_atraccion_nuevo": 26, "disponibilidad_nuevo": true}	-1
263	2019-10-25 16:09:07.3508	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 12, "hora_fin_anterior": "2111-12-12T00:12:00", "hora_ini_anterior": "2111-12-12T00:12:00", "precio_per_anterior": 12, "descripcion_anterior": "12", "id_atraccion_anterior": 26, "disponibilidad_anterior": true}	-1
264	2019-10-25 16:09:08.4086	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 123, "hora_fin_anterior": "2012-12-12T00:12:00", "hora_ini_anterior": "2012-12-12T00:12:00", "precio_per_anterior": 123, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 25, "disponibilidad_anterior": true}	-1
265	2019-10-25 16:09:34.6689	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 123, "hora_fin_nuevo": "2012-03-12T00:12:00", "hora_ini_nuevo": "2012-12-12T00:12:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "id_atraccion_nuevo": 27, "disponibilidad_nuevo": true}	-1
266	2019-10-25 16:18:19.22	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12T00:12:00", "hora_ini_nuevo": "2012-12-12T00:12:00", "precio_per_nuevo": 12, "descripcion_nuevo": "123", "id_atraccion_nuevo": 28, "disponibilidad_nuevo": true}	-1
267	2019-10-25 16:19:25.4768	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12T12:12:00", "hora_ini_nuevo": "2012-12-12T00:12:00", "precio_per_nuevo": 121, "descripcion_nuevo": "123", "id_atraccion_nuevo": 29, "disponibilidad_nuevo": true}	-1
268	2019-10-25 16:20:15.2786	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 12, "hora_fin_nuevo": "2012-12-12T12:12:00", "hora_ini_nuevo": "2012-12-12T00:12:00", "precio_per_nuevo": 121, "descripcion_nuevo": "123", "id_atraccion_nuevo": 30, "disponibilidad_nuevo": true}	-1
269	2019-10-25 16:21:52.0616	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 12, "hora_fin_nuevo": "2121-12-12T00:12:00", "hora_ini_nuevo": "2121-02-11T00:12:00", "precio_per_nuevo": 12, "descripcion_nuevo": "123", "id_atraccion_nuevo": 31, "disponibilidad_nuevo": true}	-1
270	2019-10-25 16:40:55.954	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 123, "hora_fin_anterior": "2012-03-12T00:12:00", "hora_ini_anterior": "2012-12-12T00:12:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "id_atraccion_anterior": 27, "disponibilidad_anterior": true}	-1
271	2019-10-25 16:56:44.5554	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 12, "hora_fin_anterior": "2121-12-12T00:12:00", "hora_ini_anterior": "2121-02-11T00:12:00", "precio_per_anterior": 12, "descripcion_anterior": "123", "id_atraccion_anterior": 31, "disponibilidad_anterior": true}	-1
272	2019-10-25 16:56:45.4876	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 12, "hora_fin_anterior": "2012-12-12T00:12:00", "hora_ini_anterior": "2012-12-12T00:12:00", "precio_per_anterior": 12, "descripcion_anterior": "123", "id_atraccion_anterior": 28, "disponibilidad_anterior": true}	-1
273	2019-10-25 16:56:46.2543	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 12, "hora_fin_anterior": "2012-12-12T12:12:00", "hora_ini_anterior": "2012-12-12T00:12:00", "precio_per_anterior": 121, "descripcion_anterior": "123", "id_atraccion_anterior": 29, "disponibilidad_anterior": true}	-1
274	2019-10-25 16:56:47.1345	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 12, "hora_fin_anterior": "2012-12-12T12:12:00", "hora_ini_anterior": "2012-12-12T00:12:00", "precio_per_anterior": 121, "descripcion_anterior": "123", "id_atraccion_anterior": 30, "disponibilidad_anterior": true}	-1
275	2019-10-25 18:37:01.1369	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
276	2019-10-25 18:37:57.9565	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 3}	-1
277	2019-10-25 18:39:35.2855	INSERT	usuario	usuario	\N	postgres	{"edad_nuevo": 12, "id_rol_nuevo": 3, "nombre_nuevo": "Nene", "id_sede_nuevo": 2, "num_cel_nuevo": 123, "session_nuevo": null, "apellido_nuevo": "asd", "contrasena_nuevo": "321", "correo_elec_nuevo": "asdf", "doc_identidad_nuevo": 321}	-1
278	2019-10-25 18:45:08.61	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
279	2019-10-25 18:48:32.0757	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
280	2019-10-25 18:48:45.7602	UPDATE	usuario	usuario	2	postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
281	2019-10-26 10:53:35.1986	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Paintball", "cant_per_nuevo": 12, "hora_fin_nuevo": "2010-12-12T00:00:00", "hora_ini_nuevo": "2010-12-12T12:12:00", "precio_per_nuevo": 12, "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 32, "disponibilidad_nuevo": true}	-1
282	2019-10-26 10:53:59.7784	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "2010-12-12T01:00:00", "hora_fin_anterior": "2010-12-12T00:00:00"}	-1
283	2019-10-26 11:58:45.1942	UPDATE	usuario	atraccion		postgres	{}	-1
284	2019-10-26 11:59:14.199	UPDATE	usuario	atraccion		postgres	{"hora_inicio_nuevo": "13:00:00", "hora_inicio_anterior": "12:00:00"}	-1
285	2019-10-26 11:59:33.3123	UPDATE	usuario	atraccion		postgres	{"hora_inicio_nuevo": "00:00:00", "hora_inicio_anterior": "13:00:00"}	-1
286	2019-10-26 11:59:40.2376	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "00:00:00", "hora_fin_anterior": "12:00:00"}	-1
287	2019-10-26 12:15:09.7992	UPDATE	usuario	atraccion		postgres	{}	-1
288	2019-10-31 18:19:12.2618	INSERT	usuario	usuario	r2c5y5x1rvgyfgxjm5uarkeu	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 4, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 123456789012, "session_nuevo": "r2c5y5x1rvgyfgxjm5uarkeu", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 12345678}	-1
320	2019-11-01 15:48:57.8289	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdfa", "cant_per_nuevo": 0, "hora_fin_nuevo": "13:02:00", "precio_per_nuevo": 4567891, "descripcion_nuevo": "asdfas", "hora_inicio_nuevo": "01:00:00", "id_atraccion_nuevo": 33, "disponibilidad_nuevo": true}	-1
289	2019-10-31 18:20:53.4437	INSERT	usuario	usuario	r2c5y5x1rvgyfgxjm5uarkeu	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 4, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 98765123, "session_nuevo": "r2c5y5x1rvgyfgxjm5uarkeu", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 123456123}	-1
290	2019-10-31 18:24:11.3826	INSERT	usuario	usuario	r2c5y5x1rvgyfgxjm5uarkeu	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 4, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 98765123, "session_nuevo": "r2c5y5x1rvgyfgxjm5uarkeu", "apellido_nuevo": "Luna", "contrasena_nuevo": "123444", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 56456341}	-1
291	2019-10-31 18:27:02.4418	INSERT	usuario	usuario	r2c5y5x1rvgyfgxjm5uarkeu	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 4, "nombre_nuevo": "Sherik", "id_sede_nuevo": 1, "num_cel_nuevo": 98765123, "session_nuevo": "r2c5y5x1rvgyfgxjm5uarkeu", "apellido_nuevo": "Luna", "contrasena_nuevo": "123453", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 12312312}	-1
292	2019-10-31 18:41:08.7864	DELETE	usuario	usuario	r2c5y5x1rvgyfgxjm5uarkeu	postgres	{"edad_anterior": 19, "id_rol_anterior": 4, "nombre_anterior": "Sherik", "id_sede_anterior": 1, "num_cel_anterior": 123456789012, "session_anterior": "r2c5y5x1rvgyfgxjm5uarkeu", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 12345678}	-1
293	2019-10-31 18:41:33.1522	DELETE	usuario	usuario	r2c5y5x1rvgyfgxjm5uarkeu	postgres	{"edad_anterior": 19, "id_rol_anterior": 4, "nombre_anterior": "Sherik", "id_sede_anterior": 1, "num_cel_anterior": 98765123, "session_anterior": "r2c5y5x1rvgyfgxjm5uarkeu", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 123456123}	-1
294	2019-10-31 18:45:18.9443	DELETE	usuario	usuario	r2c5y5x1rvgyfgxjm5uarkeu	postgres	{"edad_anterior": 19, "id_rol_anterior": 4, "nombre_anterior": "Sherik", "id_sede_anterior": 1, "num_cel_anterior": 98765123, "session_anterior": "r2c5y5x1rvgyfgxjm5uarkeu", "apellido_anterior": "Luna", "contrasena_anterior": "123444", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 56456341}	-1
295	2019-10-31 18:45:41.8353	DELETE	usuario	usuario	r2c5y5x1rvgyfgxjm5uarkeu	postgres	{"edad_anterior": 19, "id_rol_anterior": 4, "nombre_anterior": "Sherik", "id_sede_anterior": 1, "num_cel_anterior": 98765123, "session_anterior": "r2c5y5x1rvgyfgxjm5uarkeu", "apellido_anterior": "Luna", "contrasena_anterior": "123453", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 12312312}	-1
296	2019-10-31 18:57:37.4283	DELETE	usuario	usuario	\N	postgres	{"edad_anterior": 12, "id_rol_anterior": 3, "nombre_anterior": "Nene", "id_sede_anterior": 2, "num_cel_anterior": 123, "session_anterior": null, "apellido_anterior": "asd", "contrasena_anterior": "321", "correo_elec_anterior": "asdf", "doc_identidad_anterior": 321}	-1
297	2019-10-31 21:13:03.8606	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "nombre_nuevo": "Camilo", "id_sede_nuevo": 2, "id_rol_anterior": 2, "nombre_anterior": "Andres", "id_sede_anterior": 1}	-1
298	2019-10-31 21:13:23.4109	UPDATE	usuario	usuario	2	postgres	{"nombre_nuevo": "Marcos", "nombre_anterior": "Jairo"}	-1
299	2019-10-31 21:23:19.0056	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2, "correo_elec_nuevo": "gambigambi", "correo_elec_anterior": "jjchamputis@gmail.com"}	-1
300	2019-10-31 21:23:37.1646	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
301	2019-10-31 21:23:37.1646	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
302	2019-10-31 21:23:44.2373	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
303	2019-10-31 21:23:52.9357	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
304	2019-10-31 21:24:02.6282	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
305	2019-10-31 21:24:22.8559	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
306	2019-10-31 21:24:38.0694	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
307	2019-10-31 21:25:32.5889	UPDATE	usuario	usuario	2	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
308	2019-10-31 21:26:26.855	UPDATE	usuario	usuario	2	postgres	{"nombre_nuevo": "Jairo", "nombre_anterior": "Marcos"}	-1
309	2019-10-31 21:28:54.5734	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "nombre_nuevo": "Andres", "id_rol_anterior": 2, "nombre_anterior": "Camilo"}	-1
310	2019-10-31 21:29:03.1459	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
311	2019-10-31 21:29:10.5452	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 1, "id_rol_anterior": 2}	-1
312	2019-10-31 21:29:28.0271	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_rol_nuevo": 2, "id_rol_anterior": 1}	-1
313	2019-10-31 21:31:08.9074	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"nombre_nuevo": "Anis", "apellido_nuevo": "Tinoco", "nombre_anterior": "Andres", "apellido_anterior": "Barreto"}	-1
314	2019-10-31 21:32:28.1814	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"apellido_nuevo": "Barreto", "apellido_anterior": "Tinoco"}	-1
315	2019-10-31 21:51:13.101	UPDATE	usuario	usuario	2	postgres	{"apellido_nuevo": "Jairo", "apellido_anterior": "Champutis", "correo_elec_nuevo": "gambigambi@as.com", "correo_elec_anterior": "gambigambi"}	-1
316	2019-10-31 22:13:11.6949	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"correo_elec_nuevo": "asd@das.com", "correo_elec_anterior": "asd@.com"}	-1
317	2019-10-31 22:16:10.9005	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
318	2019-10-31 22:44:12.769	UPDATE	usuario	usuario	2	postgres	{}	-1
319	2019-11-01 15:45:02.8394	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{}	-1
565	2019-11-16 06:35:02.962	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
321	2019-11-01 16:30:03.5831	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdfa", "cant_per_anterior": 0, "hora_fin_anterior": "13:02:00", "precio_per_anterior": 4567891, "descripcion_anterior": "asdfas", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 33, "disponibilidad_anterior": true}	-1
322	2019-11-01 16:35:12.6079	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "Bebe", "cant_per_nuevo": 1, "hora_fin_nuevo": "17:01:00", "precio_per_nuevo": 1234, "descripcion_nuevo": "123", "hora_inicio_nuevo": "00:12:00", "id_atraccion_nuevo": 34, "disponibilidad_nuevo": true}	-1
323	2019-11-01 17:02:02.9843	UPDATE	usuario	atraccion		postgres	{}	-1
324	2019-11-01 17:02:06.7732	UPDATE	usuario	atraccion		postgres	{}	-1
325	2019-11-01 17:02:13.507	UPDATE	usuario	atraccion		postgres	{}	-1
326	2019-11-01 18:57:51.8117	UPDATE	usuario	usuario	2	postgres	{}	-1
327	2019-11-01 19:00:34.1311	INSERT	usuario	sede		postgres	{"nombre_nuevo": "Neutral", "id_admi_nuevo": 1077976549, "id_sede_nuevo": 3, "telefono_nuevo": 0, "direccion_nuevo": "Neutral"}	-1
328	2019-11-01 20:40:40.4424	UPDATE	usuario	usuario	2	postgres	{}	-1
329	2019-11-01 21:53:34.217	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Bebe", "cant_per_anterior": 1, "hora_fin_anterior": "17:01:00", "precio_per_anterior": 1234, "descripcion_anterior": "123", "hora_inicio_anterior": "00:12:00", "id_atraccion_anterior": 34, "disponibilidad_anterior": true}	-1
330	2019-11-01 21:53:36.2432	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "Paintball", "cant_per_anterior": 12, "hora_fin_anterior": "00:00:00", "precio_per_anterior": 12, "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "hora_inicio_anterior": "00:00:00", "id_atraccion_anterior": 32, "disponibilidad_anterior": true}	-1
331	2019-11-02 10:55:46.7107	UPDATE	usuario	usuario	2	postgres	{}	-1
332	2019-11-02 11:48:05.0193	UPDATE	usuario	usuario	fhcohutbcoq3esvc0h3bqpej	postgres	{"nombre_nuevo": "Andres", "session_nuevo": "fhcohutbcoq3esvc0h3bqpej", "nombre_anterior": "Camilo", "session_anterior": "2"}	-1
333	2019-11-02 12:09:40.4585	UPDATE	usuario	usuario	jb40bdew0vbfn0fpbgrejmbu	postgres	{"nombre_nuevo": "Camilo", "session_nuevo": "jb40bdew0vbfn0fpbgrejmbu", "nombre_anterior": "Andres", "session_anterior": "fhcohutbcoq3esvc0h3bqpej"}	-1
334	2019-11-02 12:10:11.7857	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 12, "cant_per_anterior": 123, "hora_inicio_nuevo": "01:00:00", "hora_inicio_anterior": "00:00:00"}	-1
335	2019-11-02 12:12:03.2525	UPDATE	usuario	usuario	t4i5xmz1tzzd0mqvoqzipzr3	postgres	{}	-1
336	2019-11-02 12:12:12.6275	UPDATE	usuario	usuario	2	postgres	{"apellido_nuevo": "Champutis", "apellido_anterior": "Jairo"}	-1
337	2019-11-02 12:21:48.3242	UPDATE	usuario	usuario	cpybnvjrey3d0ioiqk2rbcdb	postgres	{"session_nuevo": "cpybnvjrey3d0ioiqk2rbcdb", "contrasena_nuevo": "123456", "session_anterior": "jb40bdew0vbfn0fpbgrejmbu", "contrasena_anterior": "123"}	-1
338	2019-11-04 15:18:01.6076	UPDATE	usuario	usuario	cersb4zdvobdbhjrr55pz1nn	postgres	{"session_nuevo": "cersb4zdvobdbhjrr55pz1nn", "contrasena_nuevo": "", "session_anterior": "cpybnvjrey3d0ioiqk2rbcdb", "contrasena_anterior": "123456"}	-1
339	2019-11-04 15:20:09.0185	DELETE	usuario	sede		postgres	{"nombre_anterior": "Neutral", "id_admi_anterior": 1077976549, "id_sede_anterior": 3, "telefono_anterior": 0, "direccion_anterior": "Neutral"}	-1
340	2019-11-04 15:26:20.557	UPDATE	usuario	usuario	cersb4zdvobdbhjrr55pz1nn	postgres	{"contrasena_nuevo": "123456", "contrasena_anterior": ""}	-1
341	2019-11-04 15:29:09.4535	UPDATE	usuario	usuario	y0xdvgkz5kubj1v3rmgm5phe	postgres	{"session_nuevo": "y0xdvgkz5kubj1v3rmgm5phe", "session_anterior": "cersb4zdvobdbhjrr55pz1nn"}	-1
342	2019-11-04 15:30:17.6682	UPDATE	usuario	usuario	yk2tywfuw1mqmn5swu0vbqhg	postgres	{"session_nuevo": "yk2tywfuw1mqmn5swu0vbqhg", "session_anterior": "y0xdvgkz5kubj1v3rmgm5phe"}	-1
343	2019-11-04 15:54:30.9718	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "AS", "cant_per_nuevo": 123, "hora_fin_nuevo": "12:00:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "hora_inicio_nuevo": "00:00:00", "id_atraccion_nuevo": 35, "disponibilidad_nuevo": true}	-1
344	2019-11-04 16:13:15.2432	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "AS", "cant_per_nuevo": 123, "hora_fin_nuevo": "12:00:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "hora_inicio_nuevo": "00:00:00", "id_atraccion_nuevo": 36, "disponibilidad_nuevo": true}	-1
345	2019-11-04 16:14:00.4608	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "AS", "cant_per_nuevo": 123, "hora_fin_nuevo": "10:00:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "hora_inicio_nuevo": "00:00:00", "id_atraccion_nuevo": 37, "disponibilidad_nuevo": true}	-1
346	2019-11-04 16:14:20.1051	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "AS", "cant_per_nuevo": 123, "hora_fin_nuevo": "10:00:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "hora_inicio_nuevo": "00:00:00", "id_atraccion_nuevo": 38, "disponibilidad_nuevo": true}	-1
347	2019-11-04 16:24:13.9964	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "AS", "cant_per_anterior": 123, "hora_fin_anterior": "12:00:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "hora_inicio_anterior": "00:00:00", "id_atraccion_anterior": 35, "disponibilidad_anterior": true}	-1
348	2019-11-04 16:24:17.971	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "AS", "cant_per_anterior": 123, "hora_fin_anterior": "12:00:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "hora_inicio_anterior": "00:00:00", "id_atraccion_anterior": 36, "disponibilidad_anterior": true}	-1
349	2019-11-04 16:24:22.0215	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "AS", "cant_per_anterior": 123, "hora_fin_anterior": "10:00:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "hora_inicio_anterior": "00:00:00", "id_atraccion_anterior": 37, "disponibilidad_anterior": true}	-1
350	2019-11-04 16:24:26.9741	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "AS", "cant_per_anterior": 123, "hora_fin_anterior": "10:00:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "hora_inicio_anterior": "00:00:00", "id_atraccion_anterior": 38, "disponibilidad_anterior": true}	-1
351	2019-11-04 16:25:23.4163	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asd", "cant_per_nuevo": 123, "hora_fin_nuevo": "10:00:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "hora_inicio_nuevo": "00:00:00", "id_atraccion_nuevo": 39, "disponibilidad_nuevo": true}	-1
352	2019-11-04 16:26:29.8579	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asd", "cant_per_nuevo": 123, "hora_fin_nuevo": "23:00:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "hora_inicio_nuevo": "13:00:00", "id_atraccion_nuevo": 40, "disponibilidad_nuevo": true}	-1
353	2019-11-04 17:15:55.4933	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asd", "cant_per_anterior": 123, "hora_fin_anterior": "10:00:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "hora_inicio_anterior": "00:00:00", "id_atraccion_anterior": 39, "disponibilidad_anterior": true}	-1
354	2019-11-04 17:15:59.388	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asd", "cant_per_anterior": 123, "hora_fin_anterior": "23:00:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "hora_inicio_anterior": "13:00:00", "id_atraccion_anterior": 40, "disponibilidad_anterior": true}	-1
355	2019-11-04 17:22:40.2123	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asd", "cant_per_nuevo": 123, "hora_fin_nuevo": "12:00:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "hora_inicio_nuevo": "09:30:00", "id_atraccion_nuevo": 41, "disponibilidad_nuevo": true}	-1
356	2019-11-04 17:24:47.4039	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asd", "cant_per_nuevo": 123, "hora_fin_nuevo": "17:30:00", "precio_per_nuevo": 123, "descripcion_nuevo": "123", "hora_inicio_nuevo": "09:30:00", "id_atraccion_nuevo": 42, "disponibilidad_nuevo": true}	-1
357	2019-11-04 17:31:58.5889	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 12, "hora_fin_nuevo": "08:12:00", "precio_per_nuevo": 1234, "descripcion_nuevo": "asdf", "hora_inicio_nuevo": "00:12:00", "id_atraccion_nuevo": 43, "disponibilidad_nuevo": true}	-1
358	2019-11-04 17:34:21.756	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 12, "hora_fin_anterior": "08:12:00", "precio_per_anterior": 1234, "descripcion_anterior": "asdf", "hora_inicio_anterior": "00:12:00", "id_atraccion_anterior": 43, "disponibilidad_anterior": true}	-1
359	2019-11-04 17:34:22.8791	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asd", "cant_per_anterior": 123, "hora_fin_anterior": "17:30:00", "precio_per_anterior": 123, "descripcion_anterior": "123", "hora_inicio_anterior": "09:30:00", "id_atraccion_anterior": 42, "disponibilidad_anterior": true}	-1
360	2019-11-04 18:53:19.3542	UPDATE	usuario	sede		postgres	{}	-1
361	2019-11-04 18:57:06.8612	UPDATE	usuario	sede		postgres	{}	-1
362	2019-11-04 19:08:50.2849	UPDATE	usuario	sede		postgres	{}	-1
363	2019-11-04 22:06:52.5511	INSERT	usuario	sede		postgres	{"nombre_nuevo": "Facatativa", "id_admi_nuevo": 123, "id_sede_nuevo": 3, "telefono_nuevo": 31231212, "direccion_nuevo": "Carrera 4TA #10-32"}	-1
364	2019-11-04 22:07:01.3621	UPDATE	usuario	sede		postgres	{}	-1
365	2019-11-04 22:08:33.2646	UPDATE	usuario	sede		postgres	{}	-1
366	2019-11-04 22:08:38.5207	UPDATE	usuario	sede		postgres	{}	-1
367	2019-11-04 22:10:50.6896	UPDATE	usuario	sede		postgres	{}	-1
368	2019-11-04 22:11:38.3115	UPDATE	usuario	sede		postgres	{}	-1
369	2019-11-04 22:13:18.7964	UPDATE	usuario	sede		postgres	{"telefono_nuevo": 1234567, "direccion_nuevo": "Carreta 4TA", "telefono_anterior": 31231212, "direccion_anterior": "Carrera 4TA #10-32"}	-1
370	2019-11-04 22:14:23.0274	UPDATE	usuario	sede		postgres	{"id_admi_nuevo": 1234, "id_admi_anterior": 123}	-1
371	2019-11-04 22:14:30.0222	UPDATE	usuario	sede		postgres	{"id_admi_nuevo": 123, "id_admi_anterior": 1234}	-1
372	2019-11-04 22:14:36.2237	UPDATE	usuario	sede		postgres	{"id_admi_nuevo": 1234, "id_admi_anterior": 123}	-1
373	2019-11-04 22:14:48.2503	DELETE	usuario	sede		postgres	{"nombre_anterior": "Facatativa", "id_admi_anterior": 1234, "id_sede_anterior": 3, "telefono_anterior": 1234567, "direccion_anterior": "Carreta 4TA"}	-1
374	2019-11-04 23:35:31.8688	UPDATE	usuario	atraccion		postgres	{"cant_per_nuevo": 12, "precio_per_nuevo": 12345, "cant_per_anterior": 123, "precio_per_anterior": 123, "disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
375	2019-11-04 23:37:56.4712	INSERT	usuario	habitacion		postgres	{"cant_per_nuevo": 21, "precio_hab_nuevo": 25234, "id_habitacion_nuevo": 1, "disponibilidad_nuevo": true, "tipo_habitacion_nuevo": "Switch"}	-1
376	2019-11-04 23:40:46.6427	DELETE	usuario	habitacion		postgres	{"cant_per_anterior": 21, "precio_hab_anterior": 25234, "id_habitacion_anterior": 1, "disponibilidad_anterior": true, "tipo_habitacion_anterior": "Switch"}	-1
377	2019-11-04 23:41:42.3793	INSERT	usuario	habitacion		postgres	{"cant_per_nuevo": 19, "precio_hab_nuevo": 25000, "id_habitacion_nuevo": 2, "disponibilidad_nuevo": true, "tipo_habitacion_nuevo": "Switch"}	-1
378	2019-11-04 23:41:55.7333	UPDATE	usuario	habitacion		postgres	{"precio_hab_nuevo": 40000, "precio_hab_anterior": 25000, "disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
379	2019-11-05 09:28:47.1561	UPDATE	usuario	usuario	by5ivdfdyjp0mhapxua50qei	postgres	{"session_nuevo": "by5ivdfdyjp0mhapxua50qei", "session_anterior": "yk2tywfuw1mqmn5swu0vbqhg"}	-1
380	2019-11-05 09:29:59.8075	INSERT	usuario	sede		postgres	{"nombre_nuevo": "Facatativa", "id_admi_nuevo": 1234, "id_sede_nuevo": 4, "telefono_nuevo": 1234567, "direccion_nuevo": "Carr"}	-1
381	2019-11-05 09:30:11.9767	UPDATE	usuario	sede		postgres	{"id_admi_nuevo": 123, "id_admi_anterior": 1234}	-1
382	2019-11-05 09:30:16.4696	UPDATE	usuario	sede		postgres	{}	-1
383	2019-11-05 09:30:31.2057	DELETE	usuario	sede		postgres	{"nombre_anterior": "Facatativa", "id_admi_anterior": 123, "id_sede_anterior": 4, "telefono_anterior": 1234567, "direccion_anterior": "Carr"}	-1
384	2019-11-05 09:31:05.264	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "17:30:00", "hora_fin_anterior": "12:00:00"}	-1
385	2019-11-09 11:46:23.6304	INSERT	usuario	usuario	1111	postgres	{"edad_nuevo": 12, "id_rol_nuevo": 3, "nombre_nuevo": "asdas", "id_sede_nuevo": 1, "num_cel_nuevo": 123, "session_nuevo": "1111", "apellido_nuevo": "asdfs", "contrasena_nuevo": "123456", "correo_elec_nuevo": "asdf", "doc_identidad_nuevo": 123412}	-1
386	2019-11-11 16:28:14.9746	UPDATE	usuario	usuario	1bzzcvo1ubqaj05uf5jlavvr	postgres	{"session_nuevo": "1bzzcvo1ubqaj05uf5jlavvr", "contrasena_nuevo": "123456", "session_anterior": "2", "contrasena_anterior": "123"}	-1
387	2019-11-11 17:23:09.871	UPDATE	usuario	usuario	1111	postgres	{"nombre_nuevo": "Carlos", "id_sede_nuevo": 2, "num_cel_nuevo": 3123210021, "apellido_nuevo": "Mesa", "nombre_anterior": "asdas", "id_sede_anterior": 1, "num_cel_anterior": 123, "apellido_anterior": "asdfs", "correo_elec_nuevo": "asdf@hotmail.com", "correo_elec_anterior": "asdf"}	-1
388	2019-11-11 17:23:46.2249	INSERT	usuario	usuario	m3sfomagot3mcssncpvu3w5r	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 3, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 1, "num_cel_nuevo": 3211234354, "session_nuevo": "m3sfomagot3mcssncpvu3w5r", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 654333311}	-1
389	2019-11-11 17:23:50.0519	DELETE	usuario	usuario	m3sfomagot3mcssncpvu3w5r	postgres	{"edad_anterior": 19, "id_rol_anterior": 3, "nombre_anterior": "Facatativa", "id_sede_anterior": 1, "num_cel_anterior": 3211234354, "session_anterior": "m3sfomagot3mcssncpvu3w5r", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 654333311}	-1
390	2019-11-11 17:40:17.2258	INSERT	usuario	usuario	hrj2k4b32ezh2lz2pujuip5z	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 3, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 1, "num_cel_nuevo": 3211234354, "session_nuevo": "hrj2k4b32ezh2lz2pujuip5z", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 12312334}	-1
391	2019-11-11 17:41:18.1732	UPDATE	usuario	usuario	hrj2k4b32ezh2lz2pujuip5z	postgres	{}	-1
392	2019-11-11 17:41:20.9912	DELETE	usuario	usuario	hrj2k4b32ezh2lz2pujuip5z	postgres	{"edad_anterior": 19, "id_rol_anterior": 3, "nombre_anterior": "Facatativa", "id_sede_anterior": 1, "num_cel_anterior": 3211234354, "session_anterior": "hrj2k4b32ezh2lz2pujuip5z", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 12312334}	-1
393	2019-11-11 17:41:58.9474	INSERT	usuario	usuario	hrj2k4b32ezh2lz2pujuip5z	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 3, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 1, "num_cel_nuevo": 3211234354, "session_nuevo": "hrj2k4b32ezh2lz2pujuip5z", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 123412366}	-1
394	2019-11-11 17:42:06.6886	DELETE	usuario	usuario	hrj2k4b32ezh2lz2pujuip5z	postgres	{"edad_anterior": 19, "id_rol_anterior": 3, "nombre_anterior": "Facatativa", "id_sede_anterior": 1, "num_cel_anterior": 3211234354, "session_anterior": "hrj2k4b32ezh2lz2pujuip5z", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 123412366}	-1
395	2019-11-11 17:45:27.184	INSERT	usuario	usuario	tgwbxq32hr003azvxwz0js0r	postgres	{"edad_nuevo": 18, "id_rol_nuevo": 3, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 1, "num_cel_nuevo": 123331, "session_nuevo": "tgwbxq32hr003azvxwz0js0r", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 1231233}	-1
396	2019-11-11 17:45:37.8226	DELETE	usuario	usuario	tgwbxq32hr003azvxwz0js0r	postgres	{"edad_anterior": 18, "id_rol_anterior": 3, "nombre_anterior": "Facatativa", "id_sede_anterior": 1, "num_cel_anterior": 123331, "session_anterior": "tgwbxq32hr003azvxwz0js0r", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 1231233}	-1
397	2019-11-11 17:46:27.6624	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asd", "cant_per_anterior": 12, "hora_fin_anterior": "17:30:00", "precio_per_anterior": 12345, "descripcion_anterior": "123", "hora_inicio_anterior": "09:30:00", "id_atraccion_anterior": 41, "disponibilidad_anterior": false}	-1
398	2019-11-11 20:42:16.9413	UPDATE	usuario	usuario	ks5umsz4uerfgqeuvuarcirk	postgres	{"edad_nuevo": 18, "edad_anterior": 12, "session_nuevo": "ks5umsz4uerfgqeuvuarcirk", "session_anterior": "1111"}	-1
399	2019-11-11 21:33:07.3487	INSERT	usuario	estado		postgres	{"id_estado_nuevo": 1, "descripcion_nuevo": "Espera"}	-1
400	2019-11-11 21:34:43.6077	INSERT	usuario	estado		postgres	{"id_estado_nuevo": 2, "descripcion_nuevo": "Activa"}	-1
401	2019-11-11 21:34:43.6077	INSERT	usuario	estado		postgres	{"id_estado_nuevo": 3, "descripcion_nuevo": "Cancelada"}	-1
402	2019-11-11 21:34:43.6077	INSERT	usuario	estado		postgres	{"id_estado_nuevo": 4, "descripcion_nuevo": "Incumplimiento"}	-1
403	2019-11-12 07:56:37.6156	INSERT	usuario	usuario	t3lgqnycdgcdacib1j0djwqe	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 3, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 1, "num_cel_nuevo": 723123123, "session_nuevo": "t3lgqnycdgcdacib1j0djwqe", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 654333312345}	-1
404	2019-11-12 08:00:00.9421	UPDATE	usuario	usuario	hsctvtpxpujw4lug4reia4ya	postgres	{"edad_nuevo": 20, "edad_anterior": 19, "session_nuevo": "hsctvtpxpujw4lug4reia4ya", "session_anterior": "t3lgqnycdgcdacib1j0djwqe"}	-1
405	2019-11-12 08:13:43.4991	INSERT	usuario	usuario	bzfxkspupn3ojjwed0xoik35	postgres	{"edad_nuevo": 18, "id_rol_nuevo": 4, "nombre_nuevo": "prueba", "id_sede_nuevo": 1, "num_cel_nuevo": 3333333, "session_nuevo": "bzfxkspupn3ojjwed0xoik35", "apellido_nuevo": "prueba", "contrasena_nuevo": "12345678", "correo_elec_nuevo": "mongee.1@prueba.com", "doc_identidad_nuevo": 12345678}	-1
406	2019-11-12 08:24:25.1962	INSERT	usuario	usuario	r4q2hpzbwqd5zh53nva1rawc	postgres	{"edad_nuevo": 26, "id_rol_nuevo": 4, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 1, "num_cel_nuevo": 1231234, "session_nuevo": "r4q2hpzbwqd5zh53nva1rawc", "apellido_nuevo": "prueba", "contrasena_nuevo": "123456", "correo_elec_nuevo": "mongee.1@prueba.com", "doc_identidad_nuevo": 123455777}	-1
407	2019-11-12 21:13:14.3737	INSERT	usuario	usuario	lxcghcbic5yre5wyyyprsm22	postgres	{"edad_nuevo": 18, "id_rol_nuevo": 4, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 1, "num_cel_nuevo": 123123, "session_nuevo": "lxcghcbic5yre5wyyyprsm22", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "mongee.1@prueba.com", "doc_identidad_nuevo": 1077976548}	-1
408	2019-11-12 21:13:45.2216	INSERT	usuario	usuario	lxcghcbic5yre5wyyyprsm22	postgres	{"edad_nuevo": 28, "id_rol_nuevo": 4, "nombre_nuevo": "prueba", "id_sede_nuevo": 1, "num_cel_nuevo": 12312321, "session_nuevo": "lxcghcbic5yre5wyyyprsm22", "apellido_nuevo": "asd", "contrasena_nuevo": "12345611", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 44444444}	-1
409	2019-11-12 21:29:00.7636	INSERT	usuario	usuario	cbal1rjp10wncdgdzjwfokb4	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 2, "nombre_nuevo": "prueba", "id_sede_nuevo": 1, "num_cel_nuevo": 3211234354, "session_nuevo": "cbal1rjp10wncdgdzjwfokb4", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "mongee.1@prueba.com", "doc_identidad_nuevo": 1077976544}	-1
410	2019-11-12 21:29:05.0832	DELETE	usuario	usuario	cbal1rjp10wncdgdzjwfokb4	postgres	{"edad_anterior": 19, "id_rol_anterior": 2, "nombre_anterior": "prueba", "id_sede_anterior": 1, "num_cel_anterior": 3211234354, "session_anterior": "cbal1rjp10wncdgdzjwfokb4", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "mongee.1@prueba.com", "doc_identidad_anterior": 1077976544}	-1
411	2019-11-12 21:30:00.5523	INSERT	usuario	usuario	cbal1rjp10wncdgdzjwfokb4	postgres	{"edad_nuevo": 22, "id_rol_nuevo": 2, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 2, "num_cel_nuevo": 1231233, "session_nuevo": "cbal1rjp10wncdgdzjwfokb4", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "mongee.1@prueba.com", "doc_identidad_nuevo": 1077976544}	-1
412	2019-11-12 21:30:13.5754	DELETE	usuario	usuario	cbal1rjp10wncdgdzjwfokb4	postgres	{"edad_anterior": 22, "id_rol_anterior": 2, "nombre_anterior": "Facatativa", "id_sede_anterior": 2, "num_cel_anterior": 1231233, "session_anterior": "cbal1rjp10wncdgdzjwfokb4", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "mongee.1@prueba.com", "doc_identidad_anterior": 1077976544}	-1
413	2019-11-12 21:51:15.8519	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 12, "hora_fin_nuevo": "09:00:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:00:00", "id_atraccion_nuevo": 44, "disponibilidad_nuevo": true}	-1
414	2019-11-12 21:52:03.8487	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 12, "hora_fin_anterior": "09:00:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 44, "disponibilidad_anterior": true}	-1
415	2019-11-12 22:06:52.4155	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "11:00:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:00:00", "id_atraccion_nuevo": 45, "disponibilidad_nuevo": true}	-1
416	2019-11-12 22:13:29.4232	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "11:32:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:32:00", "id_atraccion_nuevo": 46, "disponibilidad_nuevo": true}	-1
417	2019-11-12 22:14:36.03	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "11:32:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:32:00", "id_atraccion_anterior": 46, "disponibilidad_anterior": true}	-1
418	2019-11-12 22:14:37.409	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "11:00:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 45, "disponibilidad_anterior": true}	-1
419	2019-11-12 22:18:33.9962	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:00:00", "hora_fin_anterior": "00:00:00"}	-1
420	2019-11-12 22:18:42.2943	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:01:00", "hora_fin_anterior": "11:00:00"}	-1
421	2019-11-12 22:20:43.5046	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "11:01:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:00:00", "id_atraccion_nuevo": 47, "disponibilidad_nuevo": true}	-1
422	2019-11-12 22:21:16.5704	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "11:01:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 47, "disponibilidad_anterior": true}	-1
423	2019-11-12 22:22:49.8261	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "11:01:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:01:00", "id_atraccion_nuevo": 48, "disponibilidad_nuevo": true}	-1
424	2019-11-12 22:30:50.4675	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "09:01:00", "hora_fin_anterior": "11:01:00"}	-1
425	2019-11-12 22:31:01.1507	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "10:01:00", "hora_fin_anterior": "09:01:00"}	-1
426	2019-11-12 22:33:55.7984	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:01:00", "hora_fin_anterior": "10:01:00"}	-1
427	2019-11-12 22:34:01.2312	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:02:00", "hora_fin_anterior": "11:01:00"}	-1
428	2019-11-12 22:34:15.7196	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:59:00", "hora_fin_anterior": "11:02:00"}	-1
429	2019-11-12 22:38:09.7073	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:00:00", "hora_fin_anterior": "11:59:00", "hora_inicio_nuevo": "01:00:00", "hora_inicio_anterior": "01:01:00"}	-1
430	2019-11-12 22:38:44.2627	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:01:00", "hora_fin_anterior": "11:00:00"}	-1
431	2019-11-12 22:40:19.7132	UPDATE	usuario	atraccion		postgres	{}	-1
432	2019-11-12 22:40:43.4219	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:00:00", "hora_fin_anterior": "11:01:00"}	-1
433	2019-11-12 22:41:02.4066	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:01:00", "hora_fin_anterior": "11:00:00"}	-1
434	2019-11-12 22:41:41.2895	UPDATE	usuario	atraccion		postgres	{}	-1
435	2019-11-12 22:49:34.5019	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "11:00:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:00:00", "id_atraccion_nuevo": 49, "disponibilidad_nuevo": true}	-1
436	2019-11-12 22:49:53.9528	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "11:01:00", "precio_per_nuevo": 3222, "descripcion_nuevo": "Ca", "hora_inicio_nuevo": "01:00:00", "id_atraccion_nuevo": 50, "disponibilidad_nuevo": true}	-1
437	2019-11-12 23:24:16.139	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "11:01:00", "precio_per_anterior": 3222, "descripcion_anterior": "Ca", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 50, "disponibilidad_anterior": true}	-1
438	2019-11-12 23:24:17.6084	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "11:01:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 48, "disponibilidad_anterior": true}	-1
439	2019-11-12 23:24:18.7353	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "11:00:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 49, "disponibilidad_anterior": true}	-1
440	2019-11-12 23:24:42.2575	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "11:00:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:00:00", "id_atraccion_nuevo": 51, "disponibilidad_nuevo": true}	-1
441	2019-11-12 23:25:48.1315	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "11:00:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:01:00", "id_atraccion_nuevo": 52, "disponibilidad_nuevo": true}	-1
442	2019-11-12 23:26:15.9317	INSERT	usuario	atraccion		postgres	{"nombre_nuevo": "asdf", "cant_per_nuevo": 1, "hora_fin_nuevo": "10:01:00", "precio_per_nuevo": 1000, "descripcion_nuevo": "Carr", "hora_inicio_nuevo": "01:00:00", "id_atraccion_nuevo": 53, "disponibilidad_nuevo": true}	-1
443	2019-11-12 23:26:18.477	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "11:00:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 51, "disponibilidad_anterior": true}	-1
444	2019-11-12 23:26:19.3712	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "11:00:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:01:00", "id_atraccion_anterior": 52, "disponibilidad_anterior": true}	-1
445	2019-11-12 23:26:20.8415	DELETE	usuario	atraccion		postgres	{"nombre_anterior": "asdf", "cant_per_anterior": 1, "hora_fin_anterior": "10:01:00", "precio_per_anterior": 1000, "descripcion_anterior": "Carr", "hora_inicio_anterior": "01:00:00", "id_atraccion_anterior": 53, "disponibilidad_anterior": true}	-1
446	2019-11-12 23:29:04.9566	UPDATE	usuario	atraccion		postgres	{"hora_fin_nuevo": "11:00:00", "hora_fin_anterior": "11:01:00"}	-1
447	2019-11-12 23:29:28.8643	UPDATE	usuario	atraccion		postgres	{"hora_inicio_nuevo": "01:08:00", "hora_inicio_anterior": "01:00:00"}	-1
448	2019-11-12 23:36:59.1358	INSERT	usuario	usuario	zewpkic30u04su1qrgrkfizw	postgres	{"edad_nuevo": 21, "id_rol_nuevo": 3, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 2, "num_cel_nuevo": 112231231, "session_nuevo": "zewpkic30u04su1qrgrkfizw", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 1077976500}	-1
449	2019-11-12 23:37:04.8985	DELETE	usuario	usuario	hsctvtpxpujw4lug4reia4ya	postgres	{"edad_anterior": 20, "id_rol_anterior": 3, "nombre_anterior": "Facatativa", "id_sede_anterior": 1, "num_cel_anterior": 723123123, "session_anterior": "hsctvtpxpujw4lug4reia4ya", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 654333312345}	-1
450	2019-11-12 23:37:08.7123	DELETE	usuario	usuario	zewpkic30u04su1qrgrkfizw	postgres	{"edad_anterior": 21, "id_rol_anterior": 3, "nombre_anterior": "Facatativa", "id_sede_anterior": 2, "num_cel_anterior": 112231231, "session_anterior": "zewpkic30u04su1qrgrkfizw", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "sluna@asd.com", "doc_identidad_anterior": 1077976500}	-1
451	2019-11-13 18:55:09.1471	INSERT	usuario	sedeatra		postgres	{"nombre_nuevo": "Paintball", "precio_nuevo": 123, "id_sede_nuevo": 1, "cant_per_nuevo": 12, "hora_fin_nuevo": "11:00:00", "hora_ini_nuevo": "01:08:00", "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 18, "id_sede_atra_nuevo": 1, "disponibilidad_nuevo": true}	-1
452	2019-11-13 18:57:01.02	DELETE	usuario	sedeatra		postgres	{"nombre_anterior": "Paintball", "precio_anterior": 123, "id_sede_anterior": 1, "cant_per_anterior": 12, "hora_fin_anterior": "11:00:00", "hora_ini_anterior": "01:08:00", "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 18, "id_sede_atra_anterior": 1, "disponibilidad_anterior": true}	-1
453	2019-11-13 19:26:16.5589	INSERT	usuario	sedeatra		postgres	{"nombre_nuevo": "Paintball", "precio_nuevo": 1000, "id_sede_nuevo": 2, "cant_per_nuevo": 12, "hora_fin_nuevo": "11:00:00", "hora_ini_nuevo": "01:08:00", "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 18, "id_sede_atra_nuevo": 2, "disponibilidad_nuevo": true}	-1
454	2019-11-13 19:29:15.1256	INSERT	usuario	sedeatra		postgres	{"nombre_nuevo": "Paintball", "precio_nuevo": 4000, "id_sede_nuevo": 2, "cant_per_nuevo": 12, "hora_fin_nuevo": "11:00:00", "hora_ini_nuevo": "01:08:00", "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 18, "id_sede_atra_nuevo": 3, "disponibilidad_nuevo": true}	-1
455	2019-11-13 19:29:15.1787	INSERT	usuario	sedeatra		postgres	{"nombre_nuevo": "Paintball", "precio_nuevo": 4000, "id_sede_nuevo": 2, "cant_per_nuevo": 12, "hora_fin_nuevo": "11:00:00", "hora_ini_nuevo": "01:08:00", "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 18, "id_sede_atra_nuevo": 4, "disponibilidad_nuevo": true}	-1
456	2019-11-13 19:29:15.2013	INSERT	usuario	sedeatra		postgres	{"nombre_nuevo": "Paintball", "precio_nuevo": 4000, "id_sede_nuevo": 2, "cant_per_nuevo": 12, "hora_fin_nuevo": "11:00:00", "hora_ini_nuevo": "01:08:00", "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 18, "id_sede_atra_nuevo": 5, "disponibilidad_nuevo": true}	-1
457	2019-11-13 19:30:36.8118	INSERT	usuario	sedeatra		postgres	{"nombre_nuevo": "Paintball", "precio_nuevo": 4000, "id_sede_nuevo": 2, "cant_per_nuevo": 12, "hora_fin_nuevo": "11:00:00", "hora_ini_nuevo": "01:08:00", "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 18, "id_sede_atra_nuevo": 6, "disponibilidad_nuevo": true}	-1
458	2019-11-13 19:30:36.8336	INSERT	usuario	sedeatra		postgres	{"nombre_nuevo": "Paintball", "precio_nuevo": 4000, "id_sede_nuevo": 2, "cant_per_nuevo": 12, "hora_fin_nuevo": "11:00:00", "hora_ini_nuevo": "01:08:00", "descripcion_nuevo": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_nuevo": 18, "id_sede_atra_nuevo": 7, "disponibilidad_nuevo": true}	-1
459	2019-11-13 19:40:22.4366	UPDATE	usuario	sedeatra		postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
460	2019-11-13 19:40:22.4366	UPDATE	usuario	sedeatra		postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
461	2019-11-13 19:40:22.4366	UPDATE	usuario	sedeatra		postgres	{"id_sede_nuevo": 1, "id_sede_anterior": 2}	-1
462	2019-11-13 20:02:20.4666	UPDATE	usuario	sedeatra		postgres	{"precio_nuevo": 5000, "precio_anterior": 4000}	-1
463	2019-11-13 20:02:24.0545	DELETE	usuario	sedeatra		postgres	{"nombre_anterior": "Paintball", "precio_anterior": 4000, "id_sede_anterior": 1, "cant_per_anterior": 12, "hora_fin_anterior": "11:00:00", "hora_ini_anterior": "01:08:00", "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 18, "id_sede_atra_anterior": 7, "disponibilidad_anterior": true}	-1
464	2019-11-13 20:02:25.5647	DELETE	usuario	sedeatra		postgres	{"nombre_anterior": "Paintball", "precio_anterior": 4000, "id_sede_anterior": 1, "cant_per_anterior": 12, "hora_fin_anterior": "11:00:00", "hora_ini_anterior": "01:08:00", "descripcion_anterior": "Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.", "id_atraccion_anterior": 18, "id_sede_atra_anterior": 6, "disponibilidad_anterior": true}	-1
465	2019-11-13 20:06:34.164	UPDATE	usuario	sedeatra		postgres	{"precio_nuevo": 4000, "precio_anterior": 5000}	-1
466	2019-11-13 20:07:52.0236	UPDATE	usuario	sedeatra		postgres	{"precio_nuevo": 0, "precio_anterior": 4000}	-1
467	2019-11-13 20:07:59.9777	UPDATE	usuario	sedeatra		postgres	{}	-1
468	2019-11-13 20:11:18.3706	UPDATE	usuario	sedeatra		postgres	{"precio_nuevo": 9001, "precio_anterior": 0}	-1
469	2019-11-13 20:11:22.2888	UPDATE	usuario	sedeatra		postgres	{"precio_nuevo": 9000, "precio_anterior": 9001}	-1
470	2019-11-13 22:46:35.3764	INSERT	usuario	sedehab		postgres	{"precio_nuevo": 25000, "id_sede_nuevo": 1, "cant_per_nuevo": 19, "tipo_hab_nuevo": "Switch", "id_sede_hab_nuevo": 1, "id_habitacion_nuevo": 2, "disponibilidad_nuevo": false}	-1
471	2019-11-13 22:46:35.5315	INSERT	usuario	sedehab		postgres	{"precio_nuevo": 25000, "id_sede_nuevo": 1, "cant_per_nuevo": 19, "tipo_hab_nuevo": "Switch", "id_sede_hab_nuevo": 2, "id_habitacion_nuevo": 2, "disponibilidad_nuevo": false}	-1
472	2019-11-13 22:46:35.553	INSERT	usuario	sedehab		postgres	{"precio_nuevo": 25000, "id_sede_nuevo": 1, "cant_per_nuevo": 19, "tipo_hab_nuevo": "Switch", "id_sede_hab_nuevo": 3, "id_habitacion_nuevo": 2, "disponibilidad_nuevo": false}	-1
473	2019-11-13 22:47:06.0016	UPDATE	usuario	habitacion		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
474	2019-11-13 22:50:36.7944	UPDATE	usuario	sedehab		postgres	{"precio_nuevo": 30000, "precio_anterior": 25000}	-1
475	2019-11-13 22:52:22.6551	DELETE	usuario	sedehab		postgres	{"precio_anterior": 25000, "id_sede_anterior": 1, "cant_per_anterior": 19, "tipo_hab_anterior": "Switch", "id_sede_hab_anterior": 2, "id_habitacion_anterior": 2, "disponibilidad_anterior": false}	-1
476	2019-11-13 22:52:34.2982	UPDATE	usuario	sedehab		postgres	{"precio_nuevo": 500000, "precio_anterior": 25000}	-1
477	2019-11-15 15:45:48.9573	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
478	2019-11-15 15:45:48.9573	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
479	2019-11-15 15:47:01.0431	UPDATE	usuario	sedehab		postgres	{"id_sede_nuevo": 2, "id_sede_anterior": 1}	-1
480	2019-11-15 15:50:32.1397	INSERT	usuario	reserva_habitacion		postgres	{"fecha_ent_nuevo": "2019-11-15", "fecha_sal_nuevo": "2019-11-23", "id_estado_nuevo": 1, "id_reserva_nuevo": 1, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
481	2019-11-15 15:58:43.3528	INSERT	usuario	reserva_habitacion		postgres	{"fecha_ent_nuevo": "2019-11-15", "fecha_sal_nuevo": "2019-11-16", "id_estado_nuevo": 1, "id_reserva_nuevo": 2, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
482	2019-11-15 15:58:43.3528	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
483	2019-11-15 15:58:54.1364	INSERT	usuario	reserva_habitacion		postgres	{"fecha_ent_nuevo": "2019-11-15", "fecha_sal_nuevo": "2019-11-16", "id_estado_nuevo": 1, "id_reserva_nuevo": 3, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
484	2019-11-15 15:58:54.1364	UPDATE	usuario	sedehab		postgres	{}	-1
485	2019-11-15 16:01:10.3658	INSERT	usuario	reserva_habitacion		postgres	{"fecha_ent_nuevo": "2019-11-15", "fecha_sal_nuevo": "2019-11-16", "id_estado_nuevo": 1, "id_reserva_nuevo": 4, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
486	2019-11-15 16:01:10.3658	UPDATE	usuario	sedehab		postgres	{}	-1
487	2019-11-15 17:37:34.1705	DELETE	usuario	usuario	lxcghcbic5yre5wyyyprsm22	postgres	{"edad_anterior": 18, "id_rol_anterior": 4, "nombre_anterior": "Facatativa", "id_sede_anterior": 1, "num_cel_anterior": 123123, "session_anterior": "lxcghcbic5yre5wyyyprsm22", "apellido_anterior": "Luna", "contrasena_anterior": "123456", "correo_elec_anterior": "mongee.1@prueba.com", "doc_identidad_anterior": 1077976548}	-1
488	2019-11-15 17:42:00.43	INSERT	usuario	sede		postgres	{"nombre_nuevo": "Facatativa", "id_admi_nuevo": 123, "id_sede_nuevo": 5, "telefono_nuevo": 22222222, "direccion_nuevo": "Carr"}	-1
489	2019-11-15 17:43:09.2078	INSERT	usuario	usuario	jgaig0ooe2kopnzmbia554bc	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 2, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 5, "num_cel_nuevo": 3211234354, "session_nuevo": "jgaig0ooe2kopnzmbia554bc", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "sluna@asd.com", "doc_identidad_nuevo": 654333312345}	-1
490	2019-11-15 17:52:50.3688	UPDATE	usuario	sede		postgres	{}	-1
491	2019-11-15 19:31:46.1207	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 2, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
492	2019-11-15 19:34:39.4808	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 3, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
493	2019-11-15 19:34:39.5209	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
494	2019-11-15 19:34:43.4134	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 4, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
495	2019-11-15 19:34:43.4345	UPDATE	usuario	sedehab		postgres	{}	-1
496	2019-11-15 19:34:44.6139	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 5, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
497	2019-11-15 19:34:44.643	UPDATE	usuario	sedehab		postgres	{}	-1
498	2019-11-15 19:34:46.1416	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 6, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
499	2019-11-15 19:34:46.2028	UPDATE	usuario	sedehab		postgres	{}	-1
500	2019-11-15 19:34:48.7314	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 7, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
501	2019-11-15 19:34:48.8343	UPDATE	usuario	sedehab		postgres	{}	-1
502	2019-11-15 19:35:09.9868	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 8, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 123412}	-1
503	2019-11-15 19:35:10.0043	UPDATE	usuario	sedehab		postgres	{}	-1
504	2019-11-15 19:45:24.6699	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-15", "fecha_sal_nuevo": "2019-11-16", "id_estado_nuevo": 1, "id_reserva_nuevo": 5, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
505	2019-11-15 19:45:24.6699	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
506	2019-11-15 19:45:49.8956	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 9, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
507	2019-11-15 19:45:49.9393	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
508	2019-11-15 19:47:55.7548	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-15", "fecha_sal_nuevo": "2019-11-16", "id_estado_nuevo": 1, "id_reserva_nuevo": 6, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
509	2019-11-15 19:47:55.7548	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
510	2019-11-15 19:48:14.9366	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 10, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
511	2019-11-15 19:48:14.9717	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
512	2019-11-15 19:52:48.8502	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "0001-01-01", "fecha_sal_nuevo": "0001-01-01", "id_estado_nuevo": 1, "id_reserva_nuevo": 7, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
513	2019-11-15 19:52:48.8502	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
514	2019-11-15 19:53:38.8283	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 0, "id_factura_nuevo": 11, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
515	2019-11-15 19:53:38.8475	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
516	2019-11-15 19:58:07.2004	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "fecha_sal_nuevo": "2019-11-30", "id_estado_nuevo": 1, "id_reserva_nuevo": 8, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
517	2019-11-15 19:58:07.2004	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
518	2019-11-15 19:58:41.0404	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
519	2019-11-15 19:59:08.3794	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "fecha_sal_nuevo": "2019-11-30", "id_estado_nuevo": 1, "id_reserva_nuevo": 9, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
520	2019-11-15 19:59:08.3794	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
521	2019-11-15 20:00:34.5704	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 1, "id_factura_nuevo": 12, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
522	2019-11-15 20:00:34.6697	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
523	2019-11-15 20:01:23.8395	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "fecha_sal_nuevo": "2019-11-27", "id_estado_nuevo": 1, "id_reserva_nuevo": 10, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
524	2019-11-15 20:01:23.8395	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
525	2019-11-15 20:03:59.3945	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 1, "id_factura_nuevo": 13, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
526	2019-11-15 20:03:59.4099	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
527	2019-11-15 20:04:27.3356	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-15", "fecha_sal_nuevo": "2019-11-22", "id_estado_nuevo": 1, "id_reserva_nuevo": 11, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
528	2019-11-15 20:04:27.3356	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
529	2019-11-15 20:34:29.4105	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 1, "id_factura_nuevo": 14, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
530	2019-11-15 20:34:29.44	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
531	2019-11-15 21:02:09.4907	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "fecha_sal_nuevo": "2019-11-21", "id_estado_nuevo": 1, "id_reserva_nuevo": 12, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
532	2019-11-15 21:02:09.4907	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
533	2019-11-15 21:02:37.8262	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 0, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 500000, "id_factura_nuevo": 15, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
534	2019-11-15 21:02:37.8438	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
535	2019-11-15 21:02:37.8438	UPDATE	usuario	reserva_habitacion		postgres	{"id_estado_nuevo": 2, "id_estado_anterior": 1}	-1
536	2019-11-15 21:06:15.0282	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "fecha_sal_nuevo": "2019-12-12", "id_estado_nuevo": 1, "id_reserva_nuevo": 13, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
537	2019-11-15 21:06:15.0282	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
538	2019-11-15 21:07:11.9668	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 500000, "id_factura_nuevo": 16, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
539	2019-11-15 21:07:11.9912	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
540	2019-11-15 21:07:11.9912	UPDATE	usuario	reserva_habitacion		postgres	{"id_estado_nuevo": 2, "id_estado_anterior": 1}	-1
541	2019-11-15 21:07:38.0375	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "fecha_sal_nuevo": "2019-12-19", "id_estado_nuevo": 1, "id_reserva_nuevo": 14, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
542	2019-11-15 21:07:38.0375	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
543	2019-11-15 21:07:44.3096	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-15", "total_pag_nuevo": 500000, "id_factura_nuevo": 17, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
544	2019-11-15 21:07:44.3252	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
545	2019-11-15 21:07:44.3252	UPDATE	usuario	reserva_habitacion		postgres	{"id_estado_nuevo": 2, "id_estado_anterior": 1}	-1
546	2019-11-15 22:05:43.9428	UPDATE	usuario	sedeatra		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
547	2019-11-15 22:11:49.667	UPDATE	usuario	sedeatra		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
548	2019-11-15 23:15:16.8662	UPDATE	usuario	estado		postgres	{"descripcion_nuevo": "Efectiva", "descripcion_anterior": "Activa"}	-1
549	2019-11-16 00:32:10.2045	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 1, "fecha_ent_nuevo": "2019-06-06", "fecha_sal_nuevo": "2019-06-06", "id_estado_nuevo": 1, "id_reserva_nuevo": 1, "id_sede_hab_nuevo": 1, "doc_identidad_nuevo": 98765}	-1
550	2019-11-16 05:20:38.4917	INSERT	usuario	estado_fac		postgres	{"id_nuevo": 3, "descripcion_nuevo": "a"}	3
551	2019-11-16 05:21:21.2681	DELETE	usuario	estado_fac		postgres	{"id_anterior": 3, "descripcion_anterior": "a"}	3
552	2019-11-16 05:26:35.019	INSERT	usuario	usuario	inklqberehtqd1bt1oxknqoo	postgres	{"edad_nuevo": 19, "id_rol_nuevo": 3, "nombre_nuevo": "Facatativa", "id_sede_nuevo": 1, "num_cel_nuevo": 3211234354, "session_nuevo": "inklqberehtqd1bt1oxknqoo", "apellido_nuevo": "Luna", "contrasena_nuevo": "123456", "correo_elec_nuevo": "mongee.1@prueba.com", "doc_identidad_nuevo": 654333312}	-1
553	2019-11-16 05:35:41.9193	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-23", "fecha_sal_nuevo": "2019-11-27", "id_estado_nuevo": 1, "id_reserva_nuevo": 15, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
554	2019-11-16 05:35:41.9193	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
555	2019-11-16 05:35:52.6918	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "total_pag_nuevo": 3500000, "id_factura_nuevo": 18, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
556	2019-11-16 05:35:52.6918	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "total_pag_nuevo": 3500000, "id_factura_nuevo": 18, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
557	2019-11-16 05:35:52.7294	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
558	2019-11-16 05:35:52.7294	UPDATE	usuario	reserva_habitacion		postgres	{"id_estado_nuevo": 2, "id_estado_anterior": 1}	-1
559	2019-11-16 06:32:57.0028	INSERT	usuario	reserva_atraccion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "fecha_sal_nuevo": "2019-11-17", "id_estado_nuevo": 1, "id_reserva_nuevo": 2, "id_sede_atra_nuevo": 2, "doc_identidad_nuevo": 1077976549}	-1
560	2019-11-16 06:32:57.0028	UPDATE	usuario	sedeatra		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
561	2019-11-16 06:33:06.4039	INSERT	usuario	factura_atraccion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "total_pag_nuevo": 0, "id_factura_nuevo": 4, "id_sedeatr_nuevo": 2, "doc_identidad_nuevo": 1077976549}	-1
562	2019-11-16 06:33:06.4217	UPDATE	usuario	sedeatra		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
563	2019-11-16 06:33:06.4217	UPDATE	usuario	reserva_atraccion		postgres	{"id_estado_nuevo": 2, "id_estado_anterior": 1}	-1
564	2019-11-16 06:35:02.962	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "fecha_sal_nuevo": "2019-11-17", "id_estado_nuevo": 1, "id_reserva_nuevo": 16, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
566	2019-11-16 06:35:08.6927	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "total_pag_nuevo": 0, "id_factura_nuevo": 19, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
567	2019-11-16 06:35:08.6927	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "total_pag_nuevo": 0, "id_factura_nuevo": 19, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
568	2019-11-16 06:35:08.7257	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
569	2019-11-16 06:35:08.7257	UPDATE	usuario	reserva_habitacion		postgres	{"id_estado_nuevo": 2, "id_estado_anterior": 1}	-1
570	2019-11-16 06:35:22.9273	INSERT	usuario	reserva_habitacion		postgres	{"id_sede_nuevo": 2, "fecha_ent_nuevo": "0001-01-01", "fecha_sal_nuevo": "2019-12-28", "id_estado_nuevo": 1, "id_reserva_nuevo": 17, "id_sede_hab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
571	2019-11-16 06:35:22.9273	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": false, "disponibilidad_anterior": true}	-1
572	2019-11-16 06:35:28.5579	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "total_pag_nuevo": 23000000, "id_factura_nuevo": 20, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
573	2019-11-16 06:35:28.5579	INSERT	usuario	factura_habitacion		postgres	{"estado_nuevo": 1, "id_sede_nuevo": 2, "fecha_ent_nuevo": "2019-11-16", "total_pag_nuevo": 23000000, "id_factura_nuevo": 20, "id_sedehab_nuevo": 3, "doc_identidad_nuevo": 1077976549}	-1
574	2019-11-16 06:35:28.5875	UPDATE	usuario	sedehab		postgres	{"disponibilidad_nuevo": true, "disponibilidad_anterior": false}	-1
575	2019-11-16 06:35:28.5875	UPDATE	usuario	reserva_habitacion		postgres	{"id_estado_nuevo": 2, "id_estado_anterior": 1}	-1
\.


                                                                                                                                                                                                                                                                               3092.dat                                                                                            0000600 0004000 0002000 00000001675 13563757540 0014277 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        9	98765	192.168.1.56	4CEDFB13DBEE	2019-11-16 00:35:31.009519	agoclburq1yznh0nvcx5054a	2019-11-16 00:36:06.235526-05
10	654333312345	10.157.12.236	4CEDFB13DBEE	2019-11-12 07:59:53.451507	hsctvtpxpujw4lug4reia4ya	2019-11-12 08:00:02.700071-05
6	1077976549	192.168.1.56	4CEDFB13DBEE	2019-11-16 00:02:14.901454	2gjecjjudqlbw4epwstjwisb	2019-11-16 00:02:17.245482-05
7	123	192.168.1.56	4CEDFB13DBEE	2019-11-13 19:41:12.598462	k0y3o1vestspc0sxksr51hnm	2019-11-13 19:41:49.950739-05
8	123412	192.168.1.56	4CEDFB13DBEE	2019-11-16 06:34:44.990798	h2dyuqsavb0apib3zajp5wdq	2019-11-16 06:35:34.924887-05
11	12345678	10.157.12.236	4CEDFB13DBEE	2019-11-12 08:14:10.547094	bzfxkspupn3ojjwed0xoik35	2019-11-12 08:14:24.219509-05
12	123455777	10.157.12.236	4CEDFB13DBEE	2019-11-12 08:24:33.746353	r4q2hpzbwqd5zh53nva1rawc	2019-11-12 08:24:36.332235-05
13	1234	192.168.1.56	4CEDFB13DBEE	2019-11-16 05:29:48.585982	x4fexscxpipjtbay4lsmqjbv	2019-11-16 05:30:31.546271-05
\.


                                                                   3078.dat                                                                                            0000600 0004000 0002000 00000000205 13563757540 0014267 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        18	Paintball	123456	Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.	12	t	01:08:00	11:00:00
\.


                                                                                                                                                                                                                                                                                                                                                                                           3074.dat                                                                                            0000600 0004000 0002000 00000000066 13563757540 0014270 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Espera
3	Cancelada
4	Incumplimiento
2	Efectiva
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                          3097.dat                                                                                            0000600 0004000 0002000 00000000052 13563757540 0014270 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Pago no realizado
2	Pago realizado
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      3096.dat                                                                                            0000600 0004000 0002000 00000000216 13563757540 0014271 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1000	2	1077976549	2019-11-16	2	2
2	1000	2	1077976549	2019-11-16	2	2
3	1000	2	1077976549	2019-11-16	2	2
4	0	2	1077976549	2019-11-16	2	1
\.


                                                                                                                                                                                                                                                                                                                                                                                  3080.dat                                                                                            0000600 0004000 0002000 00000000512 13563757540 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        12	1	3	1077976549	2019-11-15	0	1
13	1	3	1077976549	2019-11-15	0	1
14	1	3	1077976549	2019-11-15	0	1
15	500000	3	1077976549	2019-11-15	0	1
16	500000	3	1077976549	2019-11-15	2	1
17	500000	3	1077976549	2019-11-15	2	1
18	3500000	3	1077976549	2019-11-16	2	1
19	0	3	1077976549	2019-11-16	2	1
20	23000000	3	1077976549	2019-11-16	2	1
\.


                                                                                                                                                                                      3082.dat                                                                                            0000600 0004000 0002000 00000000031 13563757540 0014257 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	Switch	19	40000	t
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       3094.dat                                                                                            0000600 0004000 0002000 00000000127 13563757540 0014270 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	2019-11-16	2019-11-23	2	2	1077976549	2
2	2019-11-16	2019-11-17	2	2	1077976549	2
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                         3084.dat                                                                                            0000600 0004000 0002000 00000000247 13563757540 0014272 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	2019-06-06	2019-06-06	1	1	98765	1
15	2019-11-23	2019-11-27	2	3	1077976549	2
16	2019-11-16	2019-11-17	2	3	1077976549	2
17	0001-01-01	2019-12-28	2	3	1077976549	2
\.


                                                                                                                                                                                                                                                                                                                                                         3072.dat                                                                                            0000600 0004000 0002000 00000000061 13563757540 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	superAdmi
2	admiSede
3	Empleado
4	Cliente
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                               3086.dat                                                                                            0000600 0004000 0002000 00000000173 13563757540 0014272 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Villeta	Carrera 2DA #10-10	3222221213	1234
2	Tobia	Diagonal 3RA #1-12	3211234354	123
5	Facatativa	Carr	22222222	123
\.


                                                                                                                                                                                                                                                                                                                                                                                                     3088.dat                                                                                            0000600 0004000 0002000 00000001015 13563757540 0014270 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        3	Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.	12	01:08:00	11:00:00	t	18	2	Paintball	4000
4	Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.	12	01:08:00	11:00:00	t	18	2	Paintball	4000
5	Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.	12	01:08:00	11:00:00	t	18	1	Paintball	9000
2	Juego de estrategia complejo en el que los participantes usan pistolas de Paintball.	12	01:08:00	11:00:00	t	18	2	Paintball	1000
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   3090.dat                                                                                            0000600 0004000 0002000 00000000066 13563757540 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Switch	19	t	2	1	30000
3	Switch	19	t	2	2	500000
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                          3071.dat                                                                                            0000600 0004000 0002000 00000001570 13563757540 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        98765	Sherik	Luna	19	123	sluna@asd.com	123	4	4ku3vegnjik3lcumdlww1kq1	1
123412	Carlos	Mesa	18	3123210021	asdf@hotmail.com	123456	3	ks5umsz4uerfgqeuvuarcirk	2
12345678	prueba	prueba	18	3333333	mongee.1@prueba.com	12345678	4	bzfxkspupn3ojjwed0xoik35	1
123455777	Facatativa	prueba	26	1231234	mongee.1@prueba.com	123456	4	r4q2hpzbwqd5zh53nva1rawc	1
44444444	prueba	asd	28	12312321	sluna@asd.com	12345611	4	lxcghcbic5yre5wyyyprsm22	1
1234	Anis	Barreto	20	3121234565	asd@das.com	123123	2	t4i5xmz1tzzd0mqvoqzipzr3	1
654333312345	Facatativa	Luna	19	3211234354	sluna@asd.com	123456	2	jgaig0ooe2kopnzmbia554bc	5
654333312	Facatativa	Luna	19	3211234354	mongee.1@prueba.com	123456	3	inklqberehtqd1bt1oxknqoo	1
1077976549	Camilo	Tinoco	20	3222695187	kmilo1873@hotmail.com	123456	1	by5ivdfdyjp0mhapxua50qei	1
123	Jairo	Champutis	20	3122861314	gambigambi@as.com	123456	2	1bzzcvo1ubqaj05uf5jlavvr	2
\.


                                                                                                                                        restore.sql                                                                                         0000600 0004000 0002000 00000445302 13563757540 0015413 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 10.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT fk_usuario_sede;
ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT fk_usuario_rol;
ALTER TABLE ONLY usuario.sedehab DROP CONSTRAINT fk_sedehab_sed;
ALTER TABLE ONLY usuario.sedehab DROP CONSTRAINT fk_sedehab_hab;
ALTER TABLE ONLY usuario.sedeatra DROP CONSTRAINT fk_sedeatra_sede;
ALTER TABLE ONLY usuario.sedeatra DROP CONSTRAINT fk_sedeatra_atra;
ALTER TABLE ONLY usuario.sede DROP CONSTRAINT fk_sede_usuario;
ALTER TABLE ONLY usuario.reserva_habitacion DROP CONSTRAINT fk_reserva_sedehab;
ALTER TABLE ONLY usuario.reserva_habitacion DROP CONSTRAINT fk_reserva_estado;
ALTER TABLE ONLY usuario.factura_habitacion DROP CONSTRAINT fk_factura_usuario;
ALTER TABLE ONLY usuario.factura_habitacion DROP CONSTRAINT fk_factura_sedehab;
ALTER TABLE ONLY usuario.factura_habitacion DROP CONSTRAINT fk_factura_estado_fa;
ALTER TABLE ONLY usuario.factura_atraccion DROP CONSTRAINT fk_factura_atra_estado_fac;
DROP TRIGGER trigger_usuario_reserva_hab ON usuario.reserva_atraccion;
DROP TRIGGER trigger_usuario_factura_hab ON usuario.factura_habitacion;
DROP TRIGGER trigger_usuario_factura_atra ON usuario.factura_atraccion;
DROP TRIGGER trigger_usuario_estado_fac ON usuario.estado_fac;
DROP TRIGGER tg_usuario_usuario ON usuario.usuario;
DROP TRIGGER tg_usuario_sedehab ON usuario.sedehab;
DROP TRIGGER tg_usuario_sedeatra ON usuario.sedeatra;
DROP TRIGGER tg_usuario_sede ON usuario.sede;
DROP TRIGGER tg_usuario_rol ON usuario.rol;
DROP TRIGGER tg_usuario_reserva ON usuario.reserva_habitacion;
DROP TRIGGER tg_usuario_habitacion ON usuario.habitacion;
DROP TRIGGER tg_usuario_factura ON usuario.factura_habitacion;
DROP TRIGGER tg_usuario_estado ON usuario.estado;
DROP TRIGGER tg_usuario_atraccion ON usuario.atraccion;
DROP INDEX usuario.fki_fk_usuario_sede;
DROP INDEX usuario.fki_fk_usuario_rol;
DROP INDEX usuario.fki_fk_sedehab_sed;
DROP INDEX usuario.fki_fk_sedehab_hab;
DROP INDEX usuario.fki_fk_sedeatra_sede;
DROP INDEX usuario.fki_fk_sedeatra_atra;
DROP INDEX usuario.fki_fk_sede_usuario;
DROP INDEX usuario.fki_fk_reserva_sedehab;
DROP INDEX usuario.fki_fk_reserva_estado;
DROP INDEX usuario.fki_fk_factura_usuario;
DROP INDEX usuario.fki_fk_factura_sedehab;
DROP INDEX usuario.fki_fk_factura_estado_fa;
DROP INDEX usuario.fki_fk_factura_atra_estado_fac;
ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT usuario_pkey;
ALTER TABLE ONLY usuario.sedehab DROP CONSTRAINT sedehab_pkey;
ALTER TABLE ONLY usuario.sedeatra DROP CONSTRAINT sedeatra_pkey;
ALTER TABLE ONLY usuario.sede DROP CONSTRAINT sede_pkey;
ALTER TABLE ONLY usuario.rol DROP CONSTRAINT rol_pkey;
ALTER TABLE ONLY usuario.reserva_habitacion DROP CONSTRAINT reserva_pkey;
ALTER TABLE ONLY usuario.reserva_atraccion DROP CONSTRAINT reserva_atraccion_pkey;
ALTER TABLE ONLY usuario.habitacion DROP CONSTRAINT habitacion_pkey;
ALTER TABLE ONLY usuario.factura_habitacion DROP CONSTRAINT factura_pkey;
ALTER TABLE ONLY usuario.factura_atraccion DROP CONSTRAINT factura_atraccion_pkey;
ALTER TABLE ONLY usuario.estado DROP CONSTRAINT estado_pkey;
ALTER TABLE ONLY usuario.estado_fac DROP CONSTRAINT estado_fac_pkey;
ALTER TABLE ONLY usuario.atraccion DROP CONSTRAINT atraccion_pkey;
ALTER TABLE ONLY security.autenticacion DROP CONSTRAINT autenticacion_pkey;
ALTER TABLE ONLY security.auditoria DROP CONSTRAINT auditoria_pkey;
ALTER TABLE usuario.sedehab ALTER COLUMN id_sede_hab DROP DEFAULT;
ALTER TABLE usuario.sedeatra ALTER COLUMN id_sede_atra DROP DEFAULT;
ALTER TABLE usuario.sede ALTER COLUMN id_sede DROP DEFAULT;
ALTER TABLE usuario.reserva_habitacion ALTER COLUMN id_reserva DROP DEFAULT;
ALTER TABLE usuario.reserva_atraccion ALTER COLUMN id_reserva DROP DEFAULT;
ALTER TABLE usuario.habitacion ALTER COLUMN id_habitacion DROP DEFAULT;
ALTER TABLE usuario.factura_habitacion ALTER COLUMN id_factura DROP DEFAULT;
ALTER TABLE usuario.factura_atraccion ALTER COLUMN id_factura DROP DEFAULT;
ALTER TABLE usuario.estado ALTER COLUMN id_estado DROP DEFAULT;
ALTER TABLE usuario.atraccion ALTER COLUMN id_atraccion DROP DEFAULT;
ALTER TABLE security.autenticacion ALTER COLUMN id DROP DEFAULT;
ALTER TABLE security.auditoria ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE usuario.sedehab_id_sede_hab_seq;
DROP SEQUENCE usuario.sedeatra_id_sede_atra_seq;
DROP SEQUENCE usuario.sede_id_sede_seq;
DROP SEQUENCE usuario.reserva_id_reserva_seq;
DROP SEQUENCE usuario.reserva_atraccion_id_reserva_seq;
DROP SEQUENCE usuario.habitacion_id_habitacion_seq;
DROP SEQUENCE usuario.factura_id_factura_seq;
DROP SEQUENCE usuario.factura_atraccion_id_factura_seq;
DROP SEQUENCE usuario."estado_id_Estado_seq";
DROP SEQUENCE usuario.atraccion_id_atraccion_seq;
DROP VIEW security.function_db_view;
DROP SEQUENCE security.autenticacion_id_seq;
DROP SEQUENCE security.auditoria_id_seq;
DROP TABLE security.auditoria;
DROP FUNCTION usuario.f_reservar_atra(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_atra integer, _doc_identidad bigint);
DROP FUNCTION usuario.f_reservar(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_hab integer, _doc_identidad bigint);
DROP FUNCTION usuario.f_modificaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _session text);
DROP FUNCTION usuario.f_modificar_usuario_admi(_doc_identidad bigint, _nombre text, _apellido text, _num_cel bigint, _correo_elec text, _id_rol integer, _id_sede integer);
DROP FUNCTION usuario.f_modificar_sede(_id_sede integer, _nombre text, _direccion text, _telefono bigint, _id_admi bigint);
DROP FUNCTION usuario.f_modificar_habitacion_sede(_id_hab integer, _precio integer);
DROP FUNCTION usuario.f_modificar_habitacion(_id_habitacion integer, _tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean);
DROP FUNCTION usuario.f_modificar_factura_atra(_id_factura integer);
DROP FUNCTION usuario.f_modificar_atraccion_sede(_id_atra integer, _precio integer);
DROP FUNCTION usuario.f_modificar_atraccion(_id_atraccion integer, _nombre text, _precio_per integer, _descripcion text, _cant_per integer, _hora_ini time without time zone, _hora_fin time without time zone, _disponibilidad boolean);
DROP FUNCTION usuario.f_login(_doc bigint, _contra text);
DROP FUNCTION usuario.f_leerue();
DROP FUNCTION usuario.f_leerua();
DROP FUNCTION usuario.f_leeru(_doc_identidad bigint);
DROP FUNCTION usuario.f_leertu();
DROP FUNCTION usuario.f_leerr();
DROP FUNCTION usuario.f_leer_usuario_emple();
DROP FUNCTION usuario.f_leer_usuario_admi();
DROP VIEW usuario.v_usuario;
DROP FUNCTION usuario.f_leer_sede(_doc_identidad bigint);
DROP FUNCTION usuario.f_leer_se();
DROP VIEW usuario.v_sede;
DROP FUNCTION usuario.f_leer_reserva_habitacion();
DROP FUNCTION usuario.f_leer_reserva_atraccion();
DROP VIEW usuario.v_reserva_atraccion;
DROP FUNCTION usuario.f_leer_reserva_atra();
DROP FUNCTION usuario.f_leer_reserva();
DROP FUNCTION usuario.f_leer_habitacion_sede_disponibles(_doc_identidad bigint);
DROP FUNCTION usuario.f_leer_habitacion_sede_consulta(_doc_identidad bigint);
DROP FUNCTION usuario.f_leer_habitacion_sede(_doc_identidad bigint);
DROP VIEW usuario.v_habitacion_sede;
DROP FUNCTION usuario.f_leer_habitacion();
DROP FUNCTION usuario.f_leer_factura_atraccion_re(_id_reserva integer);
DROP FUNCTION usuario.f_leer_factura_atraccion();
DROP VIEW usuario.v_factura;
DROP FUNCTION usuario.f_leer_atraccion_sede_consulta(_doc_identidad bigint);
DROP FUNCTION usuario.f_leer_atraccion_sede(_doc_identidad bigint);
DROP FUNCTION usuario.f_leer_atraccion_sede();
DROP VIEW usuario.v_atraccion_sede;
DROP FUNCTION usuario.f_leer_atraccion();
DROP FUNCTION usuario.f_insertaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _id_rol integer, _session text, _id_sede integer);
DROP FUNCTION usuario.f_insertar_sede(_nombre text, _direccion text, _telefono bigint, _id_admi bigint);
DROP FUNCTION usuario.f_insertar_habitacion_sede(_id_habi integer, _id_sede integer, _precio integer);
DROP FUNCTION usuario.f_insertar_habitacion(_tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean);
DROP FUNCTION usuario.f_insertar_factura_atra(_total bigint, _id_sede_atr integer, _doc_identidad bigint, _fecha_sal date, _estado integer);
DROP FUNCTION usuario.f_insertar_factura(_total bigint, _id_sede_hab integer, _doc_identidad bigint, _fecha_sal date, _estado integer);
DROP FUNCTION usuario.f_insertar_atraccion_sede(_id_atraccion integer, _id_sede integer, _precio integer);
DROP FUNCTION usuario.f_insertar_atraccion(_nombre text, _precio_per integer, _descripcion text, _cant_per integer, _disponibilidad boolean, _hora_ini time without time zone, _hora_fin time without time zone);
DROP FUNCTION usuario.f_entregar_reservar(_id_sede_hab integer, _id_reserva integer);
DROP FUNCTION usuario.f_entregar_reserva_atr(_id_sede_atr integer, _id_reserva integer);
DROP FUNCTION usuario.f_eliminar_usuario(_doc_identidad bigint);
DROP FUNCTION usuario.f_eliminar_sede(_id_sede integer);
DROP FUNCTION usuario.f_eliminar_habitacion_sede(_id_hab integer);
DROP FUNCTION usuario.f_eliminar_habitacion(_id_habitacion integer);
DROP FUNCTION usuario.f_eliminar_atraccion_sede(_id_atraccion integer);
DROP FUNCTION usuario.f_eliminar_atraccion(_id_atraccion integer);
DROP FUNCTION usuario.f_consulta_reserva(_id_reserva integer);
DROP FUNCTION usuario.f_consulta_emple_clien(_doc_identidad bigint);
DROP FUNCTION usuario.f_consulta_clien(_doc_identidad bigint);
DROP FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.usuario;
DROP FUNCTION security.field_audit(_data_new usuario.sedehab, _data_old usuario.sedehab, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.sedehab;
DROP FUNCTION security.field_audit(_data_new usuario.sedeatra, _data_old usuario.sedeatra, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.sedeatra;
DROP FUNCTION security.field_audit(_data_new usuario.sede, _data_old usuario.sede, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.sede;
DROP FUNCTION security.field_audit(_data_new usuario.rol, _data_old usuario.rol, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.rol;
DROP FUNCTION security.field_audit(_data_new usuario.reserva_habitacion, _data_old usuario.reserva_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.reserva_habitacion;
DROP FUNCTION security.field_audit(_data_new usuario.reserva_atraccion, _data_old usuario.reserva_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.reserva_atraccion;
DROP FUNCTION security.field_audit(_data_new usuario.habitacion, _data_old usuario.habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.habitacion;
DROP FUNCTION security.field_audit(_data_new usuario.factura_habitacion, _data_old usuario.factura_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.factura_habitacion;
DROP FUNCTION security.field_audit(_data_new usuario.factura_atraccion, _data_old usuario.factura_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.factura_atraccion;
DROP FUNCTION security.field_audit(_data_new usuario.estado_fac, _data_old usuario.estado_fac, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.estado_fac;
DROP FUNCTION security.field_audit(_data_new usuario.estado, _data_old usuario.estado, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.estado;
DROP FUNCTION security.field_audit(_data_new usuario.atraccion, _data_old usuario.atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
DROP TABLE usuario.atraccion;
DROP FUNCTION security.f_validar_fecha_fin(_doc_identidad bigint);
DROP FUNCTION security.f_obtener_sesion(_doc_identidad bigint);
DROP TABLE security.autenticacion;
DROP FUNCTION security.f_log_auditoria();
DROP FUNCTION security.f_guardar_sesion(_doc_identidad bigint, _ip text, _mac text, _session text);
DROP FUNCTION security.f_continuar(_doc bigint);
DROP FUNCTION security.f_cerrar_sesion(_doc_identidad bigint);
DROP EXTENSION plpgsql;
DROP SCHEMA usuario;
DROP SCHEMA security;
DROP SCHEMA public;
--
-- Name: DATABASE gambi; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE gambi IS 'Base de datos para el software Gambi';


--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: security; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA security;


ALTER SCHEMA security OWNER TO postgres;

--
-- Name: SCHEMA security; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA security IS 'Esquema de la seguridad del proyecto';


--
-- Name: usuario; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA usuario;


ALTER SCHEMA usuario OWNER TO postgres;

--
-- Name: SCHEMA usuario; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA usuario IS 'Esquema de los datos del usuario';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: f_cerrar_sesion(bigint); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.f_cerrar_sesion(_doc_identidad bigint) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE security.autenticacion
	SET 
		fecha_fin = current_timestamp
	WHERE 
		doc_identidad = _doc_identidad;
END
$$;


ALTER FUNCTION security.f_cerrar_sesion(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_continuar(bigint); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.f_continuar(_doc bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE
		_cantidad integer;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM security.autenticacion WHERE doc_identidad = _doc);
		IF (_cantidad = 1)
			THEN
				UPDATE security.autenticacion
				SET 
					fecha_fin = 1
				WHERE 
					doc_identidad = _doc;
			RETURN QUERY SELECT true;
		ELSE 
			RETURN QUERY SELECT false;
		END IF;
	END
$$;


ALTER FUNCTION security.f_continuar(_doc bigint) OWNER TO postgres;

--
-- Name: f_guardar_sesion(bigint, text, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.f_guardar_sesion(_doc_identidad bigint, _ip text, _mac text, _session text) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE
		_cantidad integer;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM security.autenticacion WHERE doc_identidad = _doc_identidad);
		IF (_cantidad = 0)
			THEN
				INSERT INTO security.autenticacion
				(
					doc_identidad,
					ip,
					mac,
					fecha_ini,
					sesion,
					fecha_fin
				)
				VALUES
				(
					_doc_identidad,
					_ip,
					_mac,
					current_timestamp,
					_session,
					1
				);
			RETURN QUERY SELECT true;
		ELSEIF (_cantidad = 1)
			THEN
				UPDATE security.autenticacion
				SET 
					doc_identidad = _doc_identidad,
					ip = _ip,
					mac = _mac,
					fecha_ini = current_timestamp,
					sesion = _session,
					fecha_fin = 1
				WHERE 
					doc_identidad = _doc_identidad;
			RETURN QUERY SELECT true;
		ELSE 
			RETURN QUERY SELECT false;
		END IF;
	END
$$;


ALTER FUNCTION security.f_guardar_sesion(_doc_identidad bigint, _ip text, _mac text, _session text) OWNER TO postgres;

--
-- Name: f_log_auditoria(); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.f_log_auditoria() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	 DECLARE
		_pk TEXT :='';		-- Representa la llave primaria de la tabla que esta siedno modificada.
		_sql TEXT;		-- Variable para la creacion del procedured.
		_column_guia RECORD; 	-- Variable para el FOR guarda los nombre de las columnas.
		_column_key RECORD; 	-- Variable para el FOR guarda los PK de las columnas.
		_session TEXT;	-- Almacena el usuario que genera el cambio.
		_user_db TEXT;		-- Almacena el usuario de bd que genera la transaccion.
		_control INT;		-- Variabel de control par alas llaves primarias.
		_count_key INT = 0;	-- Cantidad de columnas pertenecientes al PK.
		_sql_insert TEXT;	-- Variable para la construcción del insert del json de forma dinamica.
		_sql_delete TEXT;	-- Variable para la construcción del delete del json de forma dinamica.
		_sql_update TEXT;	-- Variable para la construcción del update del json de forma dinamica.
		_new_data RECORD; 	-- Fila que representa los campos nuevos del registro.
		_old_data RECORD;	-- Fila que representa los campos viejos del registro.

	BEGIN

			-- Se genera la evaluacion para determianr el tipo de accion sobre la tabla
		 IF (TG_OP = 'INSERT') THEN
			_new_data := NEW;
			_old_data := NEW;
		ELSEIF (TG_OP = 'UPDATE') THEN
			_new_data := NEW;
			_old_data := OLD;
		ELSE
			_new_data := OLD;
			_old_data := OLD;
		END IF;

		-- Se genera la evaluacion para determianr el tipo de accion sobre la tabla
		IF ((SELECT COUNT(*) FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME AND column_name = 'id' ) > 0) THEN
			_pk := _new_data.id;
		ELSE
			_pk := '-1';
		END IF;

		-- Se valida que exista el campo modified_by
		IF ((SELECT COUNT(*) FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME AND column_name = 'session') > 0) THEN
			_session := _new_data.session;
		ELSE
			_session := '';
		END IF;

		-- Se guarda el susuario de bd que genera la transaccion
		_user_db := (SELECT CURRENT_USER);

		-- Se evalua que exista el procedimeinto adecuado
		IF (SELECT COUNT(*) FROM security.function_db_view acfdv WHERE acfdv.b_function = 'field_audit' AND acfdv.b_type_parameters = TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', character varying, character varying, character varying, text, character varying, text, text') > 0
			THEN
				-- Se realiza la invocación del procedured generado dinamivamente
				PERFORM security.field_audit(_new_data, _old_data, TG_OP, _session, _user_db , _pk, ''::text);
		ELSE
			-- Se empieza la construcción del Procedured generico
			_sql := 'CREATE OR REPLACE FUNCTION security.field_audit( _data_new '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', _data_old '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', _accion character varying, _session text, _user_db character varying, _table_pk text, _init text)'
			|| ' RETURNS TEXT AS ''
'
			|| '
'
	|| '	DECLARE
'
	|| '		_column_data TEXT;
	 	_datos jsonb;
	 	
'
	|| '	BEGIN
			_datos = ''''{}'''';
';
			-- Se evalua si hay que actualizar la pk del registro de auditoria.
			IF _pk = '-1'
				THEN
					_sql := _sql
					|| '
		_column_data := ';

					-- Se genera el update con la clave pk de la tabla
					SELECT
						COUNT(isk.column_name)
					INTO
						_control
					FROM
						information_schema.table_constraints istc JOIN information_schema.key_column_usage isk ON isk.constraint_name = istc.constraint_name
					WHERE
						istc.table_schema = TG_TABLE_SCHEMA
					 AND	istc.table_name = TG_TABLE_NAME
					 AND	istc.constraint_type ilike '%primary%';

					-- Se agregan las columnas que componen la pk de la tabla.
					FOR _column_key IN SELECT
							isk.column_name
						FROM
							information_schema.table_constraints istc JOIN information_schema.key_column_usage isk ON isk.constraint_name = istc.constraint_name
						WHERE
							istc.table_schema = TG_TABLE_SCHEMA
						 AND	istc.table_name = TG_TABLE_NAME
						 AND	istc.constraint_type ilike '%primary%'
						ORDER BY 
							isk.ordinal_position  LOOP

						_sql := _sql || ' _data_new.' || _column_key.column_name;
						
						_count_key := _count_key + 1 ;
						
						IF _count_key < _control THEN
							_sql :=	_sql || ' || ' || ''''',''''' || ' ||';
						END IF;
					END LOOP;
				_sql := _sql || ';';
			END IF;

			_sql_insert:='
		IF _accion = ''''INSERT''''
			THEN
				';
			_sql_delete:='
		ELSEIF _accion = ''''DELETE''''
			THEN
				';
			_sql_update:='
		ELSE
			';

			-- Se genera el ciclo de agregado de columnas para el nuevo procedured
			FOR _column_guia IN SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME
				LOOP
						
					_sql_insert:= _sql_insert || '_datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_nuevo'
					|| ''''', '
					|| '_data_new.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_insert:= _sql_insert
						||'::text';
					END IF;

					_sql_insert:= _sql_insert || ')::jsonb;
				';

					_sql_delete := _sql_delete || '_datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_anterior'
					|| ''''', '
					|| '_data_old.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_delete:= _sql_delete
						||'::text';
					END IF;

					_sql_delete:= _sql_delete || ')::jsonb;
				';

					_sql_update := _sql_update || 'IF _data_old.' || _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update || ' <> _data_new.' || _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update || '
				THEN _datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_anterior'
					|| ''''', '
					|| '_data_old.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update
					|| ', '''''
					|| _column_guia.column_name
					|| '_nuevo'
					|| ''''', _data_new.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update
					|| ')::jsonb;
			END IF;
			';
			END LOOP;

			-- Se le agrega la parte final del procedured generico
			
			_sql:= _sql || _sql_insert || _sql_delete || _sql_update
			|| ' 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			''''' || TG_TABLE_SCHEMA || ''''',
			''''' || TG_TABLE_NAME || ''''',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;'''
|| '
LANGUAGE plpgsql;';

			-- Se genera la ejecución de _sql, es decir se crea el nuevo procedured de forma generica.
			EXECUTE _sql;

		-- Se realiza la invocación del procedured generado dinamivamente
			PERFORM security.field_audit(_new_data, _old_data, TG_OP::character varying, _session, _user_db, _pk, ''::text);

		END IF;

		RETURN NULL;

END;
$$;


ALTER FUNCTION security.f_log_auditoria() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: autenticacion; Type: TABLE; Schema: security; Owner: postgres
--

CREATE TABLE security.autenticacion (
    id integer NOT NULL,
    doc_identidad bigint NOT NULL,
    ip text NOT NULL,
    mac text NOT NULL,
    fecha_ini timestamp without time zone NOT NULL,
    sesion text NOT NULL,
    fecha_fin text NOT NULL
);


ALTER TABLE security.autenticacion OWNER TO postgres;

--
-- Name: TABLE autenticacion; Type: COMMENT; Schema: security; Owner: postgres
--

COMMENT ON TABLE security.autenticacion IS 'Tabla de datos para almacenar los datos de los usuarios cuando inician sesión';


--
-- Name: f_obtener_sesion(bigint); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.f_obtener_sesion(_doc_identidad bigint) RETURNS SETOF security.autenticacion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			sc.*
		FROM 
			security.autenticacion sc
		WHERE 
			doc_identidad = _doc_identidad;
	END;
$$;


ALTER FUNCTION security.f_obtener_sesion(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_validar_fecha_fin(bigint); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.f_validar_fecha_fin(_doc_identidad bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF (SELECT fecha_fin FROM security.autenticacion WHERE doc_identidad = _doc_identidad) = null
		THEN
		RETURN QUERY SELECT true;
	ELSE 
		RETURN QUERY SELECT false;
	END IF;
END
$$;


ALTER FUNCTION security.f_validar_fecha_fin(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: atraccion; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.atraccion (
    id_atraccion integer NOT NULL,
    nombre text NOT NULL,
    precio_per integer NOT NULL,
    descripcion text NOT NULL,
    cant_per integer NOT NULL,
    disponibilidad boolean NOT NULL,
    hora_inicio time without time zone,
    hora_fin time without time zone
);


ALTER TABLE usuario.atraccion OWNER TO postgres;

--
-- Name: TABLE atraccion; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.atraccion IS 'tabla para almacenar los datos de las atracciones ';


--
-- Name: field_audit(usuario.atraccion, usuario.atraccion, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.atraccion, _data_old usuario.atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_atraccion;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_atraccion_nuevo', _data_new.id_atraccion)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('precio_per_nuevo', _data_new.precio_per)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				_datos := _datos || json_build_object('cant_per_nuevo', _data_new.cant_per)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('hora_inicio_nuevo', _data_new.hora_inicio)::jsonb;
				_datos := _datos || json_build_object('hora_fin_nuevo', _data_new.hora_fin)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_atraccion_anterior', _data_old.id_atraccion)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('precio_per_anterior', _data_old.precio_per)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				_datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('hora_inicio_anterior', _data_old.hora_inicio)::jsonb;
				_datos := _datos || json_build_object('hora_fin_anterior', _data_old.hora_fin)::jsonb;
				
		ELSE
			IF _data_old.id_atraccion <> _data_new.id_atraccion
				THEN _datos := _datos || json_build_object('id_atraccion_anterior', _data_old.id_atraccion, 'id_atraccion_nuevo', _data_new.id_atraccion)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.precio_per <> _data_new.precio_per
				THEN _datos := _datos || json_build_object('precio_per_anterior', _data_old.precio_per, 'precio_per_nuevo', _data_new.precio_per)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			IF _data_old.cant_per <> _data_new.cant_per
				THEN _datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per, 'cant_per_nuevo', _data_new.cant_per)::jsonb;
			END IF;
			IF _data_old.disponibilidad <> _data_new.disponibilidad
				THEN _datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad, 'disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
			END IF;
			IF _data_old.hora_inicio <> _data_new.hora_inicio
				THEN _datos := _datos || json_build_object('hora_inicio_anterior', _data_old.hora_inicio, 'hora_inicio_nuevo', _data_new.hora_inicio)::jsonb;
			END IF;
			IF _data_old.hora_fin <> _data_new.hora_fin
				THEN _datos := _datos || json_build_object('hora_fin_anterior', _data_old.hora_fin, 'hora_fin_nuevo', _data_new.hora_fin)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'atraccion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.atraccion, _data_old usuario.atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: estado; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.estado (
    id_estado integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE usuario.estado OWNER TO postgres;

--
-- Name: TABLE estado; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.estado IS 'Tabla para almacenar los datos de los estados de la reserva';


--
-- Name: field_audit(usuario.estado, usuario.estado, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.estado, _data_old usuario.estado, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_estado;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_estado_nuevo', _data_new.id_estado)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				
		ELSE
			IF _data_old.id_estado <> _data_new.id_estado
				THEN _datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado, 'id_estado_nuevo', _data_new.id_estado)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'estado',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.estado, _data_old usuario.estado, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: estado_fac; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.estado_fac (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE usuario.estado_fac OWNER TO postgres;

--
-- Name: field_audit(usuario.estado_fac, usuario.estado_fac, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.estado_fac, _data_old usuario.estado_fac, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'estado_fac',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.estado_fac, _data_old usuario.estado_fac, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: factura_atraccion; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.factura_atraccion (
    id_factura integer NOT NULL,
    total_pag bigint NOT NULL,
    id_sedeatr integer NOT NULL,
    doc_identidad bigint NOT NULL,
    fecha_ent date NOT NULL,
    id_sede integer NOT NULL,
    estado integer NOT NULL
);


ALTER TABLE usuario.factura_atraccion OWNER TO postgres;

--
-- Name: field_audit(usuario.factura_atraccion, usuario.factura_atraccion, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.factura_atraccion, _data_old usuario.factura_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_factura;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_factura_nuevo', _data_new.id_factura)::jsonb;
				_datos := _datos || json_build_object('total_pag_nuevo', _data_new.total_pag)::jsonb;
				_datos := _datos || json_build_object('id_sedeatr_nuevo', _data_new.id_sedeatr)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('estado_nuevo', _data_new.estado)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_factura_anterior', _data_old.id_factura)::jsonb;
				_datos := _datos || json_build_object('total_pag_anterior', _data_old.total_pag)::jsonb;
				_datos := _datos || json_build_object('id_sedeatr_anterior', _data_old.id_sedeatr)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('estado_anterior', _data_old.estado)::jsonb;
				
		ELSE
			IF _data_old.id_factura <> _data_new.id_factura
				THEN _datos := _datos || json_build_object('id_factura_anterior', _data_old.id_factura, 'id_factura_nuevo', _data_new.id_factura)::jsonb;
			END IF;
			IF _data_old.total_pag <> _data_new.total_pag
				THEN _datos := _datos || json_build_object('total_pag_anterior', _data_old.total_pag, 'total_pag_nuevo', _data_new.total_pag)::jsonb;
			END IF;
			IF _data_old.id_sedeatr <> _data_new.id_sedeatr
				THEN _datos := _datos || json_build_object('id_sedeatr_anterior', _data_old.id_sedeatr, 'id_sedeatr_nuevo', _data_new.id_sedeatr)::jsonb;
			END IF;
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.fecha_ent <> _data_new.fecha_ent
				THEN _datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent, 'fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.estado <> _data_new.estado
				THEN _datos := _datos || json_build_object('estado_anterior', _data_old.estado, 'estado_nuevo', _data_new.estado)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'factura_atraccion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.factura_atraccion, _data_old usuario.factura_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: factura_habitacion; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.factura_habitacion (
    id_factura integer NOT NULL,
    total_pag bigint NOT NULL,
    id_sedehab integer,
    doc_identidad bigint NOT NULL,
    fecha_ent date,
    id_sede integer,
    estado integer
);


ALTER TABLE usuario.factura_habitacion OWNER TO postgres;

--
-- Name: TABLE factura_habitacion; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.factura_habitacion IS 'Tabla para almacenar los datos de las facturas.';


--
-- Name: field_audit(usuario.factura_habitacion, usuario.factura_habitacion, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.factura_habitacion, _data_old usuario.factura_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_factura;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_factura_nuevo', _data_new.id_factura)::jsonb;
				_datos := _datos || json_build_object('total_pag_nuevo', _data_new.total_pag)::jsonb;
				_datos := _datos || json_build_object('id_sedehab_nuevo', _data_new.id_sedehab)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('estado_nuevo', _data_new.estado)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_factura_anterior', _data_old.id_factura)::jsonb;
				_datos := _datos || json_build_object('total_pag_anterior', _data_old.total_pag)::jsonb;
				_datos := _datos || json_build_object('id_sedehab_anterior', _data_old.id_sedehab)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('estado_anterior', _data_old.estado)::jsonb;
				
		ELSE
			IF _data_old.id_factura <> _data_new.id_factura
				THEN _datos := _datos || json_build_object('id_factura_anterior', _data_old.id_factura, 'id_factura_nuevo', _data_new.id_factura)::jsonb;
			END IF;
			IF _data_old.total_pag <> _data_new.total_pag
				THEN _datos := _datos || json_build_object('total_pag_anterior', _data_old.total_pag, 'total_pag_nuevo', _data_new.total_pag)::jsonb;
			END IF;
			IF _data_old.id_sedehab <> _data_new.id_sedehab
				THEN _datos := _datos || json_build_object('id_sedehab_anterior', _data_old.id_sedehab, 'id_sedehab_nuevo', _data_new.id_sedehab)::jsonb;
			END IF;
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.fecha_ent <> _data_new.fecha_ent
				THEN _datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent, 'fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.estado <> _data_new.estado
				THEN _datos := _datos || json_build_object('estado_anterior', _data_old.estado, 'estado_nuevo', _data_new.estado)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'factura_habitacion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.factura_habitacion, _data_old usuario.factura_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: habitacion; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.habitacion (
    id_habitacion integer NOT NULL,
    tipo_habitacion text NOT NULL,
    cant_per integer NOT NULL,
    precio_hab integer NOT NULL,
    disponibilidad boolean NOT NULL
);


ALTER TABLE usuario.habitacion OWNER TO postgres;

--
-- Name: TABLE habitacion; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.habitacion IS 'Tabla para almacenar los datos de las habitaciones';


--
-- Name: field_audit(usuario.habitacion, usuario.habitacion, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.habitacion, _data_old usuario.habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_habitacion;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_habitacion_nuevo', _data_new.id_habitacion)::jsonb;
				_datos := _datos || json_build_object('tipo_habitacion_nuevo', _data_new.tipo_habitacion)::jsonb;
				_datos := _datos || json_build_object('cant_per_nuevo', _data_new.cant_per)::jsonb;
				_datos := _datos || json_build_object('precio_hab_nuevo', _data_new.precio_hab)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_habitacion_anterior', _data_old.id_habitacion)::jsonb;
				_datos := _datos || json_build_object('tipo_habitacion_anterior', _data_old.tipo_habitacion)::jsonb;
				_datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per)::jsonb;
				_datos := _datos || json_build_object('precio_hab_anterior', _data_old.precio_hab)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad)::jsonb;
				
		ELSE
			IF _data_old.id_habitacion <> _data_new.id_habitacion
				THEN _datos := _datos || json_build_object('id_habitacion_anterior', _data_old.id_habitacion, 'id_habitacion_nuevo', _data_new.id_habitacion)::jsonb;
			END IF;
			IF _data_old.tipo_habitacion <> _data_new.tipo_habitacion
				THEN _datos := _datos || json_build_object('tipo_habitacion_anterior', _data_old.tipo_habitacion, 'tipo_habitacion_nuevo', _data_new.tipo_habitacion)::jsonb;
			END IF;
			IF _data_old.cant_per <> _data_new.cant_per
				THEN _datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per, 'cant_per_nuevo', _data_new.cant_per)::jsonb;
			END IF;
			IF _data_old.precio_hab <> _data_new.precio_hab
				THEN _datos := _datos || json_build_object('precio_hab_anterior', _data_old.precio_hab, 'precio_hab_nuevo', _data_new.precio_hab)::jsonb;
			END IF;
			IF _data_old.disponibilidad <> _data_new.disponibilidad
				THEN _datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad, 'disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'habitacion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.habitacion, _data_old usuario.habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: reserva_atraccion; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.reserva_atraccion (
    id_reserva integer NOT NULL,
    fecha_ent date NOT NULL,
    fecha_sal date NOT NULL,
    id_estado integer NOT NULL,
    id_sede_atra integer NOT NULL,
    doc_identidad bigint NOT NULL,
    id_sede integer NOT NULL
);


ALTER TABLE usuario.reserva_atraccion OWNER TO postgres;

--
-- Name: field_audit(usuario.reserva_atraccion, usuario.reserva_atraccion, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.reserva_atraccion, _data_old usuario.reserva_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_reserva;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_reserva_nuevo', _data_new.id_reserva)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('fecha_sal_nuevo', _data_new.fecha_sal)::jsonb;
				_datos := _datos || json_build_object('id_estado_nuevo', _data_new.id_estado)::jsonb;
				_datos := _datos || json_build_object('id_sede_atra_nuevo', _data_new.id_sede_atra)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_reserva_anterior', _data_old.id_reserva)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('fecha_sal_anterior', _data_old.fecha_sal)::jsonb;
				_datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado)::jsonb;
				_datos := _datos || json_build_object('id_sede_atra_anterior', _data_old.id_sede_atra)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				
		ELSE
			IF _data_old.id_reserva <> _data_new.id_reserva
				THEN _datos := _datos || json_build_object('id_reserva_anterior', _data_old.id_reserva, 'id_reserva_nuevo', _data_new.id_reserva)::jsonb;
			END IF;
			IF _data_old.fecha_ent <> _data_new.fecha_ent
				THEN _datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent, 'fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
			END IF;
			IF _data_old.fecha_sal <> _data_new.fecha_sal
				THEN _datos := _datos || json_build_object('fecha_sal_anterior', _data_old.fecha_sal, 'fecha_sal_nuevo', _data_new.fecha_sal)::jsonb;
			END IF;
			IF _data_old.id_estado <> _data_new.id_estado
				THEN _datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado, 'id_estado_nuevo', _data_new.id_estado)::jsonb;
			END IF;
			IF _data_old.id_sede_atra <> _data_new.id_sede_atra
				THEN _datos := _datos || json_build_object('id_sede_atra_anterior', _data_old.id_sede_atra, 'id_sede_atra_nuevo', _data_new.id_sede_atra)::jsonb;
			END IF;
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'reserva_atraccion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.reserva_atraccion, _data_old usuario.reserva_atraccion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: reserva_habitacion; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.reserva_habitacion (
    id_reserva integer NOT NULL,
    fecha_ent date NOT NULL,
    fecha_sal date NOT NULL,
    id_estado integer NOT NULL,
    id_sede_hab integer,
    doc_identidad bigint,
    id_sede integer
);


ALTER TABLE usuario.reserva_habitacion OWNER TO postgres;

--
-- Name: TABLE reserva_habitacion; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.reserva_habitacion IS 'Tabla para almacenar los datos de las reservas';


--
-- Name: field_audit(usuario.reserva_habitacion, usuario.reserva_habitacion, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.reserva_habitacion, _data_old usuario.reserva_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_reserva;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_reserva_nuevo', _data_new.id_reserva)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('fecha_sal_nuevo', _data_new.fecha_sal)::jsonb;
				_datos := _datos || json_build_object('id_estado_nuevo', _data_new.id_estado)::jsonb;
				_datos := _datos || json_build_object('id_sede_hab_nuevo', _data_new.id_sede_hab)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_reserva_anterior', _data_old.id_reserva)::jsonb;
				_datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent)::jsonb;
				_datos := _datos || json_build_object('fecha_sal_anterior', _data_old.fecha_sal)::jsonb;
				_datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado)::jsonb;
				_datos := _datos || json_build_object('id_sede_hab_anterior', _data_old.id_sede_hab)::jsonb;
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				
		ELSE
			IF _data_old.id_reserva <> _data_new.id_reserva
				THEN _datos := _datos || json_build_object('id_reserva_anterior', _data_old.id_reserva, 'id_reserva_nuevo', _data_new.id_reserva)::jsonb;
			END IF;
			IF _data_old.fecha_ent <> _data_new.fecha_ent
				THEN _datos := _datos || json_build_object('fecha_ent_anterior', _data_old.fecha_ent, 'fecha_ent_nuevo', _data_new.fecha_ent)::jsonb;
			END IF;
			IF _data_old.fecha_sal <> _data_new.fecha_sal
				THEN _datos := _datos || json_build_object('fecha_sal_anterior', _data_old.fecha_sal, 'fecha_sal_nuevo', _data_new.fecha_sal)::jsonb;
			END IF;
			IF _data_old.id_estado <> _data_new.id_estado
				THEN _datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado, 'id_estado_nuevo', _data_new.id_estado)::jsonb;
			END IF;
			IF _data_old.id_sede_hab <> _data_new.id_sede_hab
				THEN _datos := _datos || json_build_object('id_sede_hab_anterior', _data_old.id_sede_hab, 'id_sede_hab_nuevo', _data_new.id_sede_hab)::jsonb;
			END IF;
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'reserva_habitacion',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.reserva_habitacion, _data_old usuario.reserva_habitacion, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: rol; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.rol (
    id_rol integer NOT NULL,
    nombre text NOT NULL
);


ALTER TABLE usuario.rol OWNER TO postgres;

--
-- Name: TABLE rol; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.rol IS 'Tabla para almacenar los datos de los roles';


--
-- Name: field_audit(usuario.rol, usuario.rol, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.rol, _data_old usuario.rol, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_rol;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_rol_nuevo', _data_new.id_rol)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				
		ELSE
			IF _data_old.id_rol <> _data_new.id_rol
				THEN _datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol, 'id_rol_nuevo', _data_new.id_rol)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'rol',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.rol, _data_old usuario.rol, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: sede; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.sede (
    id_sede integer NOT NULL,
    nombre text NOT NULL,
    direccion text NOT NULL,
    telefono bigint NOT NULL,
    id_admi bigint NOT NULL
);


ALTER TABLE usuario.sede OWNER TO postgres;

--
-- Name: TABLE sede; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.sede IS 'Tabla para almacenar los datos de las sedes';


--
-- Name: field_audit(usuario.sede, usuario.sede, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.sede, _data_old usuario.sede, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_sede;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('direccion_nuevo', _data_new.direccion)::jsonb;
				_datos := _datos || json_build_object('telefono_nuevo', _data_new.telefono)::jsonb;
				_datos := _datos || json_build_object('id_admi_nuevo', _data_new.id_admi)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('direccion_anterior', _data_old.direccion)::jsonb;
				_datos := _datos || json_build_object('telefono_anterior', _data_old.telefono)::jsonb;
				_datos := _datos || json_build_object('id_admi_anterior', _data_old.id_admi)::jsonb;
				
		ELSE
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.direccion <> _data_new.direccion
				THEN _datos := _datos || json_build_object('direccion_anterior', _data_old.direccion, 'direccion_nuevo', _data_new.direccion)::jsonb;
			END IF;
			IF _data_old.telefono <> _data_new.telefono
				THEN _datos := _datos || json_build_object('telefono_anterior', _data_old.telefono, 'telefono_nuevo', _data_new.telefono)::jsonb;
			END IF;
			IF _data_old.id_admi <> _data_new.id_admi
				THEN _datos := _datos || json_build_object('id_admi_anterior', _data_old.id_admi, 'id_admi_nuevo', _data_new.id_admi)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'sede',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.sede, _data_old usuario.sede, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: sedeatra; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.sedeatra (
    id_sede_atra integer NOT NULL,
    descripcion text NOT NULL,
    cant_per integer NOT NULL,
    hora_ini time(6) without time zone NOT NULL,
    hora_fin time(6) without time zone NOT NULL,
    disponibilidad boolean NOT NULL,
    id_atraccion integer NOT NULL,
    id_sede integer NOT NULL,
    nombre text NOT NULL,
    precio integer NOT NULL
);


ALTER TABLE usuario.sedeatra OWNER TO postgres;

--
-- Name: TABLE sedeatra; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.sedeatra IS 'Tabla para almacenar los datos de las atracciones de la sede';


--
-- Name: field_audit(usuario.sedeatra, usuario.sedeatra, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.sedeatra, _data_old usuario.sedeatra, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_sede_atra;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_sede_atra_nuevo', _data_new.id_sede_atra)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				_datos := _datos || json_build_object('cant_per_nuevo', _data_new.cant_per)::jsonb;
				_datos := _datos || json_build_object('hora_ini_nuevo', _data_new.hora_ini)::jsonb;
				_datos := _datos || json_build_object('hora_fin_nuevo', _data_new.hora_fin)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('id_atraccion_nuevo', _data_new.id_atraccion)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('precio_nuevo', _data_new.precio)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_sede_atra_anterior', _data_old.id_sede_atra)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				_datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per)::jsonb;
				_datos := _datos || json_build_object('hora_ini_anterior', _data_old.hora_ini)::jsonb;
				_datos := _datos || json_build_object('hora_fin_anterior', _data_old.hora_fin)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('id_atraccion_anterior', _data_old.id_atraccion)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('precio_anterior', _data_old.precio)::jsonb;
				
		ELSE
			IF _data_old.id_sede_atra <> _data_new.id_sede_atra
				THEN _datos := _datos || json_build_object('id_sede_atra_anterior', _data_old.id_sede_atra, 'id_sede_atra_nuevo', _data_new.id_sede_atra)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			IF _data_old.cant_per <> _data_new.cant_per
				THEN _datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per, 'cant_per_nuevo', _data_new.cant_per)::jsonb;
			END IF;
			IF _data_old.hora_ini <> _data_new.hora_ini
				THEN _datos := _datos || json_build_object('hora_ini_anterior', _data_old.hora_ini, 'hora_ini_nuevo', _data_new.hora_ini)::jsonb;
			END IF;
			IF _data_old.hora_fin <> _data_new.hora_fin
				THEN _datos := _datos || json_build_object('hora_fin_anterior', _data_old.hora_fin, 'hora_fin_nuevo', _data_new.hora_fin)::jsonb;
			END IF;
			IF _data_old.disponibilidad <> _data_new.disponibilidad
				THEN _datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad, 'disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
			END IF;
			IF _data_old.id_atraccion <> _data_new.id_atraccion
				THEN _datos := _datos || json_build_object('id_atraccion_anterior', _data_old.id_atraccion, 'id_atraccion_nuevo', _data_new.id_atraccion)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.precio <> _data_new.precio
				THEN _datos := _datos || json_build_object('precio_anterior', _data_old.precio, 'precio_nuevo', _data_new.precio)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'sedeatra',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.sedeatra, _data_old usuario.sedeatra, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: sedehab; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.sedehab (
    id_sede_hab integer NOT NULL,
    tipo_hab text NOT NULL,
    cant_per integer NOT NULL,
    disponibilidad boolean NOT NULL,
    id_habitacion integer NOT NULL,
    id_sede integer NOT NULL,
    precio integer NOT NULL
);


ALTER TABLE usuario.sedehab OWNER TO postgres;

--
-- Name: TABLE sedehab; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.sedehab IS 'Tabla para almacenar los datos de las habitaciones de la sede';


--
-- Name: field_audit(usuario.sedehab, usuario.sedehab, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.sedehab, _data_old usuario.sedehab, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id_sede_hab;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_sede_hab_nuevo', _data_new.id_sede_hab)::jsonb;
				_datos := _datos || json_build_object('tipo_hab_nuevo', _data_new.tipo_hab)::jsonb;
				_datos := _datos || json_build_object('cant_per_nuevo', _data_new.cant_per)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('id_habitacion_nuevo', _data_new.id_habitacion)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				_datos := _datos || json_build_object('precio_nuevo', _data_new.precio)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_sede_hab_anterior', _data_old.id_sede_hab)::jsonb;
				_datos := _datos || json_build_object('tipo_hab_anterior', _data_old.tipo_hab)::jsonb;
				_datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per)::jsonb;
				_datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad)::jsonb;
				_datos := _datos || json_build_object('id_habitacion_anterior', _data_old.id_habitacion)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				_datos := _datos || json_build_object('precio_anterior', _data_old.precio)::jsonb;
				
		ELSE
			IF _data_old.id_sede_hab <> _data_new.id_sede_hab
				THEN _datos := _datos || json_build_object('id_sede_hab_anterior', _data_old.id_sede_hab, 'id_sede_hab_nuevo', _data_new.id_sede_hab)::jsonb;
			END IF;
			IF _data_old.tipo_hab <> _data_new.tipo_hab
				THEN _datos := _datos || json_build_object('tipo_hab_anterior', _data_old.tipo_hab, 'tipo_hab_nuevo', _data_new.tipo_hab)::jsonb;
			END IF;
			IF _data_old.cant_per <> _data_new.cant_per
				THEN _datos := _datos || json_build_object('cant_per_anterior', _data_old.cant_per, 'cant_per_nuevo', _data_new.cant_per)::jsonb;
			END IF;
			IF _data_old.disponibilidad <> _data_new.disponibilidad
				THEN _datos := _datos || json_build_object('disponibilidad_anterior', _data_old.disponibilidad, 'disponibilidad_nuevo', _data_new.disponibilidad)::jsonb;
			END IF;
			IF _data_old.id_habitacion <> _data_new.id_habitacion
				THEN _datos := _datos || json_build_object('id_habitacion_anterior', _data_old.id_habitacion, 'id_habitacion_nuevo', _data_new.id_habitacion)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			IF _data_old.precio <> _data_new.precio
				THEN _datos := _datos || json_build_object('precio_anterior', _data_old.precio, 'precio_nuevo', _data_new.precio)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'sedehab',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.sedehab, _data_old usuario.sedehab, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: usuario; Type: TABLE; Schema: usuario; Owner: postgres
--

CREATE TABLE usuario.usuario (
    doc_identidad bigint NOT NULL,
    nombre text NOT NULL,
    apellido text NOT NULL,
    edad integer NOT NULL,
    num_cel bigint NOT NULL,
    correo_elec text NOT NULL,
    contrasena text NOT NULL,
    id_rol integer NOT NULL,
    session text,
    id_sede integer
);


ALTER TABLE usuario.usuario OWNER TO postgres;

--
-- Name: TABLE usuario; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON TABLE usuario.usuario IS 'Tabla para los datos del usuario';


--
-- Name: field_audit(usuario.usuario, usuario.usuario, character varying, text, character varying, text, text); Type: FUNCTION; Schema: security; Owner: postgres
--

CREATE FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.doc_identidad;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('apellido_nuevo', _data_new.apellido)::jsonb;
				_datos := _datos || json_build_object('edad_nuevo', _data_new.edad)::jsonb;
				_datos := _datos || json_build_object('num_cel_nuevo', _data_new.num_cel)::jsonb;
				_datos := _datos || json_build_object('correo_elec_nuevo', _data_new.correo_elec)::jsonb;
				_datos := _datos || json_build_object('contrasena_nuevo', _data_new.contrasena)::jsonb;
				_datos := _datos || json_build_object('id_rol_nuevo', _data_new.id_rol)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('id_sede_nuevo', _data_new.id_sede)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('apellido_anterior', _data_old.apellido)::jsonb;
				_datos := _datos || json_build_object('edad_anterior', _data_old.edad)::jsonb;
				_datos := _datos || json_build_object('num_cel_anterior', _data_old.num_cel)::jsonb;
				_datos := _datos || json_build_object('correo_elec_anterior', _data_old.correo_elec)::jsonb;
				_datos := _datos || json_build_object('contrasena_anterior', _data_old.contrasena)::jsonb;
				_datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede)::jsonb;
				
		ELSE
			IF _data_old.doc_identidad <> _data_new.doc_identidad
				THEN _datos := _datos || json_build_object('doc_identidad_anterior', _data_old.doc_identidad, 'doc_identidad_nuevo', _data_new.doc_identidad)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.apellido <> _data_new.apellido
				THEN _datos := _datos || json_build_object('apellido_anterior', _data_old.apellido, 'apellido_nuevo', _data_new.apellido)::jsonb;
			END IF;
			IF _data_old.edad <> _data_new.edad
				THEN _datos := _datos || json_build_object('edad_anterior', _data_old.edad, 'edad_nuevo', _data_new.edad)::jsonb;
			END IF;
			IF _data_old.num_cel <> _data_new.num_cel
				THEN _datos := _datos || json_build_object('num_cel_anterior', _data_old.num_cel, 'num_cel_nuevo', _data_new.num_cel)::jsonb;
			END IF;
			IF _data_old.correo_elec <> _data_new.correo_elec
				THEN _datos := _datos || json_build_object('correo_elec_anterior', _data_old.correo_elec, 'correo_elec_nuevo', _data_new.correo_elec)::jsonb;
			END IF;
			IF _data_old.contrasena <> _data_new.contrasena
				THEN _datos := _datos || json_build_object('contrasena_anterior', _data_old.contrasena, 'contrasena_nuevo', _data_new.contrasena)::jsonb;
			END IF;
			IF _data_old.id_rol <> _data_new.id_rol
				THEN _datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol, 'id_rol_nuevo', _data_new.id_rol)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.id_sede <> _data_new.id_sede
				THEN _datos := _datos || json_build_object('id_sede_anterior', _data_old.id_sede, 'id_sede_nuevo', _data_new.id_sede)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'usuario',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;


ALTER FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) OWNER TO postgres;

--
-- Name: f_consulta_clien(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_consulta_clien(_doc_identidad bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	filtro integer;
		BEGIN
			filtro := (SELECT id_rol FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
			IF filtro = 4 
			THEN 
				RETURN QUERY
				SELECT 
					uu.*
				FROM 
					usuario.usuario uu 
				WHERE doc_identidad = _doc_identidad;
			ELSE 
				RETURN QUERY SELECT 
					1::BIGINT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					1::BIGINT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					''::TEXT,
					1::INTEGER;
			END IF;
		END
$$;


ALTER FUNCTION usuario.f_consulta_clien(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_consulta_emple_clien(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_consulta_emple_clien(_doc_identidad bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	filtro integer;
		BEGIN
			filtro := (SELECT id_rol FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
			IF (filtro = 3) OR filtro = 4 
			THEN 
				RETURN QUERY
				SELECT 
					uu.*
				FROM 
					usuario.usuario uu 
				WHERE doc_identidad = _doc_identidad;
			ELSE 
				RETURN QUERY SELECT 
					1::BIGINT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					1::BIGINT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					''::TEXT,
					1::INTEGER;
			END IF;
		END
$$;


ALTER FUNCTION usuario.f_consulta_emple_clien(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_consulta_reserva(integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_consulta_reserva(_id_reserva integer) RETURNS SETOF usuario.reserva_habitacion
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	filtro integer;
		BEGIN
			filtro := (SELECT id_estado FROM usuario.reserva WHERE id_reserva = _id_reserva);
			IF filtro = 1
			THEN 
				RETURN QUERY
				SELECT 
					ur.*
				FROM 
					usuario.reserva ur 
				WHERE id_reserva = _id_reserva;
			ELSE 
				RETURN QUERY SELECT 
					-1::INTEGER,
					''::TIMESTAMP,
					''::TIMESTAMP,
					''::JSON,
					1::INTEGER,
					1::INTEGER,
					1::INTEGER;
			END IF;
		END
$$;


ALTER FUNCTION usuario.f_consulta_reserva(_id_reserva integer) OWNER TO postgres;

--
-- Name: f_eliminar_atraccion(integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_eliminar_atraccion(_id_atraccion integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		DELETE FROM usuario.atraccion
		WHERE id_atraccion = _id_atraccion;
	END;
$$;


ALTER FUNCTION usuario.f_eliminar_atraccion(_id_atraccion integer) OWNER TO postgres;

--
-- Name: f_eliminar_atraccion_sede(integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_eliminar_atraccion_sede(_id_atraccion integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		DELETE FROM usuario.sedeatra
		WHERE id_sede_atra = _id_atraccion;
	END;
$$;


ALTER FUNCTION usuario.f_eliminar_atraccion_sede(_id_atraccion integer) OWNER TO postgres;

--
-- Name: f_eliminar_habitacion(integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_eliminar_habitacion(_id_habitacion integer) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		DELETE FROM usuario.habitacion WHERE id_habitacion = _id_habitacion;
	END;
$$;


ALTER FUNCTION usuario.f_eliminar_habitacion(_id_habitacion integer) OWNER TO postgres;

--
-- Name: f_eliminar_habitacion_sede(integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_eliminar_habitacion_sede(_id_hab integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		DELETE FROM usuario.sedehab
		WHERE id_sede_hab = _id_hab;
	END;
$$;


ALTER FUNCTION usuario.f_eliminar_habitacion_sede(_id_hab integer) OWNER TO postgres;

--
-- Name: f_eliminar_sede(integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_eliminar_sede(_id_sede integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad int;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE id_sede = _id_sede);
		IF _cantidad = 0
		THEN 
			DELETE FROM usuario.sede WHERE id_sede = _id_sede;
			RETURN QUERY SELECT true;
		ELSE 
			RETURN QUERY SELECT false;
		END IF;
	END
$$;


ALTER FUNCTION usuario.f_eliminar_sede(_id_sede integer) OWNER TO postgres;

--
-- Name: f_eliminar_usuario(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_eliminar_usuario(_doc_identidad bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad int;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.sede WHERE id_admi = _doc_identidad);
		IF _cantidad = 0
			THEN 
				DELETE FROM usuario.usuario
				WHERE doc_identidad = _doc_identidad;
			RETURN QUERY SELECT true;
		ELSE
			RETURN QUERY SELECT false;
		END IF;
	END;
$$;


ALTER FUNCTION usuario.f_eliminar_usuario(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_entregar_reserva_atr(integer, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_entregar_reserva_atr(_id_sede_atr integer, _id_reserva integer) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		UPDATE usuario.sedeatra
			SET 
				disponibilidad = true
			WHERE 
				id_sede_atra = _id_sede_atr;
		UPDATE usuario.reserva_atraccion
			SET 
				id_estado = 2
			WHERE 
				id_reserva = _id_reserva;
	END
$$;


ALTER FUNCTION usuario.f_entregar_reserva_atr(_id_sede_atr integer, _id_reserva integer) OWNER TO postgres;

--
-- Name: f_entregar_reservar(integer, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_entregar_reservar(_id_sede_hab integer, _id_reserva integer) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		UPDATE usuario.sedehab
			SET 
				disponibilidad = true
			WHERE 
				id_sede_hab = _id_sede_hab;
		UPDATE usuario.reserva_habitacion
			SET 
				id_estado = 2
			WHERE 
				id_reserva = _id_reserva;
	END
$$;


ALTER FUNCTION usuario.f_entregar_reservar(_id_sede_hab integer, _id_reserva integer) OWNER TO postgres;

--
-- Name: f_insertar_atraccion(text, integer, text, integer, boolean, time without time zone, time without time zone); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_insertar_atraccion(_nombre text, _precio_per integer, _descripcion text, _cant_per integer, _disponibilidad boolean, _hora_ini time without time zone, _hora_fin time without time zone) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		IF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) = (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) <= 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					INSERT INTO usuario.atraccion
						(		
						nombre,
						precio_per,
						descripcion,
						cant_per,
						disponibilidad,
						hora_inicio,
						hora_fin
						)
					VALUES 
						(	
						_nombre,
						_precio_per,
						_descripcion,
						_cant_per,
						_disponibilidad,
						_hora_ini,
						_hora_fin
						);
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		ELSEIF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) > (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) <= 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					INSERT INTO usuario.atraccion
						(		
						nombre,
						precio_per,
						descripcion,
						cant_per,
						disponibilidad,
						hora_inicio,
						hora_fin
						)
					VALUES 
						(	
						_nombre,
						_precio_per,
						_descripcion,
						_cant_per,
						_disponibilidad,
						_hora_ini,
						_hora_fin
						);
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		ELSEIF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) < (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) < 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					INSERT INTO usuario.atraccion
						(		
						nombre,
						precio_per,
						descripcion,
						cant_per,
						disponibilidad,
						hora_inicio,
						hora_fin
						)
					VALUES 
						(	
						_nombre,
						_precio_per,
						_descripcion,
						_cant_per,
						_disponibilidad,
						_hora_ini,
						_hora_fin
						);
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		END IF;
	END
$$;


ALTER FUNCTION usuario.f_insertar_atraccion(_nombre text, _precio_per integer, _descripcion text, _cant_per integer, _disponibilidad boolean, _hora_ini time without time zone, _hora_fin time without time zone) OWNER TO postgres;

--
-- Name: f_insertar_atraccion_sede(integer, integer, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_insertar_atraccion_sede(_id_atraccion integer, _id_sede integer, _precio integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
		_nombre text;
		_descripcion text;
		_cant integer;
		_hora_ini time without time zone;
		_hora_fin time without time zone;
		_disponibilidad boolean;
	BEGIN
		_nombre := (SELECT nombre FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_descripcion := (SELECT descripcion FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_cant := (SELECT cant_per FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_hora_ini := (SELECT hora_inicio FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_hora_fin := (SELECT hora_fin FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		_disponibilidad := (SELECT disponibilidad FROM usuario.atraccion WHERE id_atraccion = _id_atraccion);
		
		INSERT INTO usuario.sedeatra
		(
			nombre,
			descripcion,
			cant_per,
			hora_ini,
			hora_fin,
			disponibilidad,
			id_atraccion,
			id_sede,
			precio
		)
		VALUES 
		(
			_nombre,
			_descripcion,
			_cant,
			_hora_ini,
			_hora_fin,
			_disponibilidad,
			_id_atraccion,
			_id_sede,
			_precio
		);
	END
$$;


ALTER FUNCTION usuario.f_insertar_atraccion_sede(_id_atraccion integer, _id_sede integer, _precio integer) OWNER TO postgres;

--
-- Name: f_insertar_factura(bigint, integer, bigint, date, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_insertar_factura(_total bigint, _id_sede_hab integer, _doc_identidad bigint, _fecha_sal date, _estado integer) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	precioo integer;
	_id_sede integer;
	BEGIN
		precioo := (SELECT precio FROM usuario.sedehab WHERE id_sede_hab = _id_sede_hab);
		_id_sede := (SELECT id_sede FROM usuario.sedehab WHERE id_sede_hab = _id_sede_hab);
		INSERT INTO usuario.factura_habitacion
		(
			total_pag,
			id_sedehab,
			doc_identidad,
			fecha_ent,
			id_sede,
			estado
		)
		VALUES 
		(
			_total * precioo,
			_id_sede_hab,
			_doc_identidad,
			_fecha_sal,
			_id_sede,
			_estado
		);
	END
$$;


ALTER FUNCTION usuario.f_insertar_factura(_total bigint, _id_sede_hab integer, _doc_identidad bigint, _fecha_sal date, _estado integer) OWNER TO postgres;

--
-- Name: f_insertar_factura_atra(bigint, integer, bigint, date, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_insertar_factura_atra(_total bigint, _id_sede_atr integer, _doc_identidad bigint, _fecha_sal date, _estado integer) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	precioo integer;
	_id_sede integer;
	BEGIN
		precioo := (SELECT precio FROM usuario.sedeatra WHERE id_sede_atra = _id_sede_atr);
		_id_sede := (SELECT id_sede FROM usuario.sedeatra WHERE id_sede_atra = _id_sede_atr);
		INSERT INTO usuario.factura_atraccion
		(
			total_pag,
			id_sedeatr,
			doc_identidad,
			fecha_ent,
			id_sede,
			estado
		)
		VALUES 
		(
			_total * precioo,
			_id_sede_atr,
			_doc_identidad,
			_fecha_sal,
			_id_sede,
			_estado
		);
	END
$$;


ALTER FUNCTION usuario.f_insertar_factura_atra(_total bigint, _id_sede_atr integer, _doc_identidad bigint, _fecha_sal date, _estado integer) OWNER TO postgres;

--
-- Name: f_insertar_habitacion(text, integer, integer, boolean); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_insertar_habitacion(_tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	BEGIN
		INSERT INTO usuario.habitacion
		(
			tipo_habitacion,
			cant_per,
			precio_hab,
			disponibilidad
		)
		VALUES 
		(
			_tipo_hab,
			_cant_per,
			_precio_hab,
			_disponibilidad
		);
	END
$$;


ALTER FUNCTION usuario.f_insertar_habitacion(_tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean) OWNER TO postgres;

--
-- Name: f_insertar_habitacion_sede(integer, integer, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_insertar_habitacion_sede(_id_habi integer, _id_sede integer, _precio integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
		_tipo_hab text;
		_cant integer;
		_disponibilidad boolean;
	BEGIN
		_tipo_hab := (SELECT tipo_habitacion FROM usuario.habitacion WHERE id_habitacion = _id_habi);
		_cant := (SELECT cant_per FROM usuario.habitacion WHERE id_habitacion = _id_habi);
		_disponibilidad := (SELECT disponibilidad FROM usuario.habitacion WHERE id_habitacion = _id_habi);
		
		INSERT INTO usuario.sedehab
		(
			tipo_hab,
			cant_per,
			disponibilidad,
			id_habitacion,
			id_sede,
			precio
		)
		VALUES 
		(
			_tipo_hab,
			_cant,
			_disponibilidad,
			_id_habi,
			_id_sede,
			_precio
		);
	END
$$;


ALTER FUNCTION usuario.f_insertar_habitacion_sede(_id_habi integer, _id_sede integer, _precio integer) OWNER TO postgres;

--
-- Name: f_insertar_sede(text, text, bigint, bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_insertar_sede(_nombre text, _direccion text, _telefono bigint, _id_admi bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	BEGIN
		INSERT INTO usuario.sede
		(
			nombre,
			direccion,
			telefono,
			id_admi
		)
		VALUES 
		(
			_nombre,
			_direccion,
			_telefono,
			_id_admi
		);
	END
$$;


ALTER FUNCTION usuario.f_insertar_sede(_nombre text, _direccion text, _telefono bigint, _id_admi bigint) OWNER TO postgres;

--
-- Name: f_insertaru(bigint, text, text, integer, bigint, text, text, integer, text, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_insertaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _id_rol integer, _session text, _id_sede integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad integer;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		IF _cantidad = 0 
			THEN 
				INSERT INTO usuario.usuario
				(
					doc_identidad,
					nombre,
					apellido,
					edad,
					num_Cel,
					correo_Elec,
					contrasena,
					id_rol,
					session,
					id_sede
				)
				VALUES 
				(
					_doc_identidad,
					_nombre,
					_apellido,
					_edad,
					_num_cel,
					_correo_elec,
					_contrasena,
					_id_rol,
					_session,
					_id_sede
				);
			RETURN QUERY SELECT true;
		ELSE
			RETURN QUERY SELECT false;
		END IF;
	END
$$;


ALTER FUNCTION usuario.f_insertaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _id_rol integer, _session text, _id_sede integer) OWNER TO postgres;

--
-- Name: f_leer_atraccion(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_atraccion() RETURNS SETOF usuario.atraccion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			ua.*
		FROM 
			usuario.atraccion ua;
	END;
$$;


ALTER FUNCTION usuario.f_leer_atraccion() OWNER TO postgres;

--
-- Name: v_atraccion_sede; Type: VIEW; Schema: usuario; Owner: postgres
--

CREATE VIEW usuario.v_atraccion_sede AS
 SELECT 0 AS id_sede_atraccion,
    ''::text AS nombre,
    ''::text AS descripcion,
    0 AS cant_per,
    0 AS precio,
    '00:00:00'::time without time zone AS hora_ini,
    '00:00:00'::time without time zone AS hora_fin,
    true AS disponibilidad,
    ''::text AS nombre_sede;


ALTER TABLE usuario.v_atraccion_sede OWNER TO postgres;

--
-- Name: f_leer_atraccion_sede(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_atraccion_sede() RETURNS SETOF usuario.v_atraccion_sede
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
		
			us.id_sede_atra,
			us.nombre,
			us.descripcion,
			us.cant_per,
			us.precio,
			us.hora_ini,
			us.hora_fin,
			us.disponibilidad,
			uu.nombre
		FROM usuario.sedeatra us
			JOIN usuario.sede uu ON uu.id_sede = us.id_sede;
	END;
$$;


ALTER FUNCTION usuario.f_leer_atraccion_sede() OWNER TO postgres;

--
-- Name: f_leer_atraccion_sede(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_atraccion_sede(_doc_identidad bigint) RETURNS SETOF usuario.v_atraccion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	_sede int;
	BEGIN 
		_sede := (SELECT id_sede FROM usuario.usuario WHERE doc_identidad =_doc_identidad);
		RETURN QUERY 
		SELECT 
			us.id_sede_atra,
			us.nombre,
			us.descripcion,
			us.cant_per,
			us.precio,
			us.hora_ini,
			us.hora_fin,
			us.disponibilidad,
			uu.nombre
		FROM usuario.sedeatra us
			JOIN usuario.sede uu ON uu.id_sede = us.id_sede
		WHERE us.id_sede = _sede;
	END;
$$;


ALTER FUNCTION usuario.f_leer_atraccion_sede(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_leer_atraccion_sede_consulta(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_atraccion_sede_consulta(_doc_identidad bigint) RETURNS SETOF usuario.v_atraccion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	BEGIN 
		RETURN QUERY 
		SELECT 
			us.id_sede_atra,
			us.nombre,
			us.descripcion,
			us.cant_per,
			us.precio,
			us.hora_ini,
			us.hora_fin,
			us.disponibilidad,
			uu.nombre
		FROM usuario.sedeatra us
			JOIN usuario.sede uu ON uu.id_sede = us.id_sede
		WHERE us.id_atraccion = _doc_identidad;
	END;
$$;


ALTER FUNCTION usuario.f_leer_atraccion_sede_consulta(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: v_factura; Type: VIEW; Schema: usuario; Owner: postgres
--

CREATE VIEW usuario.v_factura AS
 SELECT 0 AS id_reserva,
    (0)::bigint AS total,
    ''::text AS sede_atra,
    (0)::bigint AS doc_identidad,
    '2019-01-01'::date AS fecha_sal,
    ''::text AS sede,
    ''::text AS estado;


ALTER TABLE usuario.v_factura OWNER TO postgres;

--
-- Name: f_leer_factura_atraccion(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_factura_atraccion() RETURNS SETOF usuario.v_factura
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			ur.id_factura,
			ur.total_pag,
			us.nombre,
			uu.doc_identidad,
			ur.fecha_ent,
			uq.nombre,
			ue.descripcion
		FROM usuario.factura_atraccion ur
			JOIN usuario.estado_fac ue ON ur.estado = ue.id
			JOIN usuario.sedeatra us ON ur.id_sedeatr = us.id_sede_atra
			JOIN usuario.usuario uu ON ur.doc_identidad = uu.doc_identidad
			JOIN usuario.sede uq ON ur.id_sede = uq.id_sede;
	END;
$$;


ALTER FUNCTION usuario.f_leer_factura_atraccion() OWNER TO postgres;

--
-- Name: f_leer_factura_atraccion_re(integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_factura_atraccion_re(_id_reserva integer) RETURNS SETOF usuario.v_factura
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			ur.id_factura,
			ur.total_pag,
			us.nombre,
			uu.doc_identidad,
			ur.fecha_ent,
			uq.nombre,
			ue.descripcion
		FROM usuario.factura_atraccion ur
			JOIN usuario.estado_fac ue ON ur.estado = ue.id
			JOIN usuario.sedeatra us ON ur.id_sedeatr = us.id_sede_atra
			JOIN usuario.usuario uu ON ur.doc_identidad = uu.doc_identidad
			JOIN usuario.sede uq ON ur.id_sede = uq.id_sede
		WHERE ur.estado = 2 AND ur.id_factura = _id_reserva;
	END;
$$;


ALTER FUNCTION usuario.f_leer_factura_atraccion_re(_id_reserva integer) OWNER TO postgres;

--
-- Name: f_leer_habitacion(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_habitacion() RETURNS SETOF usuario.habitacion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uh.*
		FROM 
			usuario.habitacion uh;
	END;
$$;


ALTER FUNCTION usuario.f_leer_habitacion() OWNER TO postgres;

--
-- Name: v_habitacion_sede; Type: VIEW; Schema: usuario; Owner: postgres
--

CREATE VIEW usuario.v_habitacion_sede AS
 SELECT 0 AS id_sede_habitacion,
    ''::text AS tipo_hab,
    0 AS cant_per,
    true AS disponibilidad,
    0 AS precio,
    ''::text AS nombre_sede;


ALTER TABLE usuario.v_habitacion_sede OWNER TO postgres;

--
-- Name: f_leer_habitacion_sede(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_habitacion_sede(_doc_identidad bigint) RETURNS SETOF usuario.v_habitacion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	_sede int;
	BEGIN 
		_sede := (SELECT id_sede FROM usuario.usuario WHERE doc_identidad =_doc_identidad);
		RETURN QUERY 
		SELECT 
			uh.id_sede_hab,
			uh.tipo_hab,
			uh.cant_per,
			uh.disponibilidad,
			uh.precio,
			uu.nombre
		FROM usuario.sedehab uh
			JOIN usuario.sede uu ON uu.id_sede = uh.id_sede
		WHERE uh.id_sede = _sede;
	END;
$$;


ALTER FUNCTION usuario.f_leer_habitacion_sede(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_leer_habitacion_sede_consulta(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_habitacion_sede_consulta(_doc_identidad bigint) RETURNS SETOF usuario.v_habitacion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	BEGIN 
		RETURN QUERY 
		SELECT 
			uh.id_sede_hab,
			uh.tipo_hab,
			uh.cant_per,
			uh.disponibilidad,
			uh.precio,
			uu.nombre
		FROM usuario.sedehab uh
			JOIN usuario.sede uu ON uu.id_sede = uh.id_sede
		WHERE uh.id_habitacion = _doc_identidad;
	END;
$$;


ALTER FUNCTION usuario.f_leer_habitacion_sede_consulta(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_leer_habitacion_sede_disponibles(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_habitacion_sede_disponibles(_doc_identidad bigint) RETURNS SETOF usuario.v_habitacion_sede
    LANGUAGE plpgsql
    AS $$ 
	DECLARE 
	_sede int;
	BEGIN 
		_sede := (SELECT id_sede FROM usuario.usuario WHERE doc_identidad =_doc_identidad);
		RETURN QUERY 
		SELECT 
			uh.id_sede_hab,
			uh.tipo_hab,
			uh.cant_per,
			uh.disponibilidad,
			uh.precio,
			uu.nombre
		FROM usuario.sedehab uh
			JOIN usuario.sede uu ON uu.id_sede = uh.id_sede
		WHERE uh.id_sede = _sede AND uh.disponibilidad = true;
	END;
$$;


ALTER FUNCTION usuario.f_leer_habitacion_sede_disponibles(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_leer_reserva(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_reserva() RETURNS SETOF usuario.reserva_habitacion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			ur.*
		FROM 
			usuario.reserva_habitacion ur;
	END;
$$;


ALTER FUNCTION usuario.f_leer_reserva() OWNER TO postgres;

--
-- Name: f_leer_reserva_atra(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_reserva_atra() RETURNS SETOF usuario.reserva_atraccion
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			ur.*
		FROM 
			usuario.reserva_atraccion ur;
	END;
$$;


ALTER FUNCTION usuario.f_leer_reserva_atra() OWNER TO postgres;

--
-- Name: v_reserva_atraccion; Type: VIEW; Schema: usuario; Owner: postgres
--

CREATE VIEW usuario.v_reserva_atraccion AS
 SELECT 0 AS id_reserva,
    '2019-01-01'::date AS fecha_ent,
    '2019-01-01'::date AS fecha_sal,
    ''::text AS estado,
    ''::text AS sede_atra,
    (0)::bigint AS doc_identidad,
    ''::text AS sede;


ALTER TABLE usuario.v_reserva_atraccion OWNER TO postgres;

--
-- Name: f_leer_reserva_atraccion(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_reserva_atraccion() RETURNS SETOF usuario.v_reserva_atraccion
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			ur.id_reserva,
			ur.fecha_ent,
			ur.fecha_sal,
			ue.descripcion,
			us.nombre,
			uu.doc_identidad,
			uq.nombre
		FROM usuario.reserva_atraccion ur
			JOIN usuario.estado ue ON ur.id_estado = ue.id_estado
			JOIN usuario.sedeatra us ON ur.id_sede_atra = us.id_sede_atra
			JOIN usuario.usuario uu ON ur.doc_identidad = uu.doc_identidad
			JOIN usuario.sede uq ON ur.id_sede = uq.id_sede;
	END;
$$;


ALTER FUNCTION usuario.f_leer_reserva_atraccion() OWNER TO postgres;

--
-- Name: f_leer_reserva_habitacion(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_reserva_habitacion() RETURNS SETOF usuario.v_reserva_atraccion
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			ur.id_reserva,
			ur.fecha_ent,
			ur.fecha_sal,
			ue.descripcion,
			us.tipo_hab,
			uu.doc_identidad,
			uq.nombre
		FROM usuario.reserva_habitacion ur
			JOIN usuario.estado ue ON ur.id_estado = ue.id_estado
			JOIN usuario.sedehab us ON ur.id_sede_hab = us.id_sede_hab
			JOIN usuario.usuario uu ON ur.doc_identidad = uu.doc_identidad
			JOIN usuario.sede uq ON ur.id_sede = uq.id_sede;
	END;
$$;


ALTER FUNCTION usuario.f_leer_reserva_habitacion() OWNER TO postgres;

--
-- Name: v_sede; Type: VIEW; Schema: usuario; Owner: postgres
--

CREATE VIEW usuario.v_sede AS
 SELECT 0 AS id_sede,
    ''::text AS nombre,
    ''::text AS direccion,
    (0)::bigint AS telefono,
    (0)::bigint AS id_admi,
    ''::text AS nombre_admi;


ALTER TABLE usuario.v_sede OWNER TO postgres;

--
-- Name: f_leer_se(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_se() RETURNS SETOF usuario.v_sede
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			us.id_sede,
			us.nombre,
			us.direccion,
			us.telefono,
			uu.doc_identidad,
			uu.nombre
		FROM usuario.sede us		
			JOIN usuario.usuario uu ON uu.doc_identidad = us.id_admi
		WHERE uu.id_rol = 2;
	END;
$$;


ALTER FUNCTION usuario.f_leer_se() OWNER TO postgres;

--
-- Name: f_leer_sede(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_sede(_doc_identidad bigint) RETURNS SETOF usuario.sede
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			us.*
		FROM 
			usuario.sede us;
	END;
$$;


ALTER FUNCTION usuario.f_leer_sede(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: v_usuario; Type: VIEW; Schema: usuario; Owner: postgres
--

CREATE VIEW usuario.v_usuario AS
 SELECT (0)::bigint AS doc_identidad,
    ''::text AS nombre,
    ''::text AS apellido,
    (0)::bigint AS num_celular,
    ''::text AS cor_electronico,
    0 AS id_rol,
    ''::text AS rol_nombre,
    0 AS id_sede,
    ''::text AS sede_nombre;


ALTER TABLE usuario.v_usuario OWNER TO postgres;

--
-- Name: f_leer_usuario_admi(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_usuario_admi() RETURNS SETOF usuario.v_usuario
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			uu.doc_identidad,
			uu.nombre,
			uu.apellido,
			uu.num_cel,
			uu.correo_elec,
			uu.id_rol,
			ur.nombre,
			uu.id_sede,
			us.nombre
		FROM usuario.usuario uu 
			JOIN usuario.rol ur ON ur.id_rol = uu.id_rol
			JOIN usuario.sede us ON us.id_sede = uu.id_sede
		WHERE uu.id_rol = 2;
	END;
$$;


ALTER FUNCTION usuario.f_leer_usuario_admi() OWNER TO postgres;

--
-- Name: f_leer_usuario_emple(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leer_usuario_emple() RETURNS SETOF usuario.v_usuario
    LANGUAGE plpgsql
    AS $$ 
	BEGIN 
		RETURN QUERY 
		SELECT 
			uu.doc_identidad,
			uu.nombre,
			uu.apellido,
			uu.num_cel,
			uu.correo_elec,
			uu.id_rol,
			ur.nombre,
			uu.id_sede,
			us.nombre
		FROM usuario.usuario uu 
			JOIN usuario.rol ur ON ur.id_rol = uu.id_rol
			JOIN usuario.sede us ON us.id_sede = uu.id_sede
		WHERE uu.id_rol = 3;
	END;
$$;


ALTER FUNCTION usuario.f_leer_usuario_emple() OWNER TO postgres;

--
-- Name: f_leerr(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leerr() RETURNS SETOF usuario.rol
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			*
		FROM usuario.rol;
	END;
$$;


ALTER FUNCTION usuario.f_leerr() OWNER TO postgres;

--
-- Name: f_leertu(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leertu() RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu;
	END;
$$;


ALTER FUNCTION usuario.f_leertu() OWNER TO postgres;

--
-- Name: f_leeru(bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leeru(_doc_identidad bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
	RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu 
		WHERE doc_identidad = _doc_identidad;
	END
$$;


ALTER FUNCTION usuario.f_leeru(_doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_leerua(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leerua() RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu 
		WHERE id_rol = 2;
	END;
$$;


ALTER FUNCTION usuario.f_leerua() OWNER TO postgres;

--
-- Name: f_leerue(); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_leerue() RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu 
		WHERE id_rol = 3;
	END;
$$;


ALTER FUNCTION usuario.f_leerue() OWNER TO postgres;

--
-- Name: f_login(bigint, text); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_login(_doc bigint, _contra text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		RETURN QUERY
		SELECT 
			uu.*
		FROM 
			usuario.usuario uu JOIN usuario.rol ur ON ur.id_rol = uu.id_rol
		WHERE 
			uu.doc_identidad = _doc
		AND
			uu.contrasena = _contra;
	END;
$$;


ALTER FUNCTION usuario.f_login(_doc bigint, _contra text) OWNER TO postgres;

--
-- Name: f_modificar_atraccion(integer, text, integer, text, integer, time without time zone, time without time zone, boolean); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_modificar_atraccion(_id_atraccion integer, _nombre text, _precio_per integer, _descripcion text, _cant_per integer, _hora_ini time without time zone, _hora_fin time without time zone, _disponibilidad boolean) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		IF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) = (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) <= 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					UPDATE usuario.atraccion
					SET 
						nombre = _nombre,
						precio_per = _precio_per,
						descripcion = _descripcion,
						cant_per = _cant_per,
						hora_inicio = _hora_ini,
						hora_fin = _hora_fin,
						disponibilidad = _disponibilidad
					WHERE 
						id_atraccion = _id_atraccion;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		ELSEIF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) > (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) <= 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					UPDATE usuario.atraccion
					SET 
						nombre = _nombre,
						precio_per = _precio_per,
						descripcion = _descripcion,
						cant_per = _cant_per,
						hora_inicio = _hora_ini,
						hora_fin = _hora_fin,
						disponibilidad = _disponibilidad
					WHERE 
						id_atraccion = _id_atraccion;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		ELSEIF (SELECT EXTRACT ('MINUTE' FROM _hora_ini)) < (SELECT EXTRACT ('MINUTE' FROM _hora_fin))
		THEN 
			IF (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) < 10) AND (((SELECT EXTRACT ('HOUR' FROM _hora_fin))-(SELECT EXTRACT ('HOUR' FROM _hora_ini))) > 0)
				THEN 
					UPDATE usuario.atraccion
					SET 
						nombre = _nombre,
						precio_per = _precio_per,
						descripcion = _descripcion,
						cant_per = _cant_per,
						hora_inicio = _hora_ini,
						hora_fin = _hora_fin,
						disponibilidad = _disponibilidad
					WHERE 
						id_atraccion = _id_atraccion;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
			END IF;
		END IF;
	END
$$;


ALTER FUNCTION usuario.f_modificar_atraccion(_id_atraccion integer, _nombre text, _precio_per integer, _descripcion text, _cant_per integer, _hora_ini time without time zone, _hora_fin time without time zone, _disponibilidad boolean) OWNER TO postgres;

--
-- Name: f_modificar_atraccion_sede(integer, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_modificar_atraccion_sede(_id_atra integer, _precio integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		UPDATE usuario.sedeatra
			SET 
				precio = _precio
			WHERE 
				id_sede_atra = _id_atra;
	END
$$;


ALTER FUNCTION usuario.f_modificar_atraccion_sede(_id_atra integer, _precio integer) OWNER TO postgres;

--
-- Name: f_modificar_factura_atra(integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_modificar_factura_atra(_id_factura integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		UPDATE usuario.factura_atraccion
			SET				
				fecha_ent = (select now()::date),
				estado = 2
			WHERE 
				id_factura = _id_factura;
	END
$$;


ALTER FUNCTION usuario.f_modificar_factura_atra(_id_factura integer) OWNER TO postgres;

--
-- Name: f_modificar_habitacion(integer, text, integer, integer, boolean); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_modificar_habitacion(_id_habitacion integer, _tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		UPDATE usuario.habitacion
			SET 
				tipo_habitacion = _tipo_hab,
				cant_per = _cant_per,
				precio_hab = _precio_hab,
				disponibilidad = _disponibilidad
			WHERE 
				id_habitacion = _id_habitacion;
	END
$$;


ALTER FUNCTION usuario.f_modificar_habitacion(_id_habitacion integer, _tipo_hab text, _cant_per integer, _precio_hab integer, _disponibilidad boolean) OWNER TO postgres;

--
-- Name: f_modificar_habitacion_sede(integer, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_modificar_habitacion_sede(_id_hab integer, _precio integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	BEGIN
		UPDATE usuario.sedehab
			SET 
				precio = _precio
			WHERE 
				id_sede_hab = _id_hab;
	END
$$;


ALTER FUNCTION usuario.f_modificar_habitacion_sede(_id_hab integer, _precio integer) OWNER TO postgres;

--
-- Name: f_modificar_sede(integer, text, text, bigint, bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_modificar_sede(_id_sede integer, _nombre text, _direccion text, _telefono bigint, _id_admi bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	BEGIN
		UPDATE usuario.sede
			SET				
				nombre = _nombre,
				direccion = _direccion,
				telefono = _telefono,
				id_admi = _id_admi
			WHERE 
				id_sede = _id_sede;
	END
$$;


ALTER FUNCTION usuario.f_modificar_sede(_id_sede integer, _nombre text, _direccion text, _telefono bigint, _id_admi bigint) OWNER TO postgres;

--
-- Name: f_modificar_usuario_admi(bigint, text, text, bigint, text, integer, integer); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_modificar_usuario_admi(_doc_identidad bigint, _nombre text, _apellido text, _num_cel bigint, _correo_elec text, _id_rol integer, _id_sede integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad integer;
	_doc bigint;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		_doc := (SELECT doc_identidad FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		IF _cantidad = 0 OR NOT _doc <> _doc_identidad
			THEN 
				UPDATE usuario.usuario
				SET 
					doc_identidad = _doc_identidad, 
					nombre = _nombre, 
					apellido = _apellido, 
					num_cel = _num_cel, 
					correo_elec = _correo_elec, 
					id_rol = _id_rol, 
					id_sede = _id_sede
				WHERE doc_identidad = _doc_identidad;
				RETURN QUERY SELECT true;
			ELSE
				RETURN QUERY SELECT false;
		END IF;
	END
$$;


ALTER FUNCTION usuario.f_modificar_usuario_admi(_doc_identidad bigint, _nombre text, _apellido text, _num_cel bigint, _correo_elec text, _id_rol integer, _id_sede integer) OWNER TO postgres;

--
-- Name: f_modificaru(bigint, text, text, integer, bigint, text, text, text); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_modificaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _session text) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_cantidad integer;
	_doc bigint;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		_doc := (SELECT doc_identidad FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		IF _cantidad = 0 OR NOT _doc <> _doc_identidad
			THEN 
				UPDATE usuario.usuario
				SET doc_identidad = _doc_identidad, nombre = _nombre, apellido = _apellido, edad = _edad, num_Cel = _num_cel, correo_Elec = _correo_elec,
				contrasena = _contrasena, session = _session
				WHERE doc_identidad = _doc_identidad;
				RETURN QUERY SELECT true;
			ELSE
				RETURN QUERY SELECT false;
		END IF;
	END
$$;


ALTER FUNCTION usuario.f_modificaru(_doc_identidad bigint, _nombre text, _apellido text, _edad integer, _num_cel bigint, _correo_elec text, _contrasena text, _session text) OWNER TO postgres;

--
-- Name: f_reservar(date, date, integer, integer, bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_reservar(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_hab integer, _doc_identidad bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_sede integer;
	_cantidad integer;
	_concu bool;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		_sede := (SELECT id_sede FROM usuario.sedehab WHERE id_sede_hab = _id_sede_hab);
		_concu := (SELECT disponibilidad FROM usuario.sedehab WHERE id_sede_hab = _id_sede_hab);
		IF (_cantidad = 1) AND (_concu = true)
			THEN 
				INSERT INTO usuario.reserva_habitacion
				(
					fecha_ent,
					fecha_sal,
					id_estado,
					id_sede_hab,
					doc_identidad,
					id_sede
				)
				VALUES 
				(
					_fecha_ent,
					_fecha_sal,
					_id_estado,
					_id_sede_hab,
					_doc_identidad,
					_sede
				);

				UPDATE usuario.sedehab
					SET 
						disponibilidad = false
					WHERE 
						id_sede_hab = _id_sede_hab;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
		END IF;
	END
$$;


ALTER FUNCTION usuario.f_reservar(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_hab integer, _doc_identidad bigint) OWNER TO postgres;

--
-- Name: f_reservar_atra(date, date, integer, integer, bigint); Type: FUNCTION; Schema: usuario; Owner: postgres
--

CREATE FUNCTION usuario.f_reservar_atra(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_atra integer, _doc_identidad bigint) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE 
	_sede integer;
	_cantidad integer;
	_concu bool;
	BEGIN
		_cantidad := (SELECT COUNT (*) FROM usuario.usuario WHERE doc_identidad = _doc_identidad);
		_sede := (SELECT id_sede FROM usuario.sedeatra WHERE id_sede_atra = _id_sede_atra);
		_concu := (SELECT disponibilidad FROM usuario.sedeatra WHERE id_sede_atra = _id_sede_atra);
		IF (_cantidad = 1) AND (_concu = true)
			THEN 
				INSERT INTO usuario.reserva_atraccion
				(
					fecha_ent,
					fecha_sal,
					id_estado,
					id_sede_atra,
					doc_identidad,
					id_sede
				)
				VALUES 
				(
					_fecha_ent,
					_fecha_sal,
					_id_estado,
					_id_sede_atra,
					_doc_identidad,
					_sede
				);

				UPDATE usuario.sedeatra
					SET 
						disponibilidad = false
					WHERE 
						id_sede_atra = _id_sede_atra;
				RETURN QUERY SELECT true;
			ELSE 
				RETURN QUERY SELECT false;
		END IF;
	END
$$;


ALTER FUNCTION usuario.f_reservar_atra(_fecha_ent date, _fecha_sal date, _id_estado integer, _id_sede_atra integer, _doc_identidad bigint) OWNER TO postgres;

--
-- Name: auditoria; Type: TABLE; Schema: security; Owner: postgres
--

CREATE TABLE security.auditoria (
    id bigint NOT NULL,
    fecha timestamp(4) without time zone,
    accion character varying(100),
    schema character varying(200),
    tabla character varying(200),
    session text,
    user_bd character varying(100),
    data jsonb,
    pk text
);


ALTER TABLE security.auditoria OWNER TO postgres;

--
-- Name: TABLE auditoria; Type: COMMENT; Schema: security; Owner: postgres
--

COMMENT ON TABLE security.auditoria IS 'Tabla para almacenar los datos de la auditoria del proyecto';


--
-- Name: auditoria_id_seq; Type: SEQUENCE; Schema: security; Owner: postgres
--

CREATE SEQUENCE security.auditoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE security.auditoria_id_seq OWNER TO postgres;

--
-- Name: auditoria_id_seq; Type: SEQUENCE OWNED BY; Schema: security; Owner: postgres
--

ALTER SEQUENCE security.auditoria_id_seq OWNED BY security.auditoria.id;


--
-- Name: autenticacion_id_seq; Type: SEQUENCE; Schema: security; Owner: postgres
--

CREATE SEQUENCE security.autenticacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE security.autenticacion_id_seq OWNER TO postgres;

--
-- Name: autenticacion_id_seq; Type: SEQUENCE OWNED BY; Schema: security; Owner: postgres
--

ALTER SEQUENCE security.autenticacion_id_seq OWNED BY security.autenticacion.id;


--
-- Name: function_db_view; Type: VIEW; Schema: security; Owner: postgres
--

CREATE VIEW security.function_db_view AS
 SELECT pp.proname AS b_function,
    oidvectortypes(pp.proargtypes) AS b_type_parameters
   FROM (pg_proc pp
     JOIN pg_namespace pn ON ((pn.oid = pp.pronamespace)))
  WHERE ((pn.nspname)::text <> ALL (ARRAY[('pg_catalog'::character varying)::text, ('information_schema'::character varying)::text, ('admin_control'::character varying)::text, ('vial'::character varying)::text]));


ALTER TABLE security.function_db_view OWNER TO postgres;

--
-- Name: atraccion_id_atraccion_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.atraccion_id_atraccion_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.atraccion_id_atraccion_seq OWNER TO postgres;

--
-- Name: atraccion_id_atraccion_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.atraccion_id_atraccion_seq OWNED BY usuario.atraccion.id_atraccion;


--
-- Name: estado_id_Estado_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario."estado_id_Estado_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario."estado_id_Estado_seq" OWNER TO postgres;

--
-- Name: estado_id_Estado_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario."estado_id_Estado_seq" OWNED BY usuario.estado.id_estado;


--
-- Name: factura_atraccion_id_factura_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.factura_atraccion_id_factura_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.factura_atraccion_id_factura_seq OWNER TO postgres;

--
-- Name: factura_atraccion_id_factura_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.factura_atraccion_id_factura_seq OWNED BY usuario.factura_atraccion.id_factura;


--
-- Name: factura_id_factura_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.factura_id_factura_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.factura_id_factura_seq OWNER TO postgres;

--
-- Name: factura_id_factura_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.factura_id_factura_seq OWNED BY usuario.factura_habitacion.id_factura;


--
-- Name: habitacion_id_habitacion_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.habitacion_id_habitacion_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.habitacion_id_habitacion_seq OWNER TO postgres;

--
-- Name: habitacion_id_habitacion_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.habitacion_id_habitacion_seq OWNED BY usuario.habitacion.id_habitacion;


--
-- Name: reserva_atraccion_id_reserva_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.reserva_atraccion_id_reserva_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.reserva_atraccion_id_reserva_seq OWNER TO postgres;

--
-- Name: reserva_atraccion_id_reserva_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.reserva_atraccion_id_reserva_seq OWNED BY usuario.reserva_atraccion.id_reserva;


--
-- Name: reserva_id_reserva_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.reserva_id_reserva_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.reserva_id_reserva_seq OWNER TO postgres;

--
-- Name: reserva_id_reserva_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.reserva_id_reserva_seq OWNED BY usuario.reserva_habitacion.id_reserva;


--
-- Name: sede_id_sede_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.sede_id_sede_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.sede_id_sede_seq OWNER TO postgres;

--
-- Name: sede_id_sede_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.sede_id_sede_seq OWNED BY usuario.sede.id_sede;


--
-- Name: sedeatra_id_sede_atra_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.sedeatra_id_sede_atra_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.sedeatra_id_sede_atra_seq OWNER TO postgres;

--
-- Name: sedeatra_id_sede_atra_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.sedeatra_id_sede_atra_seq OWNED BY usuario.sedeatra.id_sede_atra;


--
-- Name: sedehab_id_sede_hab_seq; Type: SEQUENCE; Schema: usuario; Owner: postgres
--

CREATE SEQUENCE usuario.sedehab_id_sede_hab_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario.sedehab_id_sede_hab_seq OWNER TO postgres;

--
-- Name: sedehab_id_sede_hab_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: postgres
--

ALTER SEQUENCE usuario.sedehab_id_sede_hab_seq OWNED BY usuario.sedehab.id_sede_hab;


--
-- Name: auditoria id; Type: DEFAULT; Schema: security; Owner: postgres
--

ALTER TABLE ONLY security.auditoria ALTER COLUMN id SET DEFAULT nextval('security.auditoria_id_seq'::regclass);


--
-- Name: autenticacion id; Type: DEFAULT; Schema: security; Owner: postgres
--

ALTER TABLE ONLY security.autenticacion ALTER COLUMN id SET DEFAULT nextval('security.autenticacion_id_seq'::regclass);


--
-- Name: atraccion id_atraccion; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.atraccion ALTER COLUMN id_atraccion SET DEFAULT nextval('usuario.atraccion_id_atraccion_seq'::regclass);


--
-- Name: estado id_estado; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.estado ALTER COLUMN id_estado SET DEFAULT nextval('usuario."estado_id_Estado_seq"'::regclass);


--
-- Name: factura_atraccion id_factura; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.factura_atraccion ALTER COLUMN id_factura SET DEFAULT nextval('usuario.factura_atraccion_id_factura_seq'::regclass);


--
-- Name: factura_habitacion id_factura; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.factura_habitacion ALTER COLUMN id_factura SET DEFAULT nextval('usuario.factura_id_factura_seq'::regclass);


--
-- Name: habitacion id_habitacion; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.habitacion ALTER COLUMN id_habitacion SET DEFAULT nextval('usuario.habitacion_id_habitacion_seq'::regclass);


--
-- Name: reserva_atraccion id_reserva; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.reserva_atraccion ALTER COLUMN id_reserva SET DEFAULT nextval('usuario.reserva_atraccion_id_reserva_seq'::regclass);


--
-- Name: reserva_habitacion id_reserva; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.reserva_habitacion ALTER COLUMN id_reserva SET DEFAULT nextval('usuario.reserva_id_reserva_seq'::regclass);


--
-- Name: sede id_sede; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sede ALTER COLUMN id_sede SET DEFAULT nextval('usuario.sede_id_sede_seq'::regclass);


--
-- Name: sedeatra id_sede_atra; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sedeatra ALTER COLUMN id_sede_atra SET DEFAULT nextval('usuario.sedeatra_id_sede_atra_seq'::regclass);


--
-- Name: sedehab id_sede_hab; Type: DEFAULT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sedehab ALTER COLUMN id_sede_hab SET DEFAULT nextval('usuario.sedehab_id_sede_hab_seq'::regclass);


--
-- Data for Name: auditoria; Type: TABLE DATA; Schema: security; Owner: postgres
--

COPY security.auditoria (id, fecha, accion, schema, tabla, session, user_bd, data, pk) FROM stdin;
\.
COPY security.auditoria (id, fecha, accion, schema, tabla, session, user_bd, data, pk) FROM '$$PATH$$/3076.dat';

--
-- Data for Name: autenticacion; Type: TABLE DATA; Schema: security; Owner: postgres
--

COPY security.autenticacion (id, doc_identidad, ip, mac, fecha_ini, sesion, fecha_fin) FROM stdin;
\.
COPY security.autenticacion (id, doc_identidad, ip, mac, fecha_ini, sesion, fecha_fin) FROM '$$PATH$$/3092.dat';

--
-- Data for Name: atraccion; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.atraccion (id_atraccion, nombre, precio_per, descripcion, cant_per, disponibilidad, hora_inicio, hora_fin) FROM stdin;
\.
COPY usuario.atraccion (id_atraccion, nombre, precio_per, descripcion, cant_per, disponibilidad, hora_inicio, hora_fin) FROM '$$PATH$$/3078.dat';

--
-- Data for Name: estado; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.estado (id_estado, descripcion) FROM stdin;
\.
COPY usuario.estado (id_estado, descripcion) FROM '$$PATH$$/3074.dat';

--
-- Data for Name: estado_fac; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.estado_fac (id, descripcion) FROM stdin;
\.
COPY usuario.estado_fac (id, descripcion) FROM '$$PATH$$/3097.dat';

--
-- Data for Name: factura_atraccion; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.factura_atraccion (id_factura, total_pag, id_sedeatr, doc_identidad, fecha_ent, id_sede, estado) FROM stdin;
\.
COPY usuario.factura_atraccion (id_factura, total_pag, id_sedeatr, doc_identidad, fecha_ent, id_sede, estado) FROM '$$PATH$$/3096.dat';

--
-- Data for Name: factura_habitacion; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.factura_habitacion (id_factura, total_pag, id_sedehab, doc_identidad, fecha_ent, id_sede, estado) FROM stdin;
\.
COPY usuario.factura_habitacion (id_factura, total_pag, id_sedehab, doc_identidad, fecha_ent, id_sede, estado) FROM '$$PATH$$/3080.dat';

--
-- Data for Name: habitacion; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.habitacion (id_habitacion, tipo_habitacion, cant_per, precio_hab, disponibilidad) FROM stdin;
\.
COPY usuario.habitacion (id_habitacion, tipo_habitacion, cant_per, precio_hab, disponibilidad) FROM '$$PATH$$/3082.dat';

--
-- Data for Name: reserva_atraccion; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.reserva_atraccion (id_reserva, fecha_ent, fecha_sal, id_estado, id_sede_atra, doc_identidad, id_sede) FROM stdin;
\.
COPY usuario.reserva_atraccion (id_reserva, fecha_ent, fecha_sal, id_estado, id_sede_atra, doc_identidad, id_sede) FROM '$$PATH$$/3094.dat';

--
-- Data for Name: reserva_habitacion; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.reserva_habitacion (id_reserva, fecha_ent, fecha_sal, id_estado, id_sede_hab, doc_identidad, id_sede) FROM stdin;
\.
COPY usuario.reserva_habitacion (id_reserva, fecha_ent, fecha_sal, id_estado, id_sede_hab, doc_identidad, id_sede) FROM '$$PATH$$/3084.dat';

--
-- Data for Name: rol; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.rol (id_rol, nombre) FROM stdin;
\.
COPY usuario.rol (id_rol, nombre) FROM '$$PATH$$/3072.dat';

--
-- Data for Name: sede; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.sede (id_sede, nombre, direccion, telefono, id_admi) FROM stdin;
\.
COPY usuario.sede (id_sede, nombre, direccion, telefono, id_admi) FROM '$$PATH$$/3086.dat';

--
-- Data for Name: sedeatra; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.sedeatra (id_sede_atra, descripcion, cant_per, hora_ini, hora_fin, disponibilidad, id_atraccion, id_sede, nombre, precio) FROM stdin;
\.
COPY usuario.sedeatra (id_sede_atra, descripcion, cant_per, hora_ini, hora_fin, disponibilidad, id_atraccion, id_sede, nombre, precio) FROM '$$PATH$$/3088.dat';

--
-- Data for Name: sedehab; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.sedehab (id_sede_hab, tipo_hab, cant_per, disponibilidad, id_habitacion, id_sede, precio) FROM stdin;
\.
COPY usuario.sedehab (id_sede_hab, tipo_hab, cant_per, disponibilidad, id_habitacion, id_sede, precio) FROM '$$PATH$$/3090.dat';

--
-- Data for Name: usuario; Type: TABLE DATA; Schema: usuario; Owner: postgres
--

COPY usuario.usuario (doc_identidad, nombre, apellido, edad, num_cel, correo_elec, contrasena, id_rol, session, id_sede) FROM stdin;
\.
COPY usuario.usuario (doc_identidad, nombre, apellido, edad, num_cel, correo_elec, contrasena, id_rol, session, id_sede) FROM '$$PATH$$/3071.dat';

--
-- Name: auditoria_id_seq; Type: SEQUENCE SET; Schema: security; Owner: postgres
--

SELECT pg_catalog.setval('security.auditoria_id_seq', 575, true);


--
-- Name: autenticacion_id_seq; Type: SEQUENCE SET; Schema: security; Owner: postgres
--

SELECT pg_catalog.setval('security.autenticacion_id_seq', 13, true);


--
-- Name: atraccion_id_atraccion_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.atraccion_id_atraccion_seq', 53, true);


--
-- Name: estado_id_Estado_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario."estado_id_Estado_seq"', 1, false);


--
-- Name: factura_atraccion_id_factura_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.factura_atraccion_id_factura_seq', 4, true);


--
-- Name: factura_id_factura_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.factura_id_factura_seq', 20, true);


--
-- Name: habitacion_id_habitacion_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.habitacion_id_habitacion_seq', 2, true);


--
-- Name: reserva_atraccion_id_reserva_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.reserva_atraccion_id_reserva_seq', 2, true);


--
-- Name: reserva_id_reserva_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.reserva_id_reserva_seq', 17, true);


--
-- Name: sede_id_sede_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.sede_id_sede_seq', 5, true);


--
-- Name: sedeatra_id_sede_atra_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.sedeatra_id_sede_atra_seq', 8, true);


--
-- Name: sedehab_id_sede_hab_seq; Type: SEQUENCE SET; Schema: usuario; Owner: postgres
--

SELECT pg_catalog.setval('usuario.sedehab_id_sede_hab_seq', 3, true);


--
-- Name: auditoria auditoria_pkey; Type: CONSTRAINT; Schema: security; Owner: postgres
--

ALTER TABLE ONLY security.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);


--
-- Name: autenticacion autenticacion_pkey; Type: CONSTRAINT; Schema: security; Owner: postgres
--

ALTER TABLE ONLY security.autenticacion
    ADD CONSTRAINT autenticacion_pkey PRIMARY KEY (id);


--
-- Name: atraccion atraccion_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.atraccion
    ADD CONSTRAINT atraccion_pkey PRIMARY KEY (id_atraccion);


--
-- Name: estado_fac estado_fac_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.estado_fac
    ADD CONSTRAINT estado_fac_pkey PRIMARY KEY (id);


--
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id_estado);


--
-- Name: factura_atraccion factura_atraccion_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.factura_atraccion
    ADD CONSTRAINT factura_atraccion_pkey PRIMARY KEY (id_factura);


--
-- Name: factura_habitacion factura_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.factura_habitacion
    ADD CONSTRAINT factura_pkey PRIMARY KEY (id_factura);


--
-- Name: habitacion habitacion_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.habitacion
    ADD CONSTRAINT habitacion_pkey PRIMARY KEY (id_habitacion);


--
-- Name: reserva_atraccion reserva_atraccion_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.reserva_atraccion
    ADD CONSTRAINT reserva_atraccion_pkey PRIMARY KEY (id_reserva);


--
-- Name: reserva_habitacion reserva_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.reserva_habitacion
    ADD CONSTRAINT reserva_pkey PRIMARY KEY (id_reserva);


--
-- Name: rol rol_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id_rol);


--
-- Name: sede sede_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sede
    ADD CONSTRAINT sede_pkey PRIMARY KEY (id_sede);


--
-- Name: sedeatra sedeatra_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sedeatra
    ADD CONSTRAINT sedeatra_pkey PRIMARY KEY (id_sede_atra);


--
-- Name: sedehab sedehab_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sedehab
    ADD CONSTRAINT sedehab_pkey PRIMARY KEY (id_sede_hab);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (doc_identidad);


--
-- Name: fki_fk_factura_atra_estado_fac; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_factura_atra_estado_fac ON usuario.factura_atraccion USING btree (estado);


--
-- Name: fki_fk_factura_estado_fa; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_factura_estado_fa ON usuario.factura_habitacion USING btree (estado);


--
-- Name: fki_fk_factura_sedehab; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_factura_sedehab ON usuario.factura_habitacion USING btree (id_sedehab);


--
-- Name: fki_fk_factura_usuario; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_factura_usuario ON usuario.factura_habitacion USING btree (doc_identidad);


--
-- Name: fki_fk_reserva_estado; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_reserva_estado ON usuario.reserva_habitacion USING btree (id_estado);


--
-- Name: fki_fk_reserva_sedehab; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_reserva_sedehab ON usuario.reserva_habitacion USING btree (id_sede_hab);


--
-- Name: fki_fk_sede_usuario; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_sede_usuario ON usuario.sede USING btree (id_admi);


--
-- Name: fki_fk_sedeatra_atra; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_sedeatra_atra ON usuario.sedeatra USING btree (id_atraccion);


--
-- Name: fki_fk_sedeatra_sede; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_sedeatra_sede ON usuario.sedeatra USING btree (id_sede);


--
-- Name: fki_fk_sedehab_hab; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_sedehab_hab ON usuario.sedehab USING btree (id_habitacion);


--
-- Name: fki_fk_sedehab_sed; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_sedehab_sed ON usuario.sedehab USING btree (id_sede);


--
-- Name: fki_fk_usuario_rol; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_usuario_rol ON usuario.usuario USING btree (id_rol);


--
-- Name: fki_fk_usuario_sede; Type: INDEX; Schema: usuario; Owner: postgres
--

CREATE INDEX fki_fk_usuario_sede ON usuario.usuario USING btree (id_sede);


--
-- Name: atraccion tg_usuario_atraccion; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_atraccion AFTER INSERT OR DELETE OR UPDATE ON usuario.atraccion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: estado tg_usuario_estado; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_estado AFTER INSERT OR DELETE OR UPDATE ON usuario.estado FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: factura_habitacion tg_usuario_factura; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_factura AFTER INSERT OR DELETE OR UPDATE ON usuario.factura_habitacion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: habitacion tg_usuario_habitacion; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_habitacion AFTER INSERT OR DELETE OR UPDATE ON usuario.habitacion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: reserva_habitacion tg_usuario_reserva; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_reserva AFTER INSERT OR DELETE OR UPDATE ON usuario.reserva_habitacion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: rol tg_usuario_rol; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_rol AFTER INSERT OR DELETE OR UPDATE ON usuario.rol FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: sede tg_usuario_sede; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_sede AFTER INSERT OR DELETE OR UPDATE ON usuario.sede FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: sedeatra tg_usuario_sedeatra; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_sedeatra AFTER INSERT OR DELETE OR UPDATE ON usuario.sedeatra FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: sedehab tg_usuario_sedehab; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_sedehab AFTER INSERT OR DELETE OR UPDATE ON usuario.sedehab FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: usuario tg_usuario_usuario; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER tg_usuario_usuario AFTER INSERT OR DELETE OR UPDATE ON usuario.usuario FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: estado_fac trigger_usuario_estado_fac; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER trigger_usuario_estado_fac AFTER INSERT OR DELETE OR UPDATE ON usuario.estado_fac FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: factura_atraccion trigger_usuario_factura_atra; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER trigger_usuario_factura_atra AFTER INSERT OR DELETE OR UPDATE ON usuario.factura_atraccion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: factura_habitacion trigger_usuario_factura_hab; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER trigger_usuario_factura_hab AFTER INSERT OR DELETE OR UPDATE ON usuario.factura_habitacion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: reserva_atraccion trigger_usuario_reserva_hab; Type: TRIGGER; Schema: usuario; Owner: postgres
--

CREATE TRIGGER trigger_usuario_reserva_hab AFTER INSERT OR DELETE OR UPDATE ON usuario.reserva_atraccion FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();


--
-- Name: factura_atraccion fk_factura_atra_estado_fac; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.factura_atraccion
    ADD CONSTRAINT fk_factura_atra_estado_fac FOREIGN KEY (estado) REFERENCES usuario.estado_fac(id);


--
-- Name: factura_habitacion fk_factura_estado_fa; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.factura_habitacion
    ADD CONSTRAINT fk_factura_estado_fa FOREIGN KEY (estado) REFERENCES usuario.estado_fac(id);


--
-- Name: factura_habitacion fk_factura_sedehab; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.factura_habitacion
    ADD CONSTRAINT fk_factura_sedehab FOREIGN KEY (id_sedehab) REFERENCES usuario.sedehab(id_sede_hab);


--
-- Name: CONSTRAINT fk_factura_sedehab ON factura_habitacion; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_factura_sedehab ON usuario.factura_habitacion IS 'Llave foránea para establecer la relación con las habitaciones de la sede';


--
-- Name: factura_habitacion fk_factura_usuario; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.factura_habitacion
    ADD CONSTRAINT fk_factura_usuario FOREIGN KEY (doc_identidad) REFERENCES usuario.usuario(doc_identidad);


--
-- Name: CONSTRAINT fk_factura_usuario ON factura_habitacion; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_factura_usuario ON usuario.factura_habitacion IS 'Llave foránea para establecer la relación con el usuario';


--
-- Name: reserva_habitacion fk_reserva_estado; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.reserva_habitacion
    ADD CONSTRAINT fk_reserva_estado FOREIGN KEY (id_estado) REFERENCES usuario.estado(id_estado);


--
-- Name: CONSTRAINT fk_reserva_estado ON reserva_habitacion; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_reserva_estado ON usuario.reserva_habitacion IS 'Llave foránea para establecer la relación entre la reserva y el estado';


--
-- Name: reserva_habitacion fk_reserva_sedehab; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.reserva_habitacion
    ADD CONSTRAINT fk_reserva_sedehab FOREIGN KEY (id_sede_hab) REFERENCES usuario.sedehab(id_sede_hab);


--
-- Name: CONSTRAINT fk_reserva_sedehab ON reserva_habitacion; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_reserva_sedehab ON usuario.reserva_habitacion IS 'Llave foránea para establecer la relación entre la reserva y las habitaciones de la sede';


--
-- Name: sede fk_sede_usuario; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sede
    ADD CONSTRAINT fk_sede_usuario FOREIGN KEY (id_admi) REFERENCES usuario.usuario(doc_identidad);


--
-- Name: CONSTRAINT fk_sede_usuario ON sede; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_sede_usuario ON usuario.sede IS 'Llave foránea para establecer la relación entre la sede y el administrador';


--
-- Name: sedeatra fk_sedeatra_atra; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sedeatra
    ADD CONSTRAINT fk_sedeatra_atra FOREIGN KEY (id_atraccion) REFERENCES usuario.atraccion(id_atraccion);


--
-- Name: CONSTRAINT fk_sedeatra_atra ON sedeatra; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_sedeatra_atra ON usuario.sedeatra IS 'Llave foránea para establecer la relación entre las atracciones de la sede y las atracciones';


--
-- Name: sedeatra fk_sedeatra_sede; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sedeatra
    ADD CONSTRAINT fk_sedeatra_sede FOREIGN KEY (id_sede) REFERENCES usuario.sede(id_sede);


--
-- Name: CONSTRAINT fk_sedeatra_sede ON sedeatra; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_sedeatra_sede ON usuario.sedeatra IS 'Llave foránea para establecer la relación entre las atracciones de la sede y la sede';


--
-- Name: sedehab fk_sedehab_hab; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sedehab
    ADD CONSTRAINT fk_sedehab_hab FOREIGN KEY (id_habitacion) REFERENCES usuario.habitacion(id_habitacion);


--
-- Name: CONSTRAINT fk_sedehab_hab ON sedehab; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_sedehab_hab ON usuario.sedehab IS 'Llave foránea para establecer la relación entre las habitaciones de la sede y las habitaciones';


--
-- Name: sedehab fk_sedehab_sed; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.sedehab
    ADD CONSTRAINT fk_sedehab_sed FOREIGN KEY (id_sede) REFERENCES usuario.sede(id_sede);


--
-- Name: CONSTRAINT fk_sedehab_sed ON sedehab; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_sedehab_sed ON usuario.sedehab IS 'Llave foránea para establecer la relación entre las habitaciones de la sede y las sedes';


--
-- Name: usuario fk_usuario_rol; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT fk_usuario_rol FOREIGN KEY (id_rol) REFERENCES usuario.rol(id_rol);


--
-- Name: CONSTRAINT fk_usuario_rol ON usuario; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_usuario_rol ON usuario.usuario IS 'Llave foránea entre las tablas usuario y rol';


--
-- Name: usuario fk_usuario_sede; Type: FK CONSTRAINT; Schema: usuario; Owner: postgres
--

ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT fk_usuario_sede FOREIGN KEY (id_sede) REFERENCES usuario.sede(id_sede);


--
-- Name: CONSTRAINT fk_usuario_sede ON usuario; Type: COMMENT; Schema: usuario; Owner: postgres
--

COMMENT ON CONSTRAINT fk_usuario_sede ON usuario.usuario IS 'Llave foránea para establecer la relacione entre el usuario y la sede';


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              