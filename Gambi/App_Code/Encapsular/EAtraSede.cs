﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de EAtraSede
/// </summary>
public class EAtraSede
{
    private int _id_sede_atraccion;
    private string nombre;
    private string descripcion;
    private int can_per;
    private TimeSpan hora_ini;
    private TimeSpan hora_fin;
    private bool disponibilidad;
    private int precio;
    private int id_atraccion;
    private int id_sede;
    private string nombre_sede;

    public int Id_sede_atraccion { get => _id_sede_atraccion; set => _id_sede_atraccion = value; }
    public string Nombre { get => nombre; set => nombre = value; }
    public string Descripcion { get => descripcion; set => descripcion = value; }
    public int Can_per { get => can_per; set => can_per = value; }
    public TimeSpan Hora_ini { get => hora_ini; set => hora_ini = value; }
    public TimeSpan Hora_fin { get => hora_fin; set => hora_fin = value; }
    public bool Disponibilidad { get => disponibilidad; set => disponibilidad = value; }
    public int Precio { get => precio; set => precio = value; }
    public int Id_atraccion { get => id_atraccion; set => id_atraccion = value; }
    public int Id_sede { get => id_sede; set => id_sede = value; }
    public string Nombre_sede { get => nombre_sede; set => nombre_sede = value; }
}