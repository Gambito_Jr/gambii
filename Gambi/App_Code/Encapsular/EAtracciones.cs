﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de EAtracciones
/// </summary>
public class EAtracciones
{
    private int idAtra;
    private string nombre;
    private int precioP;
    private string descripcion;
    private int cantP;
    private bool disponibilidad;
    private TimeSpan horaIni;
    private TimeSpan horaFin;

    public int IdAtra { get => idAtra; set => idAtra = value; }
    public string Nombre { get => nombre; set => nombre = value; }
    public int PrecioP { get => precioP; set => precioP = value; }
    public string Descripcion { get => descripcion; set => descripcion = value; }
    public int CantP { get => cantP; set => cantP = value; }
    public bool Disponibilidad { get => disponibilidad; set => disponibilidad = value; }
    public TimeSpan HoraIni { get => horaIni; set => horaIni = value; }
    public TimeSpan HoraFin { get => horaFin; set => horaFin = value; }
}