﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

/// <summary>
/// Descripción breve de DAtracciones
/// </summary>
public class DAtracciones
{
    public bool insertarAtraccion(EAtracciones atra)
    {
        DataTable atraccion = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);
        Boolean res = false;

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_insertar_atraccion", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_nombre", NpgsqlDbType.Text).Value = atra.Nombre;
            dataAdapter.SelectCommand.Parameters.Add("_precio_per", NpgsqlDbType.Integer).Value = atra.PrecioP;
            dataAdapter.SelectCommand.Parameters.Add("_descripcion", NpgsqlDbType.Text).Value = atra.Descripcion;
            dataAdapter.SelectCommand.Parameters.Add("_cant_per", NpgsqlDbType.Integer).Value = atra.CantP;
            dataAdapter.SelectCommand.Parameters.Add("_disponibilidad", NpgsqlDbType.Boolean).Value = atra.Disponibilidad;
            dataAdapter.SelectCommand.Parameters.Add("_hora_ini", NpgsqlDbType.Time).Value = atra.HoraIni;
            dataAdapter.SelectCommand.Parameters.Add("_hora_fin", NpgsqlDbType.Time).Value = atra.HoraFin;

            conection.Open();
            dataAdapter.Fill(atraccion);
            res = Convert.ToBoolean(atraccion.Rows[0].ItemArray[0]);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return res;
    }

    public List<EAtracciones> obteneratra()
    {
        DataTable atracciones = new DataTable();
        List<EAtracciones> listaAtra = new List<EAtracciones>();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_leer_atraccion", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            conection.Open();
            dataAdapter.Fill(atracciones);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        listaAtra = atracciones.AsEnumerable().Select(m => new EAtracciones()
        {
            IdAtra = m.Field<int>("id_atraccion"),
            Nombre = m.Field<string>("nombre"),
            PrecioP = m.Field<int>("precio_per"),
            Descripcion = m.Field<string>("descripcion"),
            CantP = m.Field<int>("cant_per"),
            Disponibilidad = m.Field<Boolean>("disponibilidad"),
            HoraIni = m.Field<TimeSpan>("hora_inicio"),
            HoraFin = m.Field<TimeSpan>("hora_fin")
        }).ToList();

        return listaAtra;
    }

    public bool modificaratra(EAtracciones atra)
    {
        DataTable atraccion = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);
        Boolean res = false;

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_modificar_atraccion", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id_atraccion", NpgsqlDbType.Integer).Value = atra.IdAtra;
            dataAdapter.SelectCommand.Parameters.Add("_nombre", NpgsqlDbType.Text).Value = atra.Nombre;
            dataAdapter.SelectCommand.Parameters.Add("_precio_per", NpgsqlDbType.Integer).Value = atra.PrecioP;
            dataAdapter.SelectCommand.Parameters.Add("_descripcion", NpgsqlDbType.Text).Value = atra.Descripcion;
            dataAdapter.SelectCommand.Parameters.Add("_cant_per", NpgsqlDbType.Integer).Value = atra.CantP;
            dataAdapter.SelectCommand.Parameters.Add("_hora_ini", NpgsqlDbType.Time).Value = atra.HoraIni;
            dataAdapter.SelectCommand.Parameters.Add("_hora_fin", NpgsqlDbType.Time).Value = atra.HoraFin;
            dataAdapter.SelectCommand.Parameters.Add("_disponibilidad", NpgsqlDbType.Boolean).Value = atra.Disponibilidad;

            conection.Open();
            dataAdapter.Fill(atraccion);
            res = Convert.ToBoolean(atraccion.Rows[0].ItemArray[0]);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return res;
    }

    public DataTable eliminaratra(EAtracciones atra)
    {
        DataTable atraccion = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_eliminar_atraccion", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id_atraccion", NpgsqlDbType.Integer).Value = atra.IdAtra;

            conection.Open();
            dataAdapter.Fill(atraccion);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return atraccion;
    }

}