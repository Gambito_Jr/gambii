﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;


public class DAtraSede
{
    public DataTable insertarAtraccionSede(EAtraSede atra)
    {
        DataTable atraccion = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_insertar_atraccion_sede", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id_atraccion", NpgsqlDbType.Integer).Value = atra.Id_atraccion;
            dataAdapter.SelectCommand.Parameters.Add("_id_sede", NpgsqlDbType.Integer).Value = atra.Id_sede;
            dataAdapter.SelectCommand.Parameters.Add("_precio", NpgsqlDbType.Integer).Value = atra.Precio;

            conection.Open();
            dataAdapter.Fill(atraccion);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return atraccion;
    }

    

    public List<EAtraSede> obteneratrasede(long doc)
    {
        DataTable atracciones = new DataTable();
        List<EAtraSede> listaAtra = new List<EAtraSede>();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_leer_atraccion_sede", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_doc_identidad", NpgsqlDbType.Integer).Value = doc;

            conection.Open();
            dataAdapter.Fill(atracciones);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        listaAtra = atracciones.AsEnumerable().Select(m => new EAtraSede()
        {
            Id_sede_atraccion = m.Field<int>("id_sede_atraccion"),
            Nombre = m.Field<string>("nombre"),
            Descripcion = m.Field<string>("descripcion"),
            Can_per = m.Field<int>("cant_per"),
            Precio = m.Field<int>("precio"),
            Hora_ini = m.Field<TimeSpan>("hora_ini"),
            Hora_fin = m.Field<TimeSpan>("hora_fin"),
            Disponibilidad = m.Field<Boolean>("disponibilidad"),
            Nombre_sede = m.Field<string>("nombre_sede")
        }).ToList();

        return listaAtra;
    }

    
    public DataTable modificaratrasede(EAtraSede atra)
    {
        DataTable atraccion = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);
        

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_modificar_atraccion_sede", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id_atra", NpgsqlDbType.Integer).Value = atra.Id_sede_atraccion;
            dataAdapter.SelectCommand.Parameters.Add("_precio", NpgsqlDbType.Integer).Value = atra.Precio;

            conection.Open();
            dataAdapter.Fill(atraccion);

        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return atraccion;
    }

    public DataTable eliminaratrasede(EAtraSede atra)
    {
        DataTable atraccion = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_eliminar_atraccion_sede", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id_atraccion", NpgsqlDbType.Integer).Value = atra.Id_sede_atraccion;

            conection.Open();
            dataAdapter.Fill(atraccion);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return atraccion;
    }

    public List<EAtraSede> obteneratrasedeMPI(long atra)
    {
        DataTable atracciones = new DataTable();
        List<EAtraSede> listaAtra = new List<EAtraSede>();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_leer_atraccion_sede_consulta", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_doc_identidad", NpgsqlDbType.Integer).Value = atra;

            conection.Open();
            dataAdapter.Fill(atracciones);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        listaAtra = atracciones.AsEnumerable().Select(m => new EAtraSede()
        {
            Id_sede_atraccion = m.Field<int>("id_sede_atraccion"),
            Nombre = m.Field<string>("nombre"),
            Descripcion = m.Field<string>("descripcion"),
            Can_per = m.Field<int>("cant_per"),
            Precio = m.Field<int>("precio"),
            Hora_ini = m.Field<TimeSpan>("hora_ini"),
            Hora_fin = m.Field<TimeSpan>("hora_fin"),
            Disponibilidad = m.Field<Boolean>("disponibilidad"),
            Nombre_sede = m.Field<string>("nombre_sede")
        }).ToList();

        return listaAtra;
    }

    public List<EAtraSede> obteneratrasededis(long doc)
    {
        DataTable atracciones = new DataTable();
        List<EAtraSede> listaAtra = new List<EAtraSede>();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_leer_atraccion_sede", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_doc_identidad", NpgsqlDbType.Integer).Value = doc;

            conection.Open();
            dataAdapter.Fill(atracciones);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        listaAtra = atracciones.AsEnumerable().Select(m => new EAtraSede()
        {
            Id_sede_atraccion = m.Field<int>("id_sede_atraccion"),
            Nombre = m.Field<string>("nombre"),
            Descripcion = m.Field<string>("descripcion"),
            Can_per = m.Field<int>("cant_per"),
            Precio = m.Field<int>("precio"),
            Hora_ini = m.Field<TimeSpan>("hora_ini"),
            Hora_fin = m.Field<TimeSpan>("hora_fin"),
            Disponibilidad = m.Field<Boolean>("disponibilidad"),
            Nombre_sede = m.Field<string>("nombre_sede")
        }).Where(x => x.Disponibilidad == true).ToList();

        return listaAtra;
    }

    public List<EAtraSede> obteneratrasedenodis(long doc)
    {
        DataTable atracciones = new DataTable();
        List<EAtraSede> listaAtra = new List<EAtraSede>();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_leer_atraccion_sede", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_doc_identidad", NpgsqlDbType.Integer).Value = doc;

            conection.Open();
            dataAdapter.Fill(atracciones);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        listaAtra = atracciones.AsEnumerable().Select(m => new EAtraSede()
        {
            Id_sede_atraccion = m.Field<int>("id_sede_atraccion"),
            Nombre = m.Field<string>("nombre"),
            Descripcion = m.Field<string>("descripcion"),
            Can_per = m.Field<int>("cant_per"),
            Precio = m.Field<int>("precio"),
            Hora_ini = m.Field<TimeSpan>("hora_ini"),
            Hora_fin = m.Field<TimeSpan>("hora_fin"),
            Disponibilidad = m.Field<Boolean>("disponibilidad"),
            Nombre_sede = m.Field<string>("nombre_sede")
        }).Where(x => x.Disponibilidad == false).ToList();

        return listaAtra;
    }
}