﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MPA.master" AutoEventWireup="true" CodeFile="~/Controller/SuperAdmiAtra.aspx.cs" Inherits="Vista_SuperAdmiAtra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .auto-style15 {
        width: 100%;
    }
    .auto-style18 {
        text-align: center;
        height: 23px;
    }
        .auto-style19 {
            text-align: center;
            height: 16px;
        }
        .auto-style22 {
            width: 394px;
        }
        .auto-style23 {
            width: 394px;
            text-align: center;
        }
        .auto-style24 {
        width: 289px;
    }
        .auto-style25 {
        width: 289px;
        text-align: center;
    }
        .auto-style26 {
            width: 289px;
            text-align: center;
            height: 23px;
        }
        .auto-style27 {
            width: 394px;
            text-align: center;
            height: 23px;
        }
        .auto-style28 {
            height: 23px;
        width: 252px;
    }
    .auto-style29 {
        width: 252px;
    }
    .auto-style30 {
        width: 1102px;
        text-align: center;
        height: 91px;
        font-size: 15px;
        background-color: #FFFFFF;
    }
    
        .auto-style31 {
            width: 289px;
            height: 24px;
        }
        .auto-style32 {
            width: 394px;
            height: 24px;
        }
        .auto-style33 {
            width: 252px;
            height: 24px;
        }
        .auto-style34 {
            width: 289px;
            text-align: center;
            height: 60px;
        }
        .auto-style35 {
            width: 394px;
            text-align: center;
            height: 60px;
        }
        .auto-style36 {
            width: 252px;
            height: 60px;
        }
    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="auto-style30">
    <tr>
        <td class="auto-style22" colspan="3">
                &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style23" colspan="3">
                <asp:Label ID="L_S" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Atracciones del centro turistico Gambi"></asp:Label>
            </td>
    </tr>
    <tr>
        <td class="auto-style24">
            &nbsp;</td>
        <td class="auto-style22">
                &nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style24">
            &nbsp;</td>
        <td class="auto-style22">
                &nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style24">
            &nbsp;</td>
        <td class="auto-style22">
                <asp:GridView ID="GV_Atra" runat="server" AutoGenerateColumns="False" DataKeyNames="IdAtra" DataSourceID="ODS_Atra" OnRowUpdating="GV_Atra_RowUpdating" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" PageSize="5">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_EN" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_EN" runat="server" ControlToValidate="TB_EN" Display="Dynamic" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_EN" runat="server" ControlToValidate="TB_EN" Display="Dynamic" ErrorMessage="No debe contener caracteres númericos " Font-Size="Small" ForeColor="Red" ValidationExpression="[A-Za-z]*"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio" SortExpression="PrecioP">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_EPP" runat="server" Text='<%# Bind("PrecioP") %>' TextMode="Number"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_EPP" runat="server" ControlToValidate="TB_EPP" Display="Dynamic" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RangeValidator ID="R_PP" runat="server" ControlToValidate="TB_EPP" Font-Size="small" ForeColor="Red" MaximumValue="1500000" MinimumValue="100000" Text="Valor inválido (100000-1500000)" Type="Integer" />
                                <br />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("PrecioP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descripcion" SortExpression="Descripcion">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_ED" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_ED" runat="server" ControlToValidate="TB_ED" Display="Dynamic" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_ED" runat="server" ControlToValidate="TB_ED" Display="Dynamic" ErrorMessage="Limite de 100 caracteres superado" Font-Size="Small" ForeColor="Red" ValidationExpression="^[\s\S]{0,100}$"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cantidad de personas" SortExpression="CantP">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_ECP" runat="server" Text='<%# Bind("CantP") %>' TextMode="Number"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_ECP" runat="server" ControlToValidate="TB_ECP" Display="Dynamic" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_ECP" runat="server" ControlToValidate="TB_ECP" Display="Dynamic" ErrorMessage="Valor inválido" Font-Size="Small" ForeColor="Red" ValidationExpression="^([1-9][0-9]?)$"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("CantP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Hora inicio" SortExpression="HoraIni">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_EHI" runat="server" Text='<%# Bind("HoraIni") %>' TextMode="Time"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_EHI" runat="server" ControlToValidate="TB_EHI" Display="Dynamic" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("HoraIni") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Hora fin" SortExpression="HoraFin">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_EHF" runat="server" Text='<%# Bind("HoraFin") %>' TextMode="Time"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_EHF" runat="server" ControlToValidate="TB_EHF" Display="Dynamic" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("HoraFin") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Disponibilidad" SortExpression="Disponibilidad">
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Disponibilidad") %>' />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Disponibilidad") %>' Enabled="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                     <EmptyDataTemplate>
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="¡Actualmente no se encuentra ningún dato registrado, por favor realice el siguiente registro!"></asp:Label>
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
                <asp:ObjectDataSource ID="ODS_Atra" runat="server" DataObjectTypeName="EAtracciones" SelectMethod="obteneratra" TypeName="DAtracciones" UpdateMethod="modificaratra" DeleteMethod="eliminaratra"></asp:ObjectDataSource>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style31"></td>
        <td class="auto-style32">
                </td>
        <td class="auto-style33"></td>
    </tr>
    <tr>
        <td class="auto-style25">
            &nbsp;</td>
        <td class="auto-style23">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" Text="REGISTRO ATRACCIONES"></asp:Label>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style24">&nbsp;</td>
        <td class="auto-style22">
                &nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
            &nbsp;</td>
        <td class="auto-style23">
            <asp:Label ID="Label3" runat="server" Text="Nombre"></asp:Label>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
                <asp:TextBox ID="TB_NA" runat="server" BorderColor="#CCCCFF" BorderStyle="Groove" Height="26px"  Width="344px" TextMode="SingleLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_N" runat="server" ControlToValidate="TB_NA" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validarRegistro"></asp:RequiredFieldValidator>
                <br />
                <asp:RegularExpressionValidator ID="REV_N" runat="server" Font-Size="Small"  ControlToValidate="TB_NA" Display="Dynamic" ErrorMessage="No debe contener caracteres númericos " ForeColor="Red" ValidationExpression="[A-Za-z]*"></asp:RegularExpressionValidator>
                </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
            &nbsp;</td>
        <td class="auto-style23">
            <asp:Label ID="Label4" runat="server" Text="Precio"></asp:Label>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style34">
                </td>
        <td class="auto-style35">
                <asp:TextBox ID="TB_PA" runat="server" BorderColor="#CCCCFF" BorderStyle="Groove" Height="26px"  Width="344px" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_PP" runat="server" ControlToValidate="TB_PA" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validarRegistro"></asp:RequiredFieldValidator>
                <br />
                <asp:RangeValidator id="R_PP" runat="server" ControlToValidate="TB_PA" MinimumValue="100000" MaximumValue="1500000" Type="Integer" Text="Valor inválido (100000-1500000)" Font-Size="small" ForeColor="Red" />
                </td>
        <td class="auto-style36"></td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
            <asp:Label ID="Label5" runat="server" Text="Descripcion"></asp:Label>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
                <asp:TextBox ID="TB_D" runat="server" BorderColor="#CCCCFF" BorderStyle="Groove" Height="26px"  Width="344px" TextMode="SingleLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_D" runat="server" ControlToValidate="TB_D" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validarRegistro"></asp:RequiredFieldValidator>
                <br />
                <asp:RegularExpressionValidator ID="REV_D" runat="server" Font-Size="Small"  ControlToValidate="TB_D" Display="Dynamic" ErrorMessage="Limite de 100 caracteres superado" ForeColor="Red" ValidationExpression="^[\s\S]{0,100}$"></asp:RegularExpressionValidator>
                </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
            <asp:Label ID="Label6" runat="server" Text="Cantidad de personas"></asp:Label>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
                <asp:TextBox ID="TB_CP" runat="server" BorderColor="#CCCCFF" BorderStyle="Groove" Height="26px"  Width="344px" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_CP" runat="server" ControlToValidate="TB_CP" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validarRegistro"></asp:RequiredFieldValidator>
                <br />
                <asp:RegularExpressionValidator ID="REV_CP" runat="server" ControlToValidate="TB_CP" Display="Dynamic" ErrorMessage="Valor inválido (1-99)" ForeColor="Red" ValidationExpression="^([1-9][0-9]?)$" Font-Size="Small"></asp:RegularExpressionValidator>
                </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
            <asp:Label ID="Label7" runat="server" Text="Hora inicio"></asp:Label>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
                <asp:TextBox ID="TB_HI" runat="server" BorderColor="#CCCCFF" BorderStyle="Groove" Height="26px"  Width="344px" TextMode="Time"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_HI" runat="server" ControlToValidate="TB_HI" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validarRegistro"></asp:RequiredFieldValidator>
                </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
            <asp:Label ID="Label8" runat="server" Text="Hora fin"></asp:Label>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style26">
                </td>
        <td class="auto-style27">
                <asp:TextBox ID="TB_HF" runat="server" BorderColor="#CCCCFF" BorderStyle="Groove" Height="26px"  Width="344px" TextMode="Time"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_HF" runat="server" ControlToValidate="TB_HF" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validarRegistro"></asp:RequiredFieldValidator>
                </td>
        <td class="auto-style28"></td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
                &nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
                <asp:Button ID="B_R" runat="server" Height="32px" OnClick="B_R_Click" Text="Registrar" Width="92px" ValidationGroup="validarRegistro"/>
        </td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style25">
                &nbsp;</td>
        <td class="auto-style23">
                &nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
    </tr>
</table>
</asp:Content>

