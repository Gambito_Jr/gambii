﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MPAS.master" AutoEventWireup="true" CodeFile="~/Controller/AdmiSedeAtra.aspx.cs" Inherits="Vista_AdmiSedeAtra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .auto-style9 {
        height: 26px;
    }
        .auto-style22 {
            margin-left: 125px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="w-100">
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="text-center">
                <asp:Label ID="L_A" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Atracciones de"></asp:Label>
            &nbsp;<asp:Label ID="L_AS" runat="server" Font-Bold="True" Font-Size="X-Large"></asp:Label>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="text-center">
            <asp:GridView ID="GV_A" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" HorizontalAlign="Center" AutoGenerateColumns="False" DataSourceID="ODS_sede_atracciones" DataKeyNames="Id_sede_atraccion" PageSize="5">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Id" SortExpression="Id_sede_atraccion">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id_sede_atraccion") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Descripcion" SortExpression="Descripcion">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cantidad personas" SortExpression="Can_per">
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Can_per") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Hora inicio" SortExpression="Hora_ini">
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("Hora_ini") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Hora fin" SortExpression="Hora_fin">
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("Hora_fin") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Disponibilidad" SortExpression="Disponibilidad">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Disponibilidad") %>' Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Precio" SortExpression="Precio">
                        <EditItemTemplate>
                            <asp:TextBox ID="TB_EP" runat="server" Text='<%# Bind("Precio") %>' TextMode="Number"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFV_C02" runat="server" ControlToValidate="TB_EP" ErrorMessage="*" ForeColor="Red" ></asp:RequiredFieldValidator>
                            <br />
                            <asp:RangeValidator ID="R_PP22" runat="server" ControlToValidate="TB_EP" Font-Size="small" ForeColor="Red" MaximumValue="999999" MinimumValue="1000" Text="Valor inválido (1000-999999)" Type="Integer" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("Precio") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sede" SortExpression="Nombre_sede">
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("Nombre_sede") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:CommandField ShowDeleteButton="True" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                <EmptyDataTemplate>
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="¡Actualmente no se encuentra ningún dato registrado, por favor realice el siguiente registro!"></asp:Label>
                    </EmptyDataTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="ODS_sede_atracciones" runat="server" SelectMethod="obteneratrasede" TypeName="DAtraSede" DataObjectTypeName="EAtraSede" DeleteMethod="eliminaratrasede" UpdateMethod="modificaratrasede">
                <SelectParameters>
                    <asp:SessionParameter Name="doc" SessionField="doc_identidad" Type="Int64" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Tipo de atracción:"></asp:Label>
&nbsp;
            <asp:DropDownList ID="DDL_A" runat="server" DataSourceID="ODS_Atracciones" DataTextField="Nombre" DataValueField="IdAtra">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ODS_Atracciones" runat="server" SelectMethod="obteneratra" TypeName="DAtracciones"></asp:ObjectDataSource>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style9"></td>
        <td class="auto-style9"></td>
        <td class="auto-style9"></td>
    </tr>
    <tr>
        <td class="auto-style9"></td>
        <td class="auto-style9">
            <asp:Label ID="Label2" runat="server" Text="¿Cuantas veces desea agregarla?"></asp:Label>
&nbsp;
            <asp:TextBox ID="TB_C" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_C" runat="server" ControlToValidate="TB_C" ErrorMessage="*" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
        &nbsp;<asp:RangeValidator id="rango1" runat="server" ControlToValidate="TB_C" MinimumValue="1" MaximumValue="10" Type="Integer" Text="1-10" Font-Size="small" ForeColor="Red" />
        </td>
        <td class="auto-style9"></td>
    </tr>
    <tr>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style9">
            <asp:Label ID="Label3" runat="server" Text="Precio:"></asp:Label>
&nbsp;
            <asp:TextBox ID="TB_P" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_C0" runat="server" ControlToValidate="TB_P" ErrorMessage="*" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
        &nbsp;<asp:RangeValidator id="R_PP" runat="server" ControlToValidate="TB_P" MinimumValue="100000" MaximumValue="1500000" Type="Integer" Text="Valor inválido (100000-1500000)" Font-Size="small" ForeColor="Red" />
        </td>
        <td class="auto-style9">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style9">
            <asp:Button ID="B_A" runat="server" CssClass="auto-style22" Text="Agregar" OnClick="B_A_Click" ValidationGroup="a"/>
        </td>
        <td class="auto-style9">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
</asp:Content>

