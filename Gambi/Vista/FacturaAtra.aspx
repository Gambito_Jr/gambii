﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/Controller/FacturaAtra.aspx.cs" Inherits="Vista_FacturaAtra" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td>
                        <CR:CrystalReportViewer ID="CRV_FA" runat="server" AutoDataBind="True" GroupTreeImagesFolderUrl="" Height="815px" ReportSourceID="CRS_FA" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="238px" />
                        <CR:CrystalReportSource ID="CRS_FA" runat="server">
                            <Report FileName="C:\Users\Gambito\Documents\Gambi\Gambi\Reportes\ReporteFacturaAtra.rpt">
                            </Report>
                        </CR:CrystalReportSource>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
