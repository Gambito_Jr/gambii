﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MPE.master" AutoEventWireup="true" CodeFile="~/Controller/EmpleResAtr.aspx.cs" Inherits="Vista_EmpleResAtr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style21 {
            height: 26px;
        }
        .auto-style22 {
            text-align: center;
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="w-100">
        <tr>
            <td class="auto-style22">&nbsp;</td>
            <td class="auto-style22">&nbsp;</td>
            <td class="auto-style23"></td>
        </tr>
        <tr>
            <td class="auto-style21"></td>
            <td class="text-center">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="Atracciones disponibles"></asp:Label>
            </td>
            <td class="text-center">
                <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Atracciones reservadas"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style21">&nbsp;</td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style21">
                &nbsp;</td>
            <td class="auto-style21">
                <div class="text-center">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" Width="342px" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="ODS_HD" ForeColor="Black" GridLines="Vertical" HorizontalAlign="Center" PageSize="7" >
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="Número atracción" SortExpression="Id_sede_atraccion">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Id_sede_atraccion") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id_sede_atraccion") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cantidad personas" SortExpression="Can_per">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Can_per") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Can_per") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hora inicio" SortExpression="Hora_ini">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Hora_ini") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Hora_ini") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hora fin" SortExpression="Hora_fin">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Hora_fin") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Hora_fin") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Precio" SortExpression="Precio">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Precio") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("Precio") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sede" SortExpression="Nombre_sede">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("Nombre_sede") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("Nombre_sede") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" Text="Reservar" CommandArgument='<%# Bind("Id_sede_atraccion") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#CCCC99" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#F7F7DE" />
        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FBFBF2" />
        <SortedAscendingHeaderStyle BackColor="#848384" />
        <SortedDescendingCellStyle BackColor="#EAEAD3" />
        <SortedDescendingHeaderStyle BackColor="#575357" />
        <EmptyDataTemplate>
                        <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="¡Actualmente no se encuentran atracciones disponibles!"></asp:Label>
                    </EmptyDataTemplate>
    </asp:GridView>
                </div>
                <asp:ObjectDataSource ID="ODS_HD" runat="server" SelectMethod="obteneratrasededis" TypeName="DAtraSede">
                    <SelectParameters>
                        <asp:SessionParameter Name="doc" SessionField="doc_identidad" Type="Int64" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <div class="text-center">
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" OnRowCommand="GridView2_RowCommand" Width="342px" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="ODS_HND" ForeColor="Black" GridLines="Vertical" HorizontalAlign="Center" PageSize="7" >
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Id_sede_atraccion" HeaderText="Número atracción" SortExpression="Id_sede_atraccion" />
            <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
            <asp:BoundField DataField="Can_per" HeaderText="Cantidad personas" SortExpression="Can_per" />
            <asp:BoundField DataField="Hora_ini" HeaderText="Hora inicio" SortExpression="Hora_ini" />
            <asp:BoundField DataField="Hora_fin" HeaderText="Hora fin" SortExpression="Hora_fin" />
            <asp:BoundField DataField="Precio" HeaderText="Precio" SortExpression="Precio" />
            <asp:BoundField DataField="Nombre_sede" HeaderText="Sede" SortExpression="Nombre_sede" />
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" Text="Entregar" CommandArgument='<%# Bind("Id_sede_atraccion") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#CCCC99" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#F7F7DE" />
        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FBFBF2" />
        <SortedAscendingHeaderStyle BackColor="#848384" />
        <SortedDescendingCellStyle BackColor="#EAEAD3" />
        <SortedDescendingHeaderStyle BackColor="#575357" />
        <EmptyDataTemplate>
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Text="¡Actualmente no se encuentran habitaciones reservadas!"></asp:Label>
                    </EmptyDataTemplate>
    </asp:GridView>
                </div>
                <asp:ObjectDataSource ID="ODS_HND" runat="server" SelectMethod="obteneratrasedenodis" TypeName="DAtraSede">
                    <SelectParameters>
                        <asp:SessionParameter Name="doc" SessionField="doc_identidad" Type="Int64" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                </td>
        </tr>
        <tr>
            <td class="auto-style21">&nbsp;</td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style21"></td>
            <td class="auto-style22" colspan="2">
                <asp:Label ID="Label18" runat="server" Text="Entregar reserva" Visible="False" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style21">&nbsp;</td>
            <td class="auto-style21">
                <br />
                <asp:Label ID="Label19" runat="server" Text="Documento identidad" Visible="False"></asp:Label>
                &nbsp;<asp:TextBox ID="TB_cedula0" runat="server" Visible="False" CssClass="auto-style24" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV_ER" runat="server" ControlToValidate="TB_cedula0" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="REV_DI0" runat="server" ControlToValidate="TB_cedula0" Display="Dynamic" ErrorMessage="Documento de identidad inválido" ForeColor="Red" ValidationGroup="1" ValidationExpression="\d{8,12}"  Font-Size="Small"></asp:RegularExpressionValidator>
            </td>
            <td>
                <asp:Button ID="B_E" runat="server" Text="Entregar" Width="168px" ValidationGroup="1" OnClick="B_E_Click" Visible="False"/>
                <asp:Button ID="B_C" runat="server" CssClass="auto-style30" OnClick="B_C_Click" Text="Cancelar" Visible="False" Width="154px" style="margin-left: 54px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style21"></td>
            <td class="auto-style21"></td>
            <td class="auto-style29"></td>
        </tr>
        <tr>
            <td class="auto-style21"></td>
            <td class="auto-style21"></td>
            <td class="auto-style29"></td>
        </tr>
        <tr>
            <td class="auto-style21">
                &nbsp;</td>
            <td class="text-center" colspan="2">
                <asp:Label ID="Label7" runat="server" Text="Seleccione la fecha de reserva" Visible="False" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style21">
                &nbsp;</td>
            <td class="auto-style21" colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style21">
                &nbsp;</td>
            <td class="text-center">
                <asp:Label ID="Label8" runat="server" Text="Fecha Inicio" Visible="False" Font-Bold="True"></asp:Label>
                <asp:Calendar ID="C_fecha_inicio" runat="server" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Visible="False" Width="350px" CssClass="auto-style27" style="margin-left: 116px">
                    <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                    <OtherMonthDayStyle ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                    <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                    <TodayDayStyle BackColor="#CCCCCC" />
                </asp:Calendar>
            </td>
            <td class="text-center">
                <asp:Label ID="Label9" runat="server" Text="Fecha Fin" Visible="False" Font-Bold="True"></asp:Label>
                <div class="text-center">
                <asp:Calendar ID="C_fecha_fin" runat="server" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Visible="False" Width="350px" CssClass="auto-style26" style="margin-left: 96px">
                    <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                    <OtherMonthDayStyle ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                    <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                    <TodayDayStyle BackColor="#CCCCCC" />
                </asp:Calendar>
                </div>
            </td>
        </tr>
        <tr>
            <td class="auto-style21">
                &nbsp;</td>
            <td class="auto-style21">
                &nbsp;</td>
            <td class="text-center">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style22">
                </td>
            <td class="auto-style22">
                <asp:Label ID="Label10" runat="server" Text="Documento identidad" Visible="False"></asp:Label>
                &nbsp;
                <asp:TextBox ID="TB_cedula" runat="server" Visible="False" CssClass="auto-style24" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TB_cedula" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validarRegistro"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="REV_DI" runat="server" ControlToValidate="TB_cedula" Display="Dynamic" ErrorMessage="Documento de identidad inválido" ForeColor="Red" ValidationExpression="\d{8,12}" Font-Size="Small"></asp:RegularExpressionValidator>
            </td>
            <td class="auto-style29">
                <asp:Button ID="BReservar" runat="server" OnClick="BReservar_Click" ValidationGroup="validarRegistro" Text="Reservar" Visible="False" Width="167px" />
                <asp:Button ID="BCancelar" runat="server" OnClick="BCancelar_Click" Text="Cancelar" Visible="False" CssClass="auto-style25" Width="154px" style="margin-left: 61px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style21">&nbsp;</td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style21">&nbsp;</td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

