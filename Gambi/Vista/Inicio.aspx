﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MPI.master" AutoEventWireup="true" CodeFile="~/Controller/Inicio.aspx.cs" Inherits="Vista_Inicio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style9 {
            width: 286px;
        }
        .auto-style10 {
            width: 286px;
            text-align: center;
        }
        .auto-style11 {
            width: 224px;
        }
        .auto-style12 {
            width: 224px;
            text-align: center;
        }
        .auto-style17 {
            width: 100%;
            height: 388px;
        }
        .auto-style18 {
            height: 26px;
        }
        .auto-style19 {
            width: 286px;
            height: 26px;
        }
        .auto-style20 {
            height: 26px;
            text-align: center;
        }
        .auto-style21 {
            height: 49px;
        }
        .auto-style25 {
            width: 289px;
        }
        .auto-style26 {
            width: 289px;
            text-align: center;
        }
        .auto-style27 {
            margin-left: 0px;
        }
        .auto-style28 {
            height: 49px;
            text-align: center;
        }
        .auto-style29 {
            width: 180px;
        }
        .auto-style30 {
            width: 180px;
            text-align: center;
        }
        .auto-style31 {
            width: 180px;
            height: 49px;
        }
        .auto-style32 {
            height: 38px;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="auto-style17">
    <tr>
        <td>&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style11">&nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
        <td class="auto-style25">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="text-center" colspan="8">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="XX-Large" Text="Bienvenidos a Gambi"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="auto-style10">
            &nbsp;</td>
        <td class="auto-style12">
            &nbsp;</td>
        <td class="auto-style30">
            &nbsp;</td>
        <td class="auto-style26">
            &nbsp;</td>
        <td class="text-center">
            &nbsp;</td>
        <td class="text-center">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="text-center" colspan="8">
            <asp:Label ID="Label2" runat="server" Text="El centro turistico Gambi es un espacio el cual  ha brindado alegría y diversión a las familias y se ha mantenido como una alternativa vigente de entretenimiento y hospedaje en el país que ha evolucionado con los años para próximamente poder ofrecer también otros servicios colaterales. "></asp:Label>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style11">&nbsp;</td>
        <td class="auto-style29">
            <br />
        </td>
        <td class="auto-style25">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style21"></td>
        <td class="auto-style28" colspan="2">
            <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Uso de atracciones"></asp:Label>
        </td>
        <td class="auto-style31"></td>
        <td class="auto-style28" colspan="2">
            <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Uso de habitaciones"></asp:Label>
        </td>
        <td class="auto-style21"></td>
        <td class="auto-style21"></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="text-center" colspan="2">
            <asp:Label ID="Label5" runat="server" Text="Para el uso de las atracciones el usuario debe realizar la reserva con anticipación para que este lista, para esto se puede comunicar a los números de telefóno o correo electrónico que esta en la parte inferior del formulario"></asp:Label>
        </td>
        <td class="auto-style29">&nbsp;</td>
        <td class="text-center" colspan="2">
            <asp:Label ID="Label6" runat="server" Text="Para el uso de las habitaciones el usuario debe realizar la reserva con anticipación para que este lista, para esto se puede comunicar a los números de telefóno o correo electrónico que esta en la parte inferior del formulario "></asp:Label>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style11">&nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
        <td class="auto-style25">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="text-center" colspan="2">
            <asp:Image ID="Image2" runat="server" Height="196px" ImageUrl="~/Imagenes/Atraccion.png" Width="450px" />
        </td>
        <td class="auto-style29">&nbsp;</td>
        <td class="text-center" colspan="2">
            <asp:Image ID="Image1" runat="server" CssClass="auto-style27" Height="196px" ImageUrl="~/Imagenes/Habitacion.jpg" Width="450px" />
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style18"></td>
        <td class="auto-style19"></td>
        <td class="auto-style20" colspan="3">
            &nbsp;</td>
        <td class="auto-style18"></td>
        <td class="auto-style18"></td>
        <td class="auto-style18"></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style11">&nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
        <td class="auto-style25">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="text-center" colspan="8">
            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="X-Large" Text="CONTACTENOS"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style32" colspan="8">
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Correo electrónico"></asp:Label>
            <br />
&nbsp;<asp:Label ID="Label10" runat="server" Font-Bold="False" Font-Size="Medium" Text="gambicorp@gmail.com"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="text-center" colspan="8">
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Teléfono"></asp:Label>
            <br />
            <asp:Label ID="Label11" runat="server" Font-Bold="False" Font-Size="Medium" Text="3222695187"></asp:Label>
            <br />
            <asp:Label ID="Label12" runat="server" Font-Bold="False" Font-Size="Medium" Text="3122861314"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="auto-style9">&nbsp;</td>
        <td class="auto-style11">&nbsp;</td>
        <td class="auto-style29">&nbsp;</td>
        <td class="auto-style25">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </table>
</asp:Content>

