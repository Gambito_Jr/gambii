﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MPC.master" AutoEventWireup="true" CodeFile="~/Controller/ClienteConAtra.aspx.cs" Inherits="Vista_ClienteConAtra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style22 {
            height: 52px;
        }
        .auto-style23 {
            height: 50px;
        }
        .auto-style24 {
            width: 914px;
        }
        .auto-style25 {
            text-align: center;
            width: 914px;
        }
        .auto-style26 {
            height: 52px;
            width: 914px;
        }
        .auto-style27 {
            height: 50px;
            width: 914px;
        }
        .auto-style29 {
            height: 40px;
            width: 914px;
            text-align: center;
        }
        .auto-style30 {
            height: 31px;
        }
        .auto-style31 {
            width: 914px;
            height: 31px;
            text-align: center;
        }
        .auto-style35 {
            width: 134px;
        }
        .auto-style36 {
            height: 52px;
            width: 134px;
        }
        .auto-style37 {
            height: 50px;
            width: 134px;
        }
        .auto-style39 {
            height: 31px;
            width: 134px;
        }
        .auto-style42 {
            height: 38px;
            text-align: center;
        }
        .auto-style43 {
            height: 38px;
            width: 160px;
        }
        .auto-style44 {
            height: 40px;
            width: 134px;
        }
        .auto-style45 {
            height: 40px;
        }
        .auto-style46 {
            height: 38px;
            width: 134px;
        }
        .auto-style47 {
            height: 38px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="w-100">
        <tr>
            <td class="auto-style35">&nbsp;</td>
            <td class="auto-style24" colspan="3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style35">&nbsp;</td>
            <td class="auto-style25" colspan="3">
                    
                <asp:Label ID="L_A" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Consultas de reservas de atracciones"></asp:Label>
                    
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style36"></td>
            <td class="auto-style26" colspan="3"></td>
            <td class="auto-style22"></td>
        </tr>
        <tr>
            <td class="auto-style37"></td>
            <td class="auto-style27" colspan="3">
                <asp:CheckBox ID="CB_G" runat="server" Text="Consulta general" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CB_E" runat="server" Text="Consulta específica " />
            </td>
            <td class="auto-style23"></td>
        </tr>
        <tr>
            <td class="auto-style44"></td>
            <td class="auto-style29" colspan="3">
                <asp:Button ID="B_A" runat="server" OnClick="B_A_Click" Text="Avanzar" />
            </td>
            <td class="auto-style45"></td>
        </tr>
        <tr>
            <td class="auto-style39"></td>
            <td class="auto-style31" colspan="3">
                <asp:linkbutton ID="LB_CG" CssClass="text-dark" runat="server" Width="423px" BorderColor="Black"  BorderStyle="Double" Visible="False" OnClick="LB_CG_Click1">CONSULTA GENERAL</asp:linkbutton>
                </td>
            <td class="auto-style30"></td>
        </tr>
        <tr>
            <td class="auto-style46"></td>
            <td class="auto-style43">
                <asp:Label ID="Label3" runat="server" Text="Escriba la busqueda:" Visible="False"></asp:Label>
            </td>
            <td class="auto-style47">
                <asp:TextBox ID="TB_F" runat="server" Width="145px" TextMode="Number" Visible="False"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TB_F" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            <td class="auto-style42">
                <asp:linkbutton ID="LB_C" CssClass="text-dark" runat="server" Width="145px" BorderColor="Black" ValidationGroup="a" BorderStyle="Double" OnClick="LB_C_Click" Visible="False">Consultar</asp:linkbutton>
                </td>
            <td class="auto-style47"></td>
        </tr>
        <tr>
            <td class="auto-style35">&nbsp;</td>
            <td class="auto-style24" colspan="3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style35">&nbsp;</td>
            <td class="auto-style24" colspan="3">
                <div class="text-center">
                    <asp:GridView ID="GV_C" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField DataField="Id_reserva" HeaderText="Id reserva" SortExpression="Id_reserva" />
                            <asp:BoundField DataField="Fecha_ent" HeaderText="Fecha entrada" SortExpression="Fecha_ent" />
                            <asp:BoundField DataField="Fecha_sal" HeaderText="Fecha salida" SortExpression="Fecha_sal" />
                            <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                            <asp:BoundField DataField="Servicio" HeaderText="Servicio" SortExpression="Servicio" />
                            <asp:BoundField DataField="Doc_identidad" HeaderText="Documento identidad" SortExpression="Doc_identidad" />
                            <asp:BoundField DataField="Sede" HeaderText="Sede" SortExpression="Sede" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#333333" />
                        <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="White" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#487575" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#275353" />
                        <EmptyDataTemplate>
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="¡Actualmente no hay reservas con su documento de identidad!"></asp:Label>
                    </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style35">&nbsp;</td>
            <td class="auto-style24" colspan="3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style35">&nbsp;</td>
            <td class="auto-style24" colspan="3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

