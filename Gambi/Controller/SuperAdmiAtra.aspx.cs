﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Vista_SuperAdmiAtra : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user_name"] == null)
            Response.Redirect("IniciarS.aspx");
        else
        {
            int user = (int)Session["rol_id"];
            string url;
            if (user != 1)
            {
                switch (user)
                {
                    case 2:
                        url = "AdmiSedeInicio.aspx";
                        break;
                    case 3:
                        url = "EmpleInicio.aspx";
                        break;
                    case 4:
                        url = "ClienteInicio.aspx";
                        break;
                    default:
                        url = "IniciarS.aspx";
                        break;
                }
                Response.Redirect(url);
            }
        }
    }

    protected void B_R_Click(object sender, EventArgs e)
    {
        try
        {
            EAtracciones atra = new EAtracciones();
            atra.Nombre = TB_NA.Text;
            atra.PrecioP = int.Parse(TB_PA.Text);
            atra.Descripcion = TB_D.Text;
            atra.CantP = int.Parse(TB_CP.Text);
            atra.HoraIni = TimeSpan.Parse(TB_HI.Text);
            atra.HoraFin = TimeSpan.Parse(TB_HF.Text);
            atra.Disponibilidad = true;
            bool resp = new DAtracciones().insertarAtraccion(atra);
            if (resp == true)
            {
                GV_Atra.DataBind();
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            else
            {
                this.RegisterStartupScript("mensaje", "<script type='text/javascript'>alert('El horario de la atracción debe ser máximo de 10 horas');</script>");
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    protected void GV_Atra_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GV_Atra.Rows[e.RowIndex];
        e.NewValues[4] = TimeSpan.Parse(e.NewValues[4].ToString());
        e.NewValues[5] = TimeSpan.Parse(e.NewValues[5].ToString());
    }
}