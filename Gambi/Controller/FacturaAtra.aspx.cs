﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

public partial class Vista_FacturaAtra : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Session["pago"] == null) & (Session["user_name"] == null))
        {
            Response.Redirect("IniciarS.aspx");
        }
        else if ((Session["pago"] == null) & (Session["user_name"] != null))
        {
            Response.Redirect("EmplePago.aspx");
        }
        llenarreporte();
    }

    protected void llenarreporte()
    {
        try
        {
            ReporteFacturaAtra consu = informacion();
            CRS_FA.ReportDocument.SetDataSource(consu);
            CRV_FA.ReportSource = CRS_FA;

        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    protected ReporteFacturaAtra informacion()
    {
        DataRow fila;  
        DataTable reporte = new DataTable();
        ReporteFacturaAtra repor = new ReporteFacturaAtra();
        
        reporte = repor.Tables["FacturaAtra"];

        var doc = int.Parse(Session["pago"].ToString());
        DataTable Intermedio = new DFactura().obtenerFacturaReporte(doc);

        int i = 0;
        
        fila = reporte.NewRow();
        fila["id_factura"] = int.Parse(Intermedio.Rows[i]["id_reserva"].ToString());
        fila["total"] = long.Parse(Intermedio.Rows[i]["total"].ToString());
        fila["servicio"] = Intermedio.Rows[i]["sede_atra"].ToString();
        fila["doc_identidad"] = long.Parse(Intermedio.Rows[i]["doc_identidad"].ToString());
        fila["fecha_ent"] = DateTime.Parse(Intermedio.Rows[i]["fecha_sal"].ToString());
        fila["sede"] = Intermedio.Rows[i]["sede"].ToString();
        fila["estado"] = Intermedio.Rows[i]["estado"].ToString();

        reporte.Rows.Add(fila);
        
         return repor;
    }
}